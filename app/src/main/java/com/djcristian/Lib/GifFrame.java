
package com.djcristian.Lib;

class GifFrame {
    int ix, iy, iw, ih;

    boolean interlace;

    boolean transparency;

    int dispose;

    int transIndex;
    /**
     * Delay, in ms, to next frame.
     */
    int delay;
    /**
     * Index in the raw buffer where we need to start reading to decode.
     */
    int bufferFrameStart;
    /**
     * Local Color Table.
     */
    int[] lct;
}