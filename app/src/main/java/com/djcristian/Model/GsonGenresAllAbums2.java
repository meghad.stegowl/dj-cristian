package com.djcristian.Model;

public class GsonGenresAllAbums2 {
    private String album_id;
    private String album_name;
    private String album_image;

    public GsonGenresAllAbums2(String album_id, String album_name, String album_image) {
        this.album_id = album_id;
        this.album_name = album_name;
        this.album_image = album_image;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public String getAlbum_image() {
        return album_image;
    }

    public void setAlbum_image(String album_image) {
        this.album_image = album_image;
    }
}
