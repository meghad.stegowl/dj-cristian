package com.djcristian.Model;


public class Emoji {
    private String emoji_name;
    private int emoji;

    public Emoji(String emoji_name, int emoji) {
        this.emoji_name = emoji_name;
        this.emoji = emoji;
    }

    public String getEmoji_name() {
        return emoji_name;
    }

    public void setEmoji_name(String emoji_name) {
        this.emoji_name = emoji_name;
    }

    public int getEmoji() {
        return emoji;
    }

    public void setEmoji(int emoji) {
        this.emoji = emoji;
    }
}
