package com.djcristian.Model;

public class PojoGenresDJ {

    String genre_name;
    String song_count;

    public PojoGenresDJ() {
    }

    public PojoGenresDJ(String genre_name, String song_count) {
        this.genre_name = genre_name;
        this.song_count = song_count;
    }

    public String getGenre_name() {
        return genre_name;
    }

    public void setGenre_name(String genre_name) {
        this.genre_name = genre_name;
    }

    public String getSong_count() {
        return song_count;
    }

    public void setSong_count(String song_count) {
        this.song_count = song_count;
    }
}
