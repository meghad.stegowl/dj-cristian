package com.djcristian.Model;

public class PojoPlaylistDJ {
    String playlistName, SongNumber;

    public PojoPlaylistDJ() {
    }

    public PojoPlaylistDJ(String playlistName, String songNumber) {
        this.playlistName = playlistName;
        this.SongNumber = songNumber;
    }

    public String getPlaylistName() {
        return playlistName;
    }

    public void setPlaylistName(String playlistName) {
        this.playlistName = playlistName;
    }

    public String getSongNumber() {
        return SongNumber;
    }

    public void setSongNumber(String songNumber) {
        SongNumber = songNumber;
    }
}
