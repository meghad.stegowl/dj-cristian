package com.djcristian.Model;

public class PojoVideoMovies {
    private String name, data_link, image, date;

    public PojoVideoMovies(String name, String data_link, String image, String date) {
        this.name = name;
        this.data_link = data_link;
        this.image = image;
        this.date = date;
    }
//    public String getID() {
//        return ID;
//    }
//
//    public void setID(String ID) {
//        this.ID = ID;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData_link() {
        return data_link;
    }

    public void setData_link(String data_link) {
        this.data_link = data_link;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
