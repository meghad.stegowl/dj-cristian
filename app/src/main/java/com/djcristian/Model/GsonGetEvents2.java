package com.djcristian.Model;

public class GsonGetEvents2 {

    private String event_id;
    private String title;
    private String description;
    private String start;
    private String end;
    private String time;
    private String latitude;
    private String longitude;
    private String address;
    private String image;

    public GsonGetEvents2(String event_id, String title, String description, String start, String end, String time, String latitude, String longitude, String address, String image) {
        this.event_id = event_id;
        this.title = title;
        this.description = description;
        this.start = start;
        this.end = end;
        this.time = time;
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        this.image = image;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}