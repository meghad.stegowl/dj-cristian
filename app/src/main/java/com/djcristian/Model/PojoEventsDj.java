package com.djcristian.Model;

public class PojoEventsDj {
    private int pic;
    private String heading;
    private String Address;

    public PojoEventsDj() {
    }

    public PojoEventsDj(int pic, String heading, String address) {
        this.pic = pic;
        this.heading = heading;
        Address = address;
    }

    public int getPic() {
        return pic;
    }

    public void setPic(int pic) {
        this.pic = pic;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }


}
