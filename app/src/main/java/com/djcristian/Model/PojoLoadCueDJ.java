package com.djcristian.Model;

public class PojoLoadCueDJ {

    int img;
    String song_name;

    public PojoLoadCueDJ() {
    }

    public PojoLoadCueDJ(String song_name) {
        this.song_name = song_name;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getSong_name() {
        return song_name;
    }

    public void setSong_name(String song_name) {
        this.song_name = song_name;
    }
}
