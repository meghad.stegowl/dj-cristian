package com.djcristian.Model;

/**
 * Created by Heyyy...Mohiniiiii on 5/18/2017.
 */
public class GalleryFolder {

    private String image, f_id, name;

    public GalleryFolder(String image, String f_id, String name) {
        this.image = image;
        this.f_id = f_id;
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getF_id() {
        return f_id;
    }

    public void setF_id(String f_id) {
        this.f_id = f_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
