package com.djcristian.Model;

public class GsonDJMixesAllAlbumData {
    private String genres_id, genres_name, position, image, album_id, album_name, album_image, total_count, artist_name;

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public String getTotal_count() {
        return total_count;
    }

    public void setTotal_count(String total_count) {
        this.total_count = total_count;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public String getGenres_id() {
        return genres_id;
    }

    public void setGenres_id(String genres_id) {
        this.genres_id = genres_id;
    }

    public String getGenres_name() {
        return genres_name;
    }

    public void setGenres_name(String genres_name) {
        this.genres_name = genres_name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public GsonDJMixesAllAlbumData(String genres_id, String genres_name, String position, String image, String artist_name) {
        this.genres_id = genres_id;
        this.genres_name = genres_name;
        this.position = position;
        this.image = image;
        this.artist_name = artist_name;
    }

    public GsonDJMixesAllAlbumData(String album_id, String album_name, String album_image) {
        this.album_id = album_id;
        this.album_name = album_name;
        this.album_image = album_image;
    }
}
