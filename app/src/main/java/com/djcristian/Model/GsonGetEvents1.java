package com.djcristian.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GsonGetEvents1 {

    private String status;
    @SerializedName("data")
    private ArrayList<GsonGetEvents2> gsonGetEvents2s;
    private String msg;

    public GsonGetEvents1(String status, ArrayList<GsonGetEvents2> gsonGetEvents2s, String msg) {
        this.status = status;
        this.gsonGetEvents2s = gsonGetEvents2s;
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<GsonGetEvents2> getGsonGetEvents2s() {
        return gsonGetEvents2s;
    }

    public void setGsonGetEvents2s(ArrayList<GsonGetEvents2> gsonGetEvents2s) {
        this.gsonGetEvents2s = gsonGetEvents2s;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}
