package com.djcristian.Model;

public class PojoCue {
    private String song_id;
    private String song_name;
    private String song_url;
    private String image;
    private String artistname;
    private String duration;

    public PojoCue(String song_id, String song_name, String song_url, String image, String artistname, String duration) {
        this.song_id = song_id;
        this.duration = duration;
        this.artistname = artistname;
        this.song_name = song_name;
        this.song_url = song_url;
        this.image = image;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getArtistname() {
        return artistname;
    }

    public void setArtistname(String artistname) {
        this.artistname = artistname;
    }

    public String getSong_id() {
        return song_id;
    }

    public void setSong_id(String song_id) {
        this.song_id = song_id;
    }

    public String getSong_name() {
        return song_name;
    }

    public void setSong_name(String song_name) {
        this.song_name = song_name;
    }

    public String getSong_url() {
        return song_url;
    }

    public void setSong_url(String song_url) {
        this.song_url = song_url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
