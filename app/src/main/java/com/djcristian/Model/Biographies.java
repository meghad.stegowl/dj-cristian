package com.djcristian.Model;



public class Biographies {
    private String bio_id;
    private String artist_name;
    private String biography;
    private String thumb;
    private String banner;
    private String insta_url;


    public String getBio_id() {
        return bio_id;
    }

    public void setBio_id(String bio_id) {
        this.bio_id = bio_id;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getInsta_url() {
        return insta_url;
    }

    public void setInsta_url(String insta_url) {
        this.insta_url = insta_url;
    }

    public Biographies(String bio_id, String artist_name, String biography, String thumb, String banner, String insta_url) {
        this.bio_id = bio_id;
        this.artist_name = artist_name;
        this.biography = biography;
        this.thumb = thumb;
        this.banner = banner;
        this.insta_url = insta_url;
    }
}
