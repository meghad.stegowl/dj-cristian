package com.djcristian.Model;

public class PojoVideoMovies2 {
    private String name, data_link, image, banner_image, description;

    public PojoVideoMovies2(String name, String data_link, String image, String banner_image, String description) {
        this.name = name;
        this.data_link = data_link;
        this.image = image;
        this.banner_image = banner_image;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData_link() {
        return data_link;
    }

    public void setData_link(String data_link) {
        this.data_link = data_link;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBanner_image() {
        return banner_image;
    }

    public void setBanner_image(String banner_image) {
        this.banner_image = banner_image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
