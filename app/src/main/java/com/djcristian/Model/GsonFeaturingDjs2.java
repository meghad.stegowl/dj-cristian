package com.djcristian.Model;


public class GsonFeaturingDjs2 {
    private String id;
    private String name;
    private String count;
    private String image;
    private String insta_url;


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getInsta_url() {
        return insta_url;
    }

    public void setInsta_url(String insta_url) {
        this.insta_url = insta_url;
    }

    public GsonFeaturingDjs2(String id, String name, String count, String image) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.image = image;
    }

    public GsonFeaturingDjs2(String id, String name, String image, String insta_url, String count) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.image = image;
        this.insta_url = insta_url;
    }
}
