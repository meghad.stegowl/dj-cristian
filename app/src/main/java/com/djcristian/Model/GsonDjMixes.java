package com.djcristian.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GsonDjMixes {
    private String status;
    private String msg;
    @SerializedName("data")
    private ArrayList<GsonFeaturingDjsSongs2> data;
    private String image;
    private String inner_image;

    public GsonDjMixes(String status, ArrayList<GsonFeaturingDjsSongs2> data, String msg, String image, String inner_image) {
        this.status = status;
        this.image = image;
        this.data = data;
        this.msg = msg;
        this.inner_image = inner_image;

    }

    public GsonDjMixes(String status, ArrayList<GsonFeaturingDjsSongs2> data, String msg) {
        this.status = status;
        this.data = data;
        this.msg = msg;

    }

    public String getInner_image() {
        return inner_image;
    }

    public void setInner_image(String inner_image) {
        this.inner_image = inner_image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<GsonFeaturingDjsSongs2> getData() {
        return data;
    }

    public void setData(ArrayList<GsonFeaturingDjsSongs2> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
