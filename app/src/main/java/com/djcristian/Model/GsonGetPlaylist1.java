package com.djcristian.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GsonGetPlaylist1 {

    private String status;
    @SerializedName("data")
    private ArrayList<GsonGetPlaylist2> data;
    private String msg;

    public GsonGetPlaylist1(String status, ArrayList<GsonGetPlaylist2> data, String msg) {
        this.status = status;
        this.data = data;
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<GsonGetPlaylist2> getData() {
        return data;
    }

    public void setData(ArrayList<GsonGetPlaylist2> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
