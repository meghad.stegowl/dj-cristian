package com.djcristian.Model;

public class More2 {
    String image;
    String title;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public More2(String image, String title) {
        this.image = image;
        this.title = title;
    }
}
