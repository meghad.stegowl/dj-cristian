package com.djcristian.Model;

/**
 * Created by Android18 on 20-10-2016.
 */
public class GalleryImages {
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public GalleryImages(String image) {
        this.image = image;
    }
}
