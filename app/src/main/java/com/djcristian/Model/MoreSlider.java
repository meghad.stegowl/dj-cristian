package com.djcristian.Model;

public class MoreSlider {
    String image, id;
    String title;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MoreSlider(String image) {
        this.image = image;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MoreSlider(String image, String title, String id) {
        this.image = image;
        this.title = title;
        this.id = id;
    }
}
