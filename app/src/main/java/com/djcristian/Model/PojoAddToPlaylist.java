package com.djcristian.Model;

public class PojoAddToPlaylist {
    private String u_id;
    private String s_id;
    private String playlist_id;
    private String playlist_name;


    public PojoAddToPlaylist(String u_id, String s_id, String playlist_id, String playlist_name) {
        this.u_id = u_id;
        this.s_id = s_id;
        this.playlist_id = playlist_id;
        this.playlist_name = playlist_name;
    }

    public String getPlaylist_name() {
        return playlist_name;
    }

    public void setPlaylist_name(String playlist_name) {
        this.playlist_name = playlist_name;
    }

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getPlaylist_id() {
        return playlist_id;
    }

    public void setPlaylist_id(String playlist_id) {
        this.playlist_id = playlist_id;
    }
}
