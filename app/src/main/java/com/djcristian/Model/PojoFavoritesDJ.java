package com.djcristian.Model;

public class PojoFavoritesDJ {
    private int img;
    private String songName;

    public PojoFavoritesDJ() {
    }

    public PojoFavoritesDJ(int img, String songName) {
        this.img = img;
        this.songName = songName;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }


}
