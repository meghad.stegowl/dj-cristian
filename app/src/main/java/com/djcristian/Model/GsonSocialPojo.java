package com.djcristian.Model;

public class GsonSocialPojo {

    private String ID;
    private String name;
    private String link;

    public GsonSocialPojo(String ID, String name, String link) {
        this.ID = ID;
        this.name = name;
        this.link = link;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
