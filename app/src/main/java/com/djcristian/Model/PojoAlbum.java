package com.djcristian.Model;

public class PojoAlbum {
    String genres_id, genres_name, total_genres, image, banner_image, insta_url;

    public PojoAlbum(String genres_id, String genres_name, String total_genres, String image, String banner_image) {
        this.genres_id = genres_id;
        this.genres_name = genres_name;
        this.total_genres = total_genres;
        this.image = image;
        this.banner_image = banner_image;
    }

    public PojoAlbum(String genres_id, String genres_name, String image, String insta_url) {
        this.genres_id = genres_id;
        this.genres_name = genres_name;
        this.image = image;
        this.insta_url = insta_url;
    }

    public String getInsta_url() {
        return insta_url;
    }

    public void setInsta_url(String insta_url) {
        this.insta_url = insta_url;
    }

    public String getBanner_image() {
        return banner_image;
    }

    public void setBanner_image(String banner_image) {
        this.banner_image = banner_image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGenres_id() {
        return genres_id;
    }

    public void setGenres_id(String genres_id) {
        this.genres_id = genres_id;
    }

    public String getGenres_name() {
        return genres_name;
    }

    public void setGenres_name(String genres_name) {
        this.genres_name = genres_name;
    }

    public String getTotal_genres() {
        return total_genres;
    }

    public void setTotal_genres(String total_genres) {
        this.total_genres = total_genres;
    }
}
