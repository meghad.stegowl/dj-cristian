package com.djcristian.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class GsonGenresAllAbums1 {
    private String status;
    @SerializedName("data")
    private ArrayList<GsonGenresAllAbums2> data;
    private String msg;

    public GsonGenresAllAbums1(String status, ArrayList<GsonGenresAllAbums2> data, String msg) {
        this.status = status;
        this.data = data;
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<GsonGenresAllAbums2> getData() {
        return data;
    }

    public void setData(ArrayList<GsonGenresAllAbums2> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
