package com.djcristian.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GsonDJMixesAllAlbum {
    private String status;
    private String msg;
    private String img;
    @SerializedName("data")
    private ArrayList<GsonDJMixesAllAlbumData> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<GsonDJMixesAllAlbumData> getData() {
        return data;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setData(ArrayList<GsonDJMixesAllAlbumData> data) {
        this.data = data;
    }

    public GsonDJMixesAllAlbum(String status, String msg, ArrayList<GsonDJMixesAllAlbumData> data, String img) {
        this.status = status;
        this.msg = msg;
        this.data = data;
        this.img = img;
    }
}
