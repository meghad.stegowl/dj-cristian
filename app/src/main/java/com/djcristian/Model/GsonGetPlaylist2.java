package com.djcristian.Model;

public class GsonGetPlaylist2 {

    private String playlist_id;
    private String u_id;
    private String name;
    private String total_count;


    public GsonGetPlaylist2(String playlist_id, String playlist_name, String total_count) {
        this.playlist_id = playlist_id;
        this.name = playlist_name;
        this.total_count = total_count;
    }

    public String getPlaylist_id() {
        return playlist_id;
    }

    public void setPlaylist_id(String playlist_id) {
        this.playlist_id = playlist_id;
    }

    public String getPlaylist_name() {
        return name;
    }

    public void setPlaylist_name(String playlist_name) {
        this.name = playlist_name;
    }

    public String getTotal_count() {
        return total_count;
    }

    public void setTotal_count(String total_count) {
        this.total_count = total_count;
    }
}
