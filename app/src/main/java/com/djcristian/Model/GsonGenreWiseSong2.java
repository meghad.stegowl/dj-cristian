package com.djcristian.Model;

import com.google.gson.annotations.SerializedName;

public class GsonGenreWiseSong2 {
    private String song_id;
    private String like_status;
    private String totle_like;
    private String song_name;
    private String song_url;
    private String image;
    private String genres_id;
    private String album_id;
    @SerializedName("song_time")
    private String duration;
    private String artists_name;
    private String nowPlaying;


    public GsonGenreWiseSong2(String song_id, String like_status, String totle_like, String song_name, String song_url, String image, String genres_id, String album_id, String duration, String artists_name, String nowPlaying) {
        this.song_id = song_id;
        this.artists_name = artists_name;
        this.like_status = like_status;
        this.totle_like = totle_like;
        this.song_name = song_name;
        this.song_url = song_url;
        this.image = image;
        this.genres_id = genres_id;
        this.album_id = album_id;
        this.duration = duration;
        this.nowPlaying = nowPlaying;
    }

    public String getNowPlaying() {
        return nowPlaying;
    }

    public void setNowPlaying(String nowPlaying) {
        this.nowPlaying = nowPlaying;
    }

    public String getArtists_name() {
        return artists_name;
    }

    public void setArtists_name(String artists_name) {
        this.artists_name = artists_name;
    }


    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSong_id() {
        return song_id;
    }

    public void setSong_id(String song_id) {
        this.song_id = song_id;
    }

    public String getLike_status() {
        return like_status;
    }

    public void setLike_status(String like_status) {
        this.like_status = like_status;
    }

    public String getTotle_like() {
        return totle_like;
    }

    public void setTotle_like(String totle_like) {
        this.totle_like = totle_like;
    }

    public String getSong_name() {
        return song_name;
    }

    public void setSong_name(String song_name) {
        this.song_name = song_name;
    }

    public String getSong_url() {
        return song_url;
    }

    public void setSong_url(String song_url) {
        this.song_url = song_url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGenres_id() {
        return genres_id;
    }

    public void setGenres_id(String genres_id) {
        this.genres_id = genres_id;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }
}
