package com.djcristian.Model;

public class GsonPlaylistData {
    private String playlist_id;
    private String playlist_name;
    private String total_count;

    public String getPlaylist_id() {
        return playlist_id;
    }

    public void setPlaylist_id(String playlist_id) {
        this.playlist_id = playlist_id;
    }

    public String getPlaylist_name() {
        return playlist_name;
    }

    public void setPlaylist_name(String playlist_name) {
        this.playlist_name = playlist_name;
    }

    public String getTotal_count() {
        return total_count;
    }

    public void setTotal_count(String total_count) {
        this.total_count = total_count;
    }

    public GsonPlaylistData(String playlist_id, String playlist_name, String total_count) {
        this.playlist_id = playlist_id;
        this.playlist_name = playlist_name;
        this.total_count = total_count;
    }
}
