package com.djcristian.Model;

public class PojoMore {

    int list_img;
    String list_item;


    public PojoMore() {
    }

    public PojoMore(int list_img, String list_item) {
        this.list_img = list_img;
        this.list_item = list_item;
    }

    public int getList_img() {
        return list_img;
    }

    public void setList_img(int list_img) {
        this.list_img = list_img;
    }

    public String getList_item() {
        return list_item;
    }

    public void setList_item(String list_item) {
        this.list_item = list_item;
    }


}
