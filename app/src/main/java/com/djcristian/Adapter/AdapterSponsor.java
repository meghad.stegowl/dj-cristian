package com.djcristian.Adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.djcristian.Fragments.FragmentSingleSponsor;
import com.djcristian.Model.MoreSlider;
import com.djcristian.R;
import com.djcristian.utility.Utils;

import java.util.ArrayList;

public class AdapterSponsor extends RecyclerView.Adapter<AdapterSponsor.ViewHolder> {

    private ArrayList<MoreSlider> moreArrayList;
    private Context context;
    private FragmentManager fragmentManager;
    private RelativeLayout rl_fragments_header;


    public AdapterSponsor(ArrayList<MoreSlider> moreArrayList, Context context, FragmentManager fragmentManager, RelativeLayout rl_fragments_header) {
        this.moreArrayList = moreArrayList;
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.rl_fragments_header = rl_fragments_header;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView img;
        public TextView title, data;
        public RelativeLayout rl_main;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_title);
            img = (ImageView) itemView.findViewById(R.id.iv_icon);
            rl_main = (RelativeLayout) itemView.findViewById(R.id.rl_main);

        }
    }


    @Override
    public AdapterSponsor.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sponsor, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterSponsor.ViewHolder holder, int position) {
        final MoreSlider bean = moreArrayList.get(position);
        Utils.getInstance().loadGlide(context, holder.img, bean.getImage());
        holder.title.setText(bean.getTitle());
        holder.rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pushInnerFragment(new FragmentSingleSponsor(context, rl_fragments_header, false, bean.getId(), bean.getTitle()), "Single", true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moreArrayList.size();
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        final Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commitAllowingStateLoss();
    }

}