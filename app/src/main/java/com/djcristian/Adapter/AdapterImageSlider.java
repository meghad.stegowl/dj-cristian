package com.djcristian.Adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.djcristian.Model.More;
import com.djcristian.R;

import java.util.ArrayList;

public class AdapterImageSlider extends PagerAdapter {

    private ArrayList<View> views = new ArrayList<View>();
    private ArrayList<More> imageArraylist;
    private Context context;

    public AdapterImageSlider(ArrayList<More> imageArraylist, Context context) {
        this.context = context;
        this.imageArraylist = imageArraylist;
    }

    @Override
    public int getCount() {
        return imageArraylist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View rootView1 = inflater.inflate(R.layout.row_image_slider, container, false);
        TextView tv_title = (TextView) rootView1.findViewById(R.id.tv_title);
        ImageView iv_image = (ImageView) rootView1.findViewById(R.id.iv_image);
        More item = imageArraylist.get(position);
        tv_title.setText("/ / " + item.getTitle() + " \\\\");
        iv_image.setImageResource(item.getImage());
        container.addView(rootView1);
        return rootView1;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(Object object) {
        int index = views.indexOf(object);
        if (index == -1)
            return POSITION_NONE;
        else
            return index;
    }
}