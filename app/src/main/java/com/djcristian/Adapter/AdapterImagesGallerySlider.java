package com.djcristian.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.fragment.app.FragmentManager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.djcristian.Model.GalleryImages;
import com.djcristian.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class AdapterImagesGallerySlider extends RecyclerView.Adapter<AdapterImagesGallerySlider.ViewHolder> {

    private ArrayList<GalleryImages> arrayList;
    private FragmentManager fragmentManager;
    private Context context;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;

    private DisplayImageOptions options;

    public AdapterImagesGallerySlider(Context context, ArrayList<GalleryImages> arrayList) {
        this.context = context;
        this.drawerLayout = drawerLayout;
        this.toolbar = toolbar;
        this.arrayList = arrayList;
        this.fragmentManager = fragmentManager;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.cristian_placeholder)
                .showImageForEmptyUri(R.drawable.cristian_placeholder)
                .showImageOnFail(R.drawable.cristian_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_gallery_image;
        public ViewHolder(View itemView) {
            super(itemView);
            iv_gallery_image = (ImageView) itemView.findViewById(R.id.iv_gallery_image);

        }
    }

    @Override
    public AdapterImagesGallerySlider.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image_slider_gallery, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterImagesGallerySlider.ViewHolder holder, final int position) {
        GalleryImages bean = arrayList.get(position);
        final String name = bean.getImage();
        ImageLoader.getInstance().displayImage(name, holder.iv_gallery_image, options);
        holder.iv_gallery_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}

