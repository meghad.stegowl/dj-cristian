package com.djcristian.Adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.djcristian.Fragments.FragmentFeaturedDjsSong;
import com.djcristian.Model.GsonFeaturingDjs2;
import com.djcristian.R;
import com.djcristian.utility.Utils;

import java.util.ArrayList;


public class AdapterFeaturingDjs extends RecyclerView.Adapter<AdapterFeaturingDjs.ViewHolder> {

    private ArrayList<GsonFeaturingDjs2> arrayList;
    private FragmentManager fragmentManager;
    private Context context;
    private RelativeLayout rl_fragments_header;


    public AdapterFeaturingDjs(Context context, ArrayList<GsonFeaturingDjs2> arrayList, FragmentManager fragmentManager, RelativeLayout rl_fragments_header) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.arrayList = arrayList;
        this.fragmentManager = fragmentManager;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title, tv_count;
        RelativeLayout rl_main;
        public ImageView iv_img;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_count = (TextView) itemView.findViewById(R.id.tv_count);
            rl_main = (RelativeLayout) itemView.findViewById(R.id.rl_main);
            iv_img = (ImageView) itemView.findViewById(R.id.iv_icon);

        }
    }

    @Override
    public AdapterFeaturingDjs.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_featuring_djs, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterFeaturingDjs.ViewHolder holder, final int position) {
        final GsonFeaturingDjs2 bean = arrayList.get(position);
        final String name = bean.getName();
        Utils.getInstance().d("In dj apdater name : " + name);
        final String id = bean.getId();
        Utils.getInstance().d("In dj apdater insta url : " + bean.getInsta_url());
        holder.tv_title.setText(name);
        Utils.getInstance().loadGlide(context, holder.iv_img, bean.getImage());
        holder.rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushInnerFragment(new FragmentFeaturedDjsSong(context, rl_fragments_header, bean.getInsta_url()), "Featuring single", true, name, id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack, String name, String id) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Bundle args = new Bundle();
        args.putString("name", name);
        args.putString("id", id);
        fragment.setArguments(args);
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commitAllowingStateLoss();
    }
}
