package com.djcristian.Adapter;

import android.content.Context;
import android.graphics.Bitmap;

import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.djcristian.Fragments.FragmentSingleEvent_New_JP_DJ;
import com.djcristian.Model.GsonGetEvents2;
import com.djcristian.R;
import com.djcristian.utility.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class AdapterEventsDJ extends RecyclerView.Adapter<AdapterEventsDJ.ViewHolder> {

    private ArrayList<GsonGetEvents2> arrayList;
    private FragmentManager fragmentManager;
    private Context context;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private LinearLayout ll_header;
    private GsonGetEvents2 bean;
    private RelativeLayout rl_fragments_header;
    private LayoutInflater inflater;
    private DisplayImageOptions options;

    public AdapterEventsDJ(Context context, ArrayList<GsonGetEvents2> arrayList, FragmentManager fragmentManager, RelativeLayout rl_fragments_header){
        this.context = context;
        this.rl_fragments_header=rl_fragments_header;
        this.arrayList = arrayList;
        this.fragmentManager = fragmentManager;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_events_dj, parent, false);
        inflater = LayoutInflater.from(context);

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final GsonGetEvents2 bean = arrayList.get(position);
        final String date=bean.getStart();
        final String title=bean.getTitle();
        final String desc=bean.getDescription();
        final String image=bean.getImage();
        Utils.getInstance().d("date"+date);
        final String time=bean.getTime();

        holder.tv_events_heading_dj1.setText(bean.getTitle());
        holder.tv_events_address_dj2.setText(bean.getAddress());
        loadGlide(context,holder.img,bean.getImage());
        final String long_t = bean.getLongitude();
        final String lati = bean.getLatitude();
        holder.date.setText(date +" At "+ time);
        holder.ll_whole_event_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushInnerFragment(new FragmentSingleEvent_New_JP_DJ(context,rl_fragments_header,false,date,time,title,desc,image,lati,long_t,bean.getAddress()), "Single event", true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img,iv_events_arrow_dj,iv_like;
        public TextView tv_events_heading_dj1,date,tv_events_address_dj2,tv_genere_song_title,tv_address;
        LinearLayout ll_whole_event_item;
        public ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.iv_events_img_dj);
            tv_events_heading_dj1 = (TextView) itemView.findViewById(R.id.tv_events_heading_dj1);
            tv_events_address_dj2 = (TextView) itemView.findViewById(R.id.tv_events_address_dj2);
            date = (TextView) itemView.findViewById(R.id.date);
            ll_whole_event_item= (LinearLayout) itemView.findViewById(R.id.ll_whole_event_item);
        }
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }

    public void loadGlide(Context context, ImageView iv, String url) {
        try {
            Glide.with(context)
                    .load(url)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.cristian_placeholder)
                    .into(iv);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
