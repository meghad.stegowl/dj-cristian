package com.djcristian.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.djcristian.Fragments.FragmentSingleEvent;
import com.djcristian.Model.GsonGetEvents2;
import com.djcristian.R;
import com.djcristian.utility.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class AdapterEvents extends RecyclerView.Adapter<AdapterEvents.ViewHolder> {

    private ArrayList<GsonGetEvents2> arrayList = new ArrayList<>();
    private FragmentManager fragmentManager;
    private Context context;
    private DisplayImageOptions options;
    private RelativeLayout rl_fragments_header;

    public AdapterEvents(Context context, ArrayList<GsonGetEvents2> arrayList, FragmentManager fragmentManager, RelativeLayout rl_fragments_header) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.arrayList = arrayList;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public AdapterEvents.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_events_dj, parent, false);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.andu_placeholder)
                .showImageForEmptyUri(R.drawable.andu_placeholder)
                .showImageOnFail(R.drawable.andu_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterEvents.ViewHolder holder, final int position) {
        final GsonGetEvents2 bean = arrayList.get(position);
        ImageLoader.getInstance().displayImage(bean.getImage(), holder.img, options);
        String date = bean.getStart();
        Utils.getInstance().d("date" + date);
        String time = bean.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date1 = null;
        try {
            date1 = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat month_date = new SimpleDateFormat("MMM dd,yyyy", Locale.ENGLISH);
        String month_name = month_date.format(date1);
        holder.tv_date_and_time.setText(month_name + " at " + time);
        holder.tv_events_heading_dj1.setText(bean.getTitle());
        holder.tv_events_address_dj2.setText(bean.getAddress());
        holder.ll_whole_event_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushFragment(new FragmentSingleEvent(context, rl_fragments_header, bean), "Single event", true);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img, iv_events_arrow_dj;
        public TextView tv_events_heading_dj1, tv_events_address_dj2, tv_date_and_time;
        LinearLayout ll_whole_event_item;

        public ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.iv_events_img_dj);
            tv_events_heading_dj1 = (TextView) itemView.findViewById(R.id.tv_events_heading_dj1);
            tv_events_address_dj2 = (TextView) itemView.findViewById(R.id.tv_events_address_dj2);
            tv_date_and_time = (TextView) itemView.findViewById(R.id.date);
            ll_whole_event_item = (LinearLayout) itemView.findViewById(R.id.ll_whole_event_item);
        }
    }

    private void pushFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commitAllowingStateLoss();
    }
}
