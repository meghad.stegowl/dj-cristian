package com.djcristian.Adapter;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.djcristian.Fragments.FragmentVideos;
import com.djcristian.Model.PojoVideoMovies;
import com.djcristian.R;
import com.djcristian.utility.Utils;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;



public class AdapterVideoMovies extends RecyclerView.Adapter<AdapterVideoMovies.ViewHolder> {

    private ArrayList<PojoVideoMovies> arrayList;
    private Context mContext;
    private FragmentManager fragmentManager;
    private RelativeLayout rl_fragment_header;
    private AdView mAdView;

    public AdapterVideoMovies(Context mContext, ArrayList<PojoVideoMovies> arrayList, FragmentManager fragmentManager, RelativeLayout rl_fragment_header, AdView mAdView) {
        this.arrayList = arrayList;
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
        this.rl_fragment_header = rl_fragment_header;
        this.mAdView=mAdView;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_video_movies, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PojoVideoMovies bean = arrayList.get(position);
        holder.tv_user_name.setText(bean.getName());
        String date = bean.getDate();
        Utils.getInstance().d("date : " + date);
        holder.tv_date.setText(date);
        holder.tv_user_name.setText(bean.getName());
        Utils.getInstance().loadGlide(mContext, holder.iv_user_emoji, bean.getImage());
        holder.ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pushInnerFragment(new FragmentVideos(mContext, rl_fragment_header, bean.getName(), bean.getData_link(),mAdView), "Single Video", true);

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_user_name, tv_comment, tv_date, tv_time;
        public ImageView iv_user_emoji;
        public RelativeLayout ll_main;


        public ViewHolder(View itemView) {
            super(itemView);
            tv_user_name = (TextView) itemView.findViewById(R.id.tv_user_name);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
            tv_comment = (TextView) itemView.findViewById(R.id.tv_comment);
            iv_user_emoji = (ImageView) itemView.findViewById(R.id.iv_user_emoji);
            ll_main = (RelativeLayout) itemView.findViewById(R.id.ll_main);
        }
    }

    public void addItem(PojoVideoMovies item) {
        arrayList.add(item);
        notifyDataSetChanged();
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commitAllowingStateLoss();
    }
}
