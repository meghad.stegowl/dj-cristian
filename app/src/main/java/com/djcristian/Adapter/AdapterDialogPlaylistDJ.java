package com.djcristian.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.djcristian.Model.PojoAddToPlaylist;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class AdapterDialogPlaylistDJ extends RecyclerView.Adapter<AdapterDialogPlaylistDJ.ViewHolder> {

    private ArrayList<PojoAddToPlaylist> arrayList;
    private Context mContext;
    private PojoAddToPlaylist bean;
    private AppCompatDialog dialog3;
    ProgressDialog mProgressDialog;

    public AdapterDialogPlaylistDJ(ArrayList<PojoAddToPlaylist> arrayList, Context mContext, AppCompatDialog dialog3) {
        this.arrayList = arrayList;
        this.mContext = mContext;
        this.dialog3 = dialog3;
    }

    @Override
    public AdapterDialogPlaylistDJ.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dialog_playlist, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterDialogPlaylistDJ.ViewHolder holder, final int position) {

        final PojoAddToPlaylist bean = arrayList.get(position);
        Utils.getInstance().d("mytag" +bean.getPlaylist_name());
        holder.tv_playlist_name_dialog.setText(bean.getPlaylist_name());
        holder.tv_playlist_name_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(mContext)) {
                    addSongToPlaylist(arrayList.get(position));
                } else {
                    Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void addSongToPlaylist(final PojoAddToPlaylist bean) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog3.dismiss();
                mProgressDialog = new ProgressDialog(mContext);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("u_id", Prefs.getPrefInstance().getValue(mContext, Const.USER_ID, ""));
                    jsonObject.put("song_id", bean.getS_id());
                    jsonObject.put("playlist_id", bean.getPlaylist_id());
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.ADD_NEW_SONG, jsonObject.toString());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                Log.i("mytag", "inside class:FragmentGenresDJ \t " + response);
                JSONObject myJsonObject;
                try {
                    myJsonObject = new JSONObject(response);
                    if (myJsonObject.getString("status").equals("1")) {
                        mProgressDialog.dismiss();
                        Utils.getInstance().toast((Activity) mContext, myJsonObject.getString("msg"));
                    } else {
                        Utils.getInstance().toast((Activity) mContext, myJsonObject.getString("msg"));
                        Utils.getInstance().d("failed");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_playlist_name_dialog;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_playlist_name_dialog = (TextView) itemView.findViewById(R.id.tv_playlist_name_dialog);
        }
    }
}
