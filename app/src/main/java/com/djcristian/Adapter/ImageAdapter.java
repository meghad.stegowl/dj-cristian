package com.djcristian.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.djcristian.Model.PojoAlbum;
import com.djcristian.R;
import com.djcristian.utility.Utils;

import java.util.ArrayList;


public class ImageAdapter extends BaseAdapter {
    Context context;
    ArrayList<PojoAlbum> emojiArrayList;
    private int selectedItem = 0;

    public ImageAdapter(Context context, ArrayList<PojoAlbum> emojiArrayList) {
        this.context = context;
        this.emojiArrayList = emojiArrayList;
    }

    @Override
    public int getCount() {
        return emojiArrayList.size();

    }

    public void setSelectedItem(int selectedItem) {
        this.selectedItem = selectedItem;
        notifyDataSetChanged();
    }

    @Override
    public Object getItem(int position) {
        return position;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View rootView = inflater.inflate(R.layout.row_slider, parent, false);
        ImageView iv_galler = (ImageView) rootView.findViewById(R.id.iv_galler);
        ImageView iv_glow = (ImageView) rootView.findViewById(R.id.iv_glow);
        PojoAlbum item = emojiArrayList.get(position);
        iv_glow.setImageResource(R.drawable.img_glow);
        Utils.getInstance().loadGlide(context, iv_galler, item.getImage());
        if (position == selectedItem) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(300, 300);
            RelativeLayout.LayoutParams layoutParamsGlow = new RelativeLayout.LayoutParams(330, 330);
            layoutParams.setMargins(20, 20, 20, 20);
            layoutParamsGlow.setMargins(10, 10, 10, 10);
            iv_galler.setLayoutParams(layoutParams);
            iv_glow.setLayoutParams(layoutParamsGlow);
            iv_glow.setVisibility(View.VISIBLE);
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(250, 250);
            layoutParams.setMargins(10, 10, 10, 10);
            iv_galler.setLayoutParams(layoutParams);
            iv_glow.setVisibility(View.GONE);
        }
        return rootView;
    }

}