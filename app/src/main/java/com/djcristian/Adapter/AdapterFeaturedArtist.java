package com.djcristian.Adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.djcristian.Fragments.FragmentFeaturedArtistSongs;
import com.djcristian.Model.PojoAlbum;
import com.djcristian.R;
import com.djcristian.utility.Utils;

import java.util.ArrayList;


public class AdapterFeaturedArtist extends RecyclerView.Adapter<AdapterFeaturedArtist.ViewHolder> {

    private ArrayList<PojoAlbum> arrayList;
    private FragmentManager fragmentManager;
    private Context context;
    private RelativeLayout rl_fragments_header;


    public AdapterFeaturedArtist(Context context, ArrayList<PojoAlbum> arrayList, FragmentManager fragmentManager, RelativeLayout rl_fragments_header) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.arrayList = arrayList;
        this.fragmentManager = fragmentManager;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title, tv_count;
        RelativeLayout rl_main;
        public ImageView iv_icon;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_count = (TextView) itemView.findViewById(R.id.tv_count);
            iv_icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            rl_main = (RelativeLayout) itemView.findViewById(R.id.rl_main);
        }
    }

    @Override
    public AdapterFeaturedArtist.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_featuring_djs, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterFeaturedArtist.ViewHolder holder, final int position) {
        final PojoAlbum bean = arrayList.get(position);
        final String name = bean.getGenres_name();
        final String id = bean.getGenres_id();
        holder.tv_title.setText(name);
        Utils.getInstance().loadGlide(context, holder.iv_icon, bean.getImage());
        holder.rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushInnerFragment(new FragmentFeaturedArtistSongs(context, rl_fragments_header, fragmentManager, bean.getInsta_url()), "artistsong", true, name, id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack, String name, String id) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Bundle args = new Bundle();
        args.putString("name", name);
        args.putString("id", id);
        fragment.setArguments(args);
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commitAllowingStateLoss();
    }
}
