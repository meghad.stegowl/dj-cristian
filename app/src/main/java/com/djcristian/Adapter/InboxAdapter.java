package com.djcristian.Adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.djcristian.Model.GsonInboxPojo;
import com.djcristian.R;

import java.util.ArrayList;

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.ViewHolder> {

    private ArrayList<GsonInboxPojo> arrayList;
    private FragmentManager fragmentManager;
    private Context context;
    private Activity caller;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private LinearLayout ll_header;
    private GsonInboxPojo bean;

    public InboxAdapter(Context context, ArrayList<GsonInboxPojo> arrayList, FragmentManager fragmentManager, LinearLayout ll_header, Toolbar toolbar, DrawerLayout drawerLayout) {
        this.context = context;
        this.arrayList = arrayList;
        this.fragmentManager = fragmentManager;
        this.ll_header = ll_header;
        this.toolbar = toolbar;
        this.drawerLayout = drawerLayout;
    }

    @Override
    public InboxAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_inbox, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(InboxAdapter.ViewHolder holder, final int position) {
        bean = arrayList.get(position);
        String msg = bean.getMsg().toString();
        Log.i("mytag", "Message : " + msg);
//        String time = bean.getTime().toString();
//        String date = bean.getDate().toString();
        holder.tv_msg.setText(msg);
//        holder.tv_date.setText(date);
//        holder.tv_time.setText(time);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_msg, tv_time, tv_date;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_msg = (TextView) itemView.findViewById(R.id.tv_msg);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);

        }
    }


}
