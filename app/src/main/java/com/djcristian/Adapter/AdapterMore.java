package com.djcristian.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.djcristian.Model.More;
import com.djcristian.R;

import java.util.ArrayList;

public class AdapterMore extends RecyclerView.Adapter<AdapterMore.ViewHolder> {

    private ArrayList<More> moreArrayList;
    private Context context;


    public AdapterMore(ArrayList<More> moreArrayList, Context context) {
        this.moreArrayList = moreArrayList;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_title;
        ImageView iv_menu_icon;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            iv_menu_icon = (ImageView) itemView.findViewById(R.id.iv_menu_icon);
        }
    }


    @Override
    public AdapterMore.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_more, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterMore.ViewHolder holder, int position) {
        More item = moreArrayList.get(position);
        holder.tv_title.setText(item.getTitle());
        holder.iv_menu_icon.setImageResource(item.getImage());
    }

    @Override
    public int getItemCount() {
        return moreArrayList.size();
    }


}