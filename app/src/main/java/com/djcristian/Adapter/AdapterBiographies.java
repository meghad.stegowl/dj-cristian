package com.djcristian.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.djcristian.Fragments.FragmentSingleBio;
import com.djcristian.Model.Biographies;
import com.djcristian.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;


public class AdapterBiographies extends RecyclerView.Adapter<AdapterBiographies.ViewHolder> {

    private ArrayList<Biographies> arrayList;
    private FragmentManager fragmentManager;
    private Context context;
    private RelativeLayout rl_fragments_header;
    private DisplayImageOptions options;


    public AdapterBiographies(Context context, ArrayList<Biographies> arrayList, FragmentManager fragmentManager, RelativeLayout rl_fragments_header) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.arrayList = arrayList;
        this.fragmentManager = fragmentManager;

    }

    public void removeItem(int i) {
        if (arrayList != null && i >= 0 && i <= arrayList.size()) {
            arrayList.remove(i);
            notifyDataSetChanged();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title, tv_count;
        RelativeLayout rl_main;
        ImageView iv_delete, iv_icon;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_count = (TextView) itemView.findViewById(R.id.tv_count);
            rl_main = (RelativeLayout) itemView.findViewById(R.id.rl_main);
            iv_delete = (ImageView) itemView.findViewById(R.id.iv_delete);
            iv_icon = (ImageView) itemView.findViewById(R.id.iv_icon);
        }
    }

    @Override
    public AdapterBiographies.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_biograpies, parent, false);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.andu_placeholder)
                .showImageForEmptyUri(R.drawable.andu_placeholder)
                .showImageOnFail(R.drawable.andu_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterBiographies.ViewHolder holder, final int position) {
        final Biographies bean = arrayList.get(position);
        holder.tv_title.setText(bean.getArtist_name());
        ImageLoader.getInstance().displayImage(bean.getThumb(), holder.iv_icon, options);
        holder.rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushInnerFragment(new FragmentSingleBio(context, rl_fragments_header, false, bean), "bio single", true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commitAllowingStateLoss();
    }
}
