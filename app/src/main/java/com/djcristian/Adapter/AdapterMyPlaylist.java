package com.djcristian.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.djcristian.Fragments.FragmentMyPlaylistSong;
import com.djcristian.Model.GsonPlaylistData;
import com.djcristian.R;
import com.djcristian.utility.Const;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class AdapterMyPlaylist extends RecyclerView.Adapter<AdapterMyPlaylist.ViewHolder> {

    private ArrayList<GsonPlaylistData> arrayList;
    private FragmentManager fragmentManager;
    private Context context;
    private RelativeLayout rl_fragments_header;
    private ProgressDialog mProgressDialog, mProgressDialog1;


    public AdapterMyPlaylist(Context context, ArrayList<GsonPlaylistData> arrayList, FragmentManager fragmentManager, RelativeLayout rl_fragments_header) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.arrayList = arrayList;
        this.fragmentManager = fragmentManager;

    }

    public void removeItem(int i) {
        if (arrayList != null && i >= 0 && i <= arrayList.size()) {
            arrayList.remove(i);
            notifyDataSetChanged();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title, tv_count;
        RelativeLayout rl_main;
        ImageView iv_delete;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_count = (TextView) itemView.findViewById(R.id.tv_count);
            rl_main = (RelativeLayout) itemView.findViewById(R.id.rl_main);
            iv_delete = (ImageView) itemView.findViewById(R.id.iv_delete);
        }
    }

    @Override
    public AdapterMyPlaylist.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_my_playlist, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterMyPlaylist.ViewHolder holder, final int position) {
        GsonPlaylistData bean = arrayList.get(position);
        final String name = bean.getPlaylist_name();
        final String id = bean.getPlaylist_id();
        String count = bean.getTotal_count();
        holder.tv_title.setText(name);
        holder.tv_count.setText(count);
        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("");
                alertDialog.setMessage("Are you sure want to delete?");
                alertDialog.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                deletePlaylist(position, id);
                            }
                        });
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
        holder.rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushInnerFragment(new FragmentMyPlaylistSong(context, rl_fragments_header), "playlist", true, name, id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @SuppressLint("StaticFieldLeak")
    private void deletePlaylist(final int pos, final String playListId) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("playlist_id", playListId);
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.DELETE_PLAYLIST, jsonObject.toString());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("Geners Response:: " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                Log.i("mytag", "inside class:FragmentGenresDJ \t " + response);
                try {
                    JSONObject mJsonObject1 = new JSONObject(response);
                    if (mJsonObject1.getString("status").equals("1")) {
                        removeItem(pos);
                    }
                    Utils.getInstance().toast((Activity) context, mJsonObject1.getString("msg"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }
    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack, String name, String id) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Bundle args = new Bundle();
        args.putString("name", name);
        args.putString("id", id);
        fragment.setArguments(args);
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commitAllowingStateLoss();
    }
}
