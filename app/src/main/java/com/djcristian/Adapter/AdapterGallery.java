package com.djcristian.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.djcristian.CustomView.RecyclerViewPositionHelper;
import com.djcristian.Fragments.FragmentGalleryNew;
import com.djcristian.Model.GalleryFolder;
import com.djcristian.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;


public class AdapterGallery extends RecyclerView.Adapter<AdapterGallery.ViewHolder> {

    private ArrayList<GalleryFolder> arrayList;
    private FragmentManager fragmentManager;
    private Context context;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private RelativeLayout r1_fragment_header;
    private RecyclerViewPositionHelper mRecyclerViewHelper;
    private DisplayImageOptions options;

    public AdapterGallery(Context context, RelativeLayout r1_fragments_header, ArrayList<GalleryFolder> arrayList, FragmentManager fragmentManager) {
        this.context = context;
        this.r1_fragment_header = r1_fragments_header;
        this.arrayList = arrayList;
        this.fragmentManager = fragmentManager;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.cristian_placeholder)
                .showImageForEmptyUri(R.drawable.cristian_placeholder)
                .showImageOnFail(R.drawable.cristian_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_gallery_image;
        RelativeLayout rel_row;
        TextView tv_foldername;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_gallery_image = (ImageView) itemView.findViewById(R.id.folder_image);
            tv_foldername = (TextView) itemView.findViewById(R.id.folder_name);
            rel_row = (RelativeLayout) itemView.findViewById(R.id.rel_row);

        }
    }

    @Override
    public AdapterGallery.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_gallery, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterGallery.ViewHolder holder, final int position) {
        GalleryFolder bean = arrayList.get(position);
        final String image = bean.getImage();
        final String name = bean.getName();
        final String fid = bean.getF_id();
        ImageLoader.getInstance().displayImage(image, holder.iv_gallery_image, options);
        holder.tv_foldername.setText(name);
        holder.rel_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pushInnerFragment(new FragmentGalleryNew(context, r1_fragment_header, false, fid, name), "GALLERY", true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }
}

