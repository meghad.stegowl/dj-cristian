package com.djcristian.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.djcristian.Model.GsonComment2;
import com.djcristian.R;

import java.util.ArrayList;


public class AdapterCommentList extends RecyclerView.Adapter<AdapterCommentList.ViewHolder> {

    private ArrayList<GsonComment2> arrayList;
    private Context mContext;

    public AdapterCommentList(ArrayList<GsonComment2> arrayList, Context mContext) {
        this.arrayList = arrayList;
        this.mContext = mContext;
    }


    @Override
    public AdapterCommentList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_comment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterCommentList.ViewHolder holder, int position) {
        GsonComment2 bean = arrayList.get(position);
        holder.tv_comment.setText(bean.getComment());
        holder.tv_user_name.setText(bean.getUser_id());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_user_name, tv_comment;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_user_name = (TextView) itemView.findViewById(R.id.tv_user_name);
            tv_comment = (TextView) itemView.findViewById(R.id.tv_comment);
        }
    }

    public void addItem(GsonComment2 item) {
        arrayList.add(item);
        notifyDataSetChanged();
    }
}
