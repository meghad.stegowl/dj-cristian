package com.djcristian.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.fragment.app.FragmentManager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.djcristian.Model.MoreSlider;
import com.djcristian.R;
import com.djcristian.utility.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.ArrayList;

public class AdapterImagesSponsorSlider extends RecyclerView.Adapter<AdapterImagesSponsorSlider.ViewHolder> {

    private ArrayList<MoreSlider> arrayList;
    private FragmentManager fragmentManager;
    private Context context;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;

    private DisplayImageOptions options;

    public AdapterImagesSponsorSlider(Context context, ArrayList<MoreSlider> arrayList) {
        this.context = context;
        this.drawerLayout = drawerLayout;
        this.toolbar = toolbar;
        this.arrayList = arrayList;
        this.fragmentManager = fragmentManager;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.cristian_placeholder)
                .showImageForEmptyUri(R.drawable.cristian_placeholder)
                .showImageOnFail(R.drawable.cristian_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_gallery_image;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_gallery_image = (ImageView) itemView.findViewById(R.id.iv_gallery_image);

        }
    }

    @Override
    public AdapterImagesSponsorSlider.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image_slider_gallery, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterImagesSponsorSlider.ViewHolder holder, final int position) {
        MoreSlider bean = arrayList.get(position);
        Utils.getInstance().loadGlide(context, holder.iv_gallery_image, bean.getImage());
        holder.iv_gallery_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}

