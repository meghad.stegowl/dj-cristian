package com.djcristian.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;

import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatDialog;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.djcristian.Activities.HomeActivity;
import com.djcristian.Fragments.FragmentMyFav;
import com.djcristian.Manager.MediaController;
import com.djcristian.Model.GsonComment2;
import com.djcristian.Model.GsonFavDjsSongs2;
import com.djcristian.Model.GsonGetPlaylist1;
import com.djcristian.Model.GsonGetPlaylist2;
import com.djcristian.Model.PojoAddToPlaylist;
import com.djcristian.Model.PojoSongForPlayer;
import com.djcristian.R;
import com.djcristian.phonemidea.PhoneMediaControl;
import com.djcristian.utility.BufferData;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.RestClient;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class AdapterMyFav extends RecyclerView.Adapter<AdapterMyFav.ViewHolder> {

    private FragmentManager fragmentManager;
    private Context context;
    private DisplayImageOptions options;
    private ProgressDialog mProgressDialog, mProgressDialog1;
    RestClient restClient;
    private String duration, fav_id;
    private ImageView menu, offradio;

    private ArrayList<GsonFavDjsSongs2> arrayList;
    ArrayList<PojoSongForPlayer> sngList;

    public AdapterMyFav(Context context, ArrayList<GsonFavDjsSongs2> arrayList, FragmentManager fragmentManager) {
        this.context = context;
        this.arrayList = arrayList;
        this.fragmentManager = fragmentManager;
        this.fav_id = fav_id;

    }

    public void removeItem(int i) {
        if (arrayList != null && i >= 0 && i <= arrayList.size()) {
            arrayList.remove(i);
            notifyDataSetChanged();

        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img, iv_like, iv_share, iv_comment, iv_fav_option_menu, small_gif;
        TextView songName, tv_count_like, tv_count_share, tv_count_comment, tv_duration, tv_artist_name;
        RelativeLayout rl_like, rl_share, rl_comment;
        LinearLayout ll_main;

        public ViewHolder(View itemView) {
            super(itemView);
            small_gif = (ImageView) itemView.findViewById(R.id.img_smallgif);
            tv_duration = (TextView) itemView.findViewById(R.id.tv_duration);
            img = (ImageView) itemView.findViewById(R.id.iv_favorite_img_dj);
            songName = (TextView) itemView.findViewById(R.id.tv_favorite_song_anme_dj);
            tv_artist_name = (TextView) itemView.findViewById(R.id.tv_artist_name);
            rl_like = (RelativeLayout) itemView.findViewById(R.id.rl_fav_like);
            rl_share = (RelativeLayout) itemView.findViewById(R.id.rl_fav_share);
            rl_comment = (RelativeLayout) itemView.findViewById(R.id.rl_fav_comment);
            iv_like = (ImageView) itemView.findViewById(R.id.iv_like);
            tv_count_like = (TextView) itemView.findViewById(R.id.tv_fav_like_count);
            iv_fav_option_menu = (ImageView) itemView.findViewById(R.id.iv_fav_option_menu);
            ll_main = (LinearLayout) itemView.findViewById(R.id.ll_main);
        }
    }

    @Override
    public AdapterMyFav.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_featuring_dg_song_new, parent, false);
        sngList = new ArrayList<PojoSongForPlayer>();
        GsonFavDjsSongs2 myBean;
        for (int i = 0; i < arrayList.size(); i++) {
            myBean = arrayList.get(i);
            duration = myBean.getDuration();
            String durInMili = String.valueOf(Utils.getInstance().strToMilli(duration));
            PojoSongForPlayer mDetail = new PojoSongForPlayer(myBean.getName(), durInMili, myBean.getUrl(), myBean.getImage(), myBean.getArtists_name(), myBean.getSong_id());
            sngList.add(mDetail);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AdapterMyFav.ViewHolder holder, final int position) {
        final GsonFavDjsSongs2 bean = arrayList.get(position);
        holder.songName.setText(bean.getName());
        holder.songName.setSelected(true);
        holder.tv_duration.setText(bean.getDuration());
        holder.tv_artist_name.setText(bean.getArtists_name());
        holder.tv_artist_name.setSelected(true);
        String nowPlaying = bean.getNowPlaying();
        DrawableImageViewTarget imageViewTarget = new DrawableImageViewTarget(holder.small_gif);
        Glide.with(context)
                .load(R.drawable.small_kenny)
                .fitCenter()
                .into(imageViewTarget);
        Utils.getInstance().loadGlide(context, holder.img, bean.getImage());
        if (nowPlaying.equals("1")) {
            holder.small_gif.setVisibility(View.VISIBLE);
        } else {
            holder.small_gif.setVisibility(View.INVISIBLE);
        }
        holder.tv_count_like.setText(bean.getTotle_like());
        if (bean.getLike_status().equals("1")) {
            holder.iv_like.setColorFilter(context.getResources().getColor(R.color.colorAccent));
            holder.tv_count_like.setTextColor(context.getResources().getColor(R.color.colorAccent));
        } else {
            holder.iv_like.setColorFilter(context.getResources().getColor(R.color.white));
            holder.tv_count_like.setTextColor(context.getResources().getColor(R.color.white));
        }
        holder.rl_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String song_name = bean.getSong_name();
                String Artist_name = bean.getArtists_name();
                String Song_url = bean.getSong_url();
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = ("Song Name: " + song_name + "\n\n" + "Song Url: " + Song_url + "\n\n" + "Artist Name: " + Artist_name + "\n\n" + "App Name: " + "By DJ Cristian");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });
        holder.rl_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    changeLikeStatus(arrayList.get(position), holder.iv_like, holder.tv_count_like);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.rl_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    showAndDoComments(arrayList.get(position));
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.iv_fav_option_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    showOptionDialog(arrayList.get(position), position);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) //returns true if internet available
                {
                    notifyDataSetChanged();
                    final PojoSongForPlayer mDetail = sngList.get(position);
                    if (holder.small_gif.getVisibility() == View.VISIBLE) {
                        HomeActivity.visiblePlayerTwo();
                    } else {
                        if (mDetail != null) {
                            final ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                            if (progressDialog != null) {
                                Utils.getInstance().d("not null");
                                if (!progressDialog.isShowing()) {
                                    Utils.getInstance().d("not showing");
                                    if (CheckNetwork.isInternetAvailable(context)) {
                                        progressDialog.show();
                                        HomeActivity.visiblePlayerTwo();
                                        progressDialog.setCanceledOnTouchOutside(true);
                                    } else {
                                        progressDialog.dismiss();
                                        Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                                    }
                                    if (MediaController.getInstance().isPlayingAudio(mDetail) && !MediaController.getInstance().isAudioPaused()) {
                                        MediaController.getInstance().pauseAudio(mDetail);
                                    } else {
                                        MediaController.getInstance().setPlaylist(sngList, mDetail, PhoneMediaControl.SonLoadFor.All.ordinal(), -1);
                                    }
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (progressDialog != null) {
                                                if (progressDialog.isShowing()) {
                                                    new Handler().postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            String duration1 = duration;
                                                            String strTime = duration1.substring(duration1.length() - 2);
                                                            int sec1 = mDetail.audioProgressSec % 60;
                                                            int sec = Integer.parseInt(strTime);
                                                            if (sec > 1 && sec1 > 1) {
                                                                progressDialog.dismiss();
                                                            }
                                                        }
                                                    }, 100);
                                                }
                                            }
                                        }
                                    }, 200);
                                }
                            }
                        }
                    }
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showOptionDialog(final GsonFavDjsSongs2 bean, final int pos) {
        final AppCompatDialog dialog1;
        dialog1 = new AppCompatDialog(context, R.style.dialog);
        dialog1.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.custom_dialog_songs_options);
        dialog1.show();
        TextView tv_custom_dilog_add_playlist = (TextView) dialog1.findViewById(R.id.tv_custom_dilog_add_playlist);
        TextView tv_custom_dilog_remove_from_fav = (TextView) dialog1.findViewById(R.id.tv_custom_dilog_remove_from_fav);

        tv_custom_dilog_add_playlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    dialog1.dismiss();
                    AddToPlaylist(bean);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tv_custom_dilog_remove_from_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    dialog1.dismiss();
                    RemoveFromFav(bean, pos);

                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void RemoveFromFav(final GsonFavDjsSongs2 bean, final int pos) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("fav_id", bean.getFav_id());
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.REMOVE_FROM_FAV, jsonObject.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                Log.i("mytag", "inside class:FragmentGenresDJ \t " + response);
                try {
                    JSONObject mJsonObject = new JSONObject(response);
                    if (mJsonObject.getString("status").equals("1")) {
                        Utils.getInstance().toast((Activity) context, "Removed Successfully");
                        arrayList.remove(pos);
                        notifyItemRemoved(pos);
                        FragmentMyFav.tv_name.setText(arrayList.size() + " Tracks");
//                        removeItem(pos);
                    } else {
                        Utils.getInstance().toast((Activity) context, "Failed");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();

    }

    @SuppressLint("StaticFieldLeak")
    private void AddToFav(final GsonFavDjsSongs2 bean) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("u_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    jsonObject.put("s_id", bean.getSong_id());
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.ADD_TO_FAV, jsonObject.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                Log.i("mytag", "inside class:FragmentGenresDJ \t " + response);
                try {
                    JSONObject mJsonObject = new JSONObject(response);
                    if (mJsonObject.getString("status").equals("1")) {
                        Utils.getInstance().toast((Activity) context, mJsonObject.getString("msg"));
                    } else {
                        Utils.getInstance().toast((Activity) context, mJsonObject.getString("msg"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    private void AddToPlaylist(final GsonFavDjsSongs2 bean) {
        final AppCompatDialog dialog2;
        dialog2 = new AppCompatDialog(context, R.style.dialog);
        dialog2.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.custom_dialog_playlist_opt);
        dialog2.show();
        TextView tv_custom_dilog_existing_playlist = (TextView) dialog2.findViewById(R.id.tv_custom_dilog_existing_playlist);
        TextView tv_custom_dilog_new_playlist = (TextView) dialog2.findViewById(R.id.tv_custom_dilog_new_playlist);
        tv_custom_dilog_existing_playlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    dialog2.dismiss();
                    showExistingPlaylist(bean);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tv_custom_dilog_new_playlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    dialog2.dismiss();
                    createNewPlaylist(bean);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void createNewPlaylist(final GsonFavDjsSongs2 bean) {
        final AppCompatDialog dialog4;
        dialog4 = new AppCompatDialog(context, R.style.dialog);
        dialog4.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog4.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog4.setContentView(R.layout.custom_dialog_create_new_playlist);
        dialog4.show();
        final EditText et_dialog_create_playlist = (EditText) dialog4.findViewById(R.id.et_dialog_create_playlist);
        Button btn_dialog_create_playlist = (Button) dialog4.findViewById(R.id.btn_dialog_create_playlist);
//        et_dialog_create_playlist.setFilters(new InputFilter[]{filter});
        et_dialog_create_playlist.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0 && s.subSequence(0, 1).toString().equalsIgnoreCase(" ")) {
                    et_dialog_create_playlist.setText("");
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        btn_dialog_create_playlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!et_dialog_create_playlist.getText().toString().equals("")) {
                    if (CheckNetwork.isInternetAvailable(context)) {
                        dialog4.dismiss();
                        callApiToCreateNewPlaylist(Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""), et_dialog_create_playlist.getText().toString(), bean);
                    } else {
                        Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    et_dialog_create_playlist.setError("Please Enter Playlist Name");
                    et_dialog_create_playlist.requestFocus();
                }
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void callApiToCreateNewPlaylist(final String u_id, final String playlist_name, final GsonFavDjsSongs2 bean) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("u_id", u_id);
                    jsonObject.put("name", playlist_name);
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.CREATE_PLAYLIST, jsonObject.toString());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                Log.i("mytag", "inside class:FragmentGenresDJ \t " + response);
                try {
                    JSONObject mJsonObject = new JSONObject(response);
                    if (mJsonObject.getString("status").equals("1")) {
                        Utils.getInstance().toast((Activity) context, mJsonObject.getString("msg"));
                        showExistingPlaylist(bean);
                    } else {
                        Utils.getInstance().toast((Activity) context, mJsonObject.getString("msg"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    private void showExistingPlaylist(GsonFavDjsSongs2 bean) {
        final AppCompatDialog dialog3;
        dialog3 = new AppCompatDialog(context, R.style.dialog);
        dialog3.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog3.setContentView(R.layout.custom_dialog_existing_playlist);
        dialog3.show();
        RecyclerView rv_ext_playlist = (RecyclerView) dialog3.findViewById(R.id.rv_existing_playlist);
        rv_ext_playlist.setLayoutManager(new LinearLayoutManager(context));
        getExistingPlaylist(rv_ext_playlist, bean, dialog3);
    }

    private void getExistingPlaylist(final RecyclerView rv, final GsonFavDjsSongs2 bean1, final AppCompatDialog dialog3) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Loading");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();
        restClient = RestClient.getInstance();
        RestClient.setRestClientInterface(new RestClient.getRestClientInterface() {
            @Override
            public void onSuccess(String jsonResponse) {
                Gson gson = new Gson();
                Utils.getInstance().d("mytag" + jsonResponse);
                GsonGetPlaylist1 mGsonGetPlaylist1 = gson.fromJson(jsonResponse, GsonGetPlaylist1.class);
                ArrayList<GsonGetPlaylist2> gsonGetPlaylist2ArrayList = new ArrayList<>();
                ArrayList<PojoAddToPlaylist> mPojoAddToPlaylists = new ArrayList<>();
                PojoAddToPlaylist bean;
                gsonGetPlaylist2ArrayList = mGsonGetPlaylist1.getData();
                String status = mGsonGetPlaylist1.getStatus();
                String msg =mGsonGetPlaylist1.getMsg();
                if(status.equals("1")) {
                    for (int i = 0; i < gsonGetPlaylist2ArrayList.size(); i++) {
                        bean = new PojoAddToPlaylist(Const.USER_ID, bean1.getSong_id(), gsonGetPlaylist2ArrayList.get(i).getPlaylist_id(), gsonGetPlaylist2ArrayList.get(i).getPlaylist_name());
                        mPojoAddToPlaylists.add(bean);
                        Utils.getInstance().d("mytag" + gsonGetPlaylist2ArrayList.get(i).getPlaylist_name());
                    }
                    rv.setLayoutManager(new LinearLayoutManager(context));
                    AdapterDialogPlaylistDJ adapter = new AdapterDialogPlaylistDJ(mPojoAddToPlaylists, context, dialog3);
                    rv.setAdapter(adapter);
                    mProgressDialog.dismiss();
                }else{
                    dialog3.dismiss();
                    Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFail(String msg) {
                mProgressDialog.dismiss();
                dialog3.dismiss();
                Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNetworkError(String msg) {
                Utils.getInstance().toast((Activity) context, msg);
                mProgressDialog.dismiss();
            }
        });
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("u_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
            Utils.getInstance().d("mytag" + jsonObject.toString());
            restClient.doPostRequest(context, Const.GET_PLAYLIST, jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showAndDoComments(final GsonFavDjsSongs2 bean) {
        final AppCompatDialog dialog;
        dialog = new AppCompatDialog(context, R.style.dialogFullScreen);
        dialog.setContentView(R.layout.custom_dialog_comment_list);
        dialog.show();
        final RecyclerView rv_commentList = (RecyclerView) dialog.findViewById(R.id.rv_commentList);
        rv_commentList.setLayoutManager(new LinearLayoutManager(context));
        setRV(rv_commentList, bean.getSong_id());
        final EditText edt_comment = (EditText) dialog.findViewById(R.id.edt_comment);
        TextView tv_send = (TextView) dialog.findViewById(R.id.tv_send);
        ImageView iv_close_comment = (ImageView) dialog.findViewById(R.id.iv_close_comment);
        iv_close_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String comment = edt_comment.getText().toString().trim();
                if (!comment.equals("") && !comment.equals(null)) {
                    edt_comment.setText(null);
                    if (CheckNetwork.isInternetAvailable(context)) {
                        doComment2(Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""), bean.getSong_id(), comment, rv_commentList);
                    } else {
                        Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void setRV(final RecyclerView rv, final String s_id) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("song_id", s_id);
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.GET_COMMENT, jsonObject.toString());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("Response:: " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.i("mytag", "inside class:FragmentGenresDJ \t " + response);
                ArrayList<GsonComment2> gsonComment2ArrayList = new ArrayList<>();
                GsonComment2 commentBean;
                try {
                    JSONObject mJsonObject1 = new JSONObject(response);
                    JSONObject mJsonObject2;
                    if (mJsonObject1.getString("status").equals("1")) {
                        JSONArray mJsonArray = new JSONArray(mJsonObject1.getString("data"));
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            mJsonObject2 = new JSONObject(mJsonArray.get(i).toString());
                            commentBean = new GsonComment2(mJsonObject2.getString("u_id"), mJsonObject2.getString("comment").toString());
                            gsonComment2ArrayList.add(commentBean);
                        }
                    }
                    AdapterCommentList adapterCommentList = new AdapterCommentList(gsonComment2ArrayList, context);
                    rv.setAdapter(adapterCommentList);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                mProgressDialog.dismiss();
            }
        }.execute();

    }

    @SuppressLint("StaticFieldLeak")
    private void doComment2(final String u_id, final String s_id, final String comment, final RecyclerView recyclerView) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog1 = new ProgressDialog(context);
                mProgressDialog1.setMessage("Loading");
                mProgressDialog1.setCanceledOnTouchOutside(false);
                mProgressDialog1.setIndeterminate(true);
                mProgressDialog1.setCancelable(true);
                mProgressDialog1.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("u_id", u_id);
                    jsonObject.put("song_id", s_id);
                    jsonObject.put("comment", comment);
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.ADD_COMMENT, jsonObject.toString());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("Response:: " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.i("mytag", "inside class:FragmentGenresDJ \t " + response);
                try {
                    JSONObject mJsonObject1 = new JSONObject(response);
                    if (mJsonObject1.getString("status").equals("1")) {
                        setRV(recyclerView, s_id);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mProgressDialog1.dismiss();

            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void changeLikeStatus(final GsonFavDjsSongs2 bean, final ImageView iv, final TextView tv) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("u_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    jsonObject.put("song_id", bean.getSong_id());
                    if (bean.getLike_status().equals("1")) {
                        jsonObject.put("status_id", "0");
                    } else {
                        jsonObject.put("status_id", "1");
                    }
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.SONG_LIKES, jsonObject.toString());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("Geners Response:: " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.i("mytag", "inside class:FragmentGenresDJ \t " + response);
                try {
                    JSONObject mJsonObject1 = new JSONObject(response);
                    JSONObject mJsonObject2 = new JSONObject(mJsonObject1.getString("data"));
                    if (mJsonObject2.getString("like_status").toString().equals("1")) {
                        iv.setColorFilter(context.getResources().getColor(R.color.colorAccent));
                        tv.setTextColor(context.getResources().getColor(R.color.colorAccent));

                    } else {
                        iv.setColorFilter(context.getResources().getColor(R.color.white));
                        tv.setTextColor(context.getResources().getColor(R.color.white));
                    }
                    bean.setLike_status(mJsonObject2.getString("like_status").toString());
                    tv.setText(mJsonObject2.getString("total_like"));
                    bean.setTotle_like(mJsonObject2.getString("total_like"));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                mProgressDialog.dismiss();
            }
        }.execute();
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


}