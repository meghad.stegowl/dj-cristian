package com.djcristian.Adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.djcristian.Fragments.FragmentSingleGenre;
import com.djcristian.Model.PojoAlbum;
import com.djcristian.R;
import com.djcristian.utility.Utils;

import java.util.ArrayList;

public class AdapterMusicGenere extends RecyclerView.Adapter<AdapterMusicGenere.ViewHolder> {

    private ArrayList<PojoAlbum> arrayList;
    private FragmentManager fragmentManager;
    private Context context;
    private PojoAlbum bean;
    private RelativeLayout rl_fragments_header;


    public AdapterMusicGenere(Context context, ArrayList<PojoAlbum> generelist, FragmentManager fragmentManager, RelativeLayout rl_fragments_header) {
        this.context = context;
        this.arrayList = generelist;
        this.fragmentManager = fragmentManager;
        this.rl_fragments_header = rl_fragments_header;
    }

    @Override
    public AdapterMusicGenere.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_featuring_djs, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterMusicGenere.ViewHolder holder, final int position) {
        bean = arrayList.get(position);
        final String id = bean.getGenres_id();
        final String genere_name = bean.getGenres_name();
        final String total_tracks= bean.getTotal_genres();
        holder.genere_name.setText(genere_name);

        if (Integer.parseInt(total_tracks)>1){
            holder.tv_count.setText(bean.getTotal_genres()+" tracks");
        }
        else {
            holder.tv_count.setText(bean.getTotal_genres()+" track");
        }

        Utils.getInstance().loadGlide(context, holder.iv_icon, bean.getImage());
        holder.rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushInnerFragment(new FragmentSingleGenre(context, rl_fragments_header, bean.getBanner_image()), "Single", true, id, genere_name);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView iv_icon;
        public TextView genere_name, tv_count;
        public ImageView single_row_more_list;
        private RelativeLayout rl_main;

        public ViewHolder(View itemView) {
            super(itemView);
            genere_name = (TextView) itemView.findViewById(R.id.tv_title);
            tv_count = (TextView) itemView.findViewById(R.id.tv_count);
            iv_icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            rl_main = (RelativeLayout) itemView.findViewById(R.id.rl_main);
        }
    }


    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack, String id, String genere_name) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        final Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("genere_name", genere_name);
        fragment.setArguments(bundle);
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commitAllowingStateLoss();
    }

}
