package com.djcristian.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.djcristian.Activities.HomeActivity;
import com.djcristian.Manager.MediaController;
import com.djcristian.Model.GsonComment2;
import com.djcristian.Model.GsonPlaylistSongs2;
import com.djcristian.Model.PojoSongForPlayer;
import com.djcristian.R;
import com.djcristian.phonemidea.PhoneMediaControl;
import com.djcristian.utility.BufferData;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class AdapterPlaylistSingleSong extends RecyclerView.Adapter<AdapterPlaylistSingleSong.ViewHolder> {

    ArrayList<PojoSongForPlayer> sngList;
    private ArrayList<GsonPlaylistSongs2> arrayList;
    private FragmentManager fragmentManager;
    private Context context;
    private ProgressDialog mProgressDialog, mProgressDialog1;
    private String duration;

    public AdapterPlaylistSingleSong(Context context, ArrayList<GsonPlaylistSongs2> gsonPlaylistSongs2s, FragmentManager fragmentManager) {
        this.context = context;
        this.arrayList = gsonPlaylistSongs2s;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public AdapterPlaylistSingleSong.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_featuring_dg_song_new, parent, false);
        sngList = new ArrayList<PojoSongForPlayer>();
        GsonPlaylistSongs2 myBean;
        for (int i = 0; i < arrayList.size(); i++) {
            myBean = arrayList.get(i);
            duration = myBean.getDuration();
            String durInMili = String.valueOf(Utils.getInstance().strToMilli(duration));
            PojoSongForPlayer mDetail = new PojoSongForPlayer(myBean.getSong_name(), durInMili, myBean.getSong_url(), myBean.getImage());
            sngList.add(mDetail);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AdapterPlaylistSingleSong.ViewHolder holder, final int position) {
        final GsonPlaylistSongs2 bean = arrayList.get(position);
        holder.songName.setText(bean.getSong_name());
        holder.songName.setSelected(true);
        holder.tv_duration.setText(bean.getDuration());
        holder.tv_artist_name.setText(bean.getArtists_name());
        holder.tv_artist_name.setSelected(true);
        String nowPlaying = bean.getNowPlaying();
        DrawableImageViewTarget imageViewTarget = new DrawableImageViewTarget(holder.small_gif);
        Glide.with(context)
                .load(R.drawable.small_kenny)
                .fitCenter()
                .into(imageViewTarget);
        Utils.getInstance().loadGlide(context, holder.img, bean.getImage());
        if (nowPlaying.equals("1")) {
            holder.small_gif.setVisibility(View.VISIBLE);
        } else {
            holder.small_gif.setVisibility(View.GONE);
        }
        holder.tv_count_like.setText(bean.getTotle_like());
        if (bean.getLike_status().equals("1")) {
            holder.iv_like.setColorFilter(context.getResources().getColor(R.color.colorAccent));
            holder.tv_count_like.setTextColor(context.getResources().getColor(R.color.colorAccent));
        } else {
            holder.iv_like.setImageResource(R.drawable.like_off);
            holder.iv_like.setImageResource(R.drawable.like_off);

        }
        holder.rl_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String song_name = bean.getSong_name();
                String Artist_name = bean.getArtists_name();
                String Song_url = bean.getSong_url();
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = ("Song Name: " + song_name + "\n\n" + "Song Url: " + Song_url + "\n\n" + "Artist Name: " + Artist_name + "\n\n" + "App Name: " + "By Los Sobrevivientes");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        holder.rl_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    changeLikeStatus(arrayList.get(position), holder.iv_like, holder.tv_count_like);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.rl_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    showAndDoComments(arrayList.get(position));
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.iv_fav_option_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    showOptionDialog(position, bean.getSip_id());
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    notifyDataSetChanged();
                    final PojoSongForPlayer mDetail = sngList.get(position);
                    if (holder.small_gif.getVisibility() == View.VISIBLE) {
                        HomeActivity.visiblePlayerTwo();
                    } else {
                        if (mDetail != null) {
                            final ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                            if (progressDialog != null) {
                                Utils.getInstance().d("not null");
                                if (!progressDialog.isShowing()) {
                                    Utils.getInstance().d("not showing");
                                    if (CheckNetwork.isInternetAvailable(context)) {
                                        progressDialog.show();
                                        HomeActivity.visiblePlayerTwo();
                                        progressDialog.setCanceledOnTouchOutside(true);
                                    } else {
                                        progressDialog.dismiss();
                                        Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                                    }
                                    if (MediaController.getInstance().isPlayingAudio(mDetail) && !MediaController.getInstance().isAudioPaused()) {
                                        MediaController.getInstance().pauseAudio(mDetail);
                                    } else {
                                        MediaController.getInstance().setPlaylist(sngList, mDetail, PhoneMediaControl.SonLoadFor.All.ordinal(), -1);
                                    }
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (progressDialog != null) {
                                                if (progressDialog.isShowing()) {
                                                    new Handler().postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            String duration1 = duration;
                                                            String strTime = duration1.substring(duration1.length() - 2);
                                                            int sec1 = mDetail.audioProgressSec % 60;
                                                            int sec = Integer.parseInt(strTime);
                                                            if (sec > 1 && sec1 > 1) {
                                                                progressDialog.dismiss();
                                                            }
                                                        }
                                                    }, 200);
                                                }
                                            }
                                        }
                                    }, 200);
                                }
                            }
                        }
                    }
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showOptionDialog(final int pos, final String sip_id) {
        final AppCompatDialog dialog1;
        dialog1 = new AppCompatDialog(context, R.style.dialog);
        dialog1.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.custom_dialog_playlist_options);
        dialog1.show();
        TextView tv_custom_dilog_add_playlist = dialog1.findViewById(R.id.tv_custom_dilog_add_playlist);
        TextView tv_custom_dilog_remove_from_fav = dialog1.findViewById(R.id.tv_custom_dilog_remove_from_fav);
        tv_custom_dilog_remove_from_fav.setText("Add To Favorites");
        tv_custom_dilog_add_playlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    dialog1.dismiss();
                    removePlaylist(pos, sip_id);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tv_custom_dilog_remove_from_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    dialog1.dismiss();
                    AddToFav(pos);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void AddToFav(final int pos) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("u_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    jsonObject.put("song_id", arrayList.get(pos).getSong_id());
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.ADD_TO_FAV, jsonObject.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                Log.i("mytag", "inside class:FragmentGenresDJ \t " + response);
                try {
                    JSONObject mJsonObject = new JSONObject(response);
                    if (mJsonObject.getString("status").equals("1")) {
                        Utils.getInstance().toast((Activity) context, mJsonObject.getString("msg"));
                    } else {
                        Utils.getInstance().toast((Activity) context, mJsonObject.getString("msg"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void removePlaylist(final int pos, final String sip_id) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("sip_id", sip_id);
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.REMOVE_FROM_PLAYLIST, jsonObject.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                Log.i("mytag", "inside class:FragmentGenresDJ \t " + response);
                try {
                    JSONObject mJsonObject = new JSONObject(response);
                    if (mJsonObject.getString("status").equals("1")) {
                        Utils.getInstance().toast((Activity) context, mJsonObject.getString("msg"));
//                        removeItem(pos);
                        arrayList.remove(pos);
                        notifyItemRemoved(pos);
                    } else {
                        Utils.getInstance().toast((Activity) context, mJsonObject.getString("msg"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    public void removeItem(int i) {
        if (arrayList != null && i >= 0 && i <= arrayList.size()) {
            arrayList.remove(i);
            notifyDataSetChanged();
        }
    }

    private void showAndDoComments(final GsonPlaylistSongs2 bean) {
        final AppCompatDialog dialog;
        dialog = new AppCompatDialog(context, R.style.dialogFullScreen);
        dialog.setContentView(R.layout.custom_dialog_comment_list);
        dialog.show();
        final RecyclerView rv_commentList = dialog.findViewById(R.id.rv_commentList);
        rv_commentList.setLayoutManager(new LinearLayoutManager(context));
        setRV(rv_commentList, bean.getSong_id());
        final EditText edt_comment = dialog.findViewById(R.id.edt_comment);
        TextView tv_send = dialog.findViewById(R.id.tv_send);
        ImageView iv_close_comment = dialog.findViewById(R.id.iv_close_comment);
        iv_close_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String comment = edt_comment.getText().toString().trim();
                if (!comment.equals("") && !comment.equals(null)) {
                    edt_comment.setText(null);
                    if (CheckNetwork.isInternetAvailable(context)) {
                        doComment2(Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""), bean.getSong_id(), comment, rv_commentList);
                    } else {
                        Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void setRV(final RecyclerView rv, final String s_id) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("song_id", s_id);
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.GET_COMMENT, jsonObject.toString());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("Response:: " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.i("mytag", "inside class:FragmentGenresDJ \t " + response);
                ArrayList<GsonComment2> gsonComment2ArrayList = new ArrayList<>();
                GsonComment2 commentBean;
                try {
                    JSONObject mJsonObject1 = new JSONObject(response);
                    JSONObject mJsonObject2;
                    if (mJsonObject1.getString("status").equals("1")) {
                        JSONArray mJsonArray = new JSONArray(mJsonObject1.getString("data"));
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            mJsonObject2 = new JSONObject(mJsonArray.get(i).toString());
                            commentBean = new GsonComment2(mJsonObject2.getString("user"), mJsonObject2.getString("comment"));
                            gsonComment2ArrayList.add(commentBean);
                        }
                    }
                    AdapterCommentList adapterCommentList = new AdapterCommentList(gsonComment2ArrayList, context);
                    rv.setAdapter(adapterCommentList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mProgressDialog.dismiss();
            }
        }.execute();

    }

    @SuppressLint("StaticFieldLeak")
    private void doComment2(final String u_id, final String s_id, final String comment, final RecyclerView recyclerView) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog1 = new ProgressDialog(context);
                mProgressDialog1.setMessage("Loading");
                mProgressDialog1.setCanceledOnTouchOutside(false);
                mProgressDialog1.setIndeterminate(true);
                mProgressDialog1.setCancelable(true);
                mProgressDialog1.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("u_id", u_id);
                    jsonObject.put("song_id", s_id);
                    jsonObject.put("comment", comment);
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.ADD_COMMENT, jsonObject.toString());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("Response:: " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.i("mytag", "inside class:FragmentGenresDJ \t " + response);
                try {
                    JSONObject mJsonObject1 = new JSONObject(response);
                    if (mJsonObject1.getString("status").equals("1")) {
                        setRV(recyclerView, s_id);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mProgressDialog1.dismiss();
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void changeLikeStatus(final GsonPlaylistSongs2 bean, final ImageView iv, final TextView tv) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("u_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    jsonObject.put("song_id", bean.getSong_id());
                    if (bean.getLike_status().equals("1")) {
                        jsonObject.put("status_id", "0");
                    } else {
                        jsonObject.put("status_id", "1");
                    }
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.SONG_LIKES, jsonObject.toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("Geners Response:: " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.i("mytag", "inside class:FragmentGenresDJ \t " + response);
                try {
                    JSONObject mJsonObject1 = new JSONObject(response);
                    JSONObject mJsonObject2 = new JSONObject(mJsonObject1.getString("data"));
                    if (mJsonObject2.getString("like_status").equals("1")) {
                        iv.setColorFilter(context.getResources().getColor(R.color.colorAccent));
                        tv.setTextColor(context.getResources().getColor(R.color.colorAccent));
                    } else {
                        iv.setColorFilter(context.getResources().getColor(R.color.white));
                        tv.setTextColor(context.getResources().getColor(R.color.white));
                    }
                    bean.setLike_status(mJsonObject2.getString("like_status"));
                    tv.setText(mJsonObject2.getString("total_like"));
                    bean.setTotle_like(mJsonObject2.getString("total_like"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mProgressDialog.dismiss();
            }
        }.execute();
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img, iv_like, iv_fav_option_menu;
        TextView songName, tv_count_like, tv_duration, tv_artist_name;
        RelativeLayout rl_like, rl_share, rl_comment;
        LinearLayout ll_main;
        ImageView small_gif;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_duration = itemView.findViewById(R.id.tv_duration);
            img = itemView.findViewById(R.id.iv_favorite_img_dj);
            small_gif = itemView.findViewById(R.id.img_smallgif);
            songName = itemView.findViewById(R.id.tv_favorite_song_anme_dj);
            tv_artist_name = itemView.findViewById(R.id.tv_artist_name);
            rl_like = itemView.findViewById(R.id.rl_fav_like);
            rl_share = itemView.findViewById(R.id.rl_fav_share);
            rl_comment = itemView.findViewById(R.id.rl_fav_comment);
            iv_like = itemView.findViewById(R.id.iv_like);
            tv_count_like = itemView.findViewById(R.id.tv_fav_like_count);
            iv_fav_option_menu = itemView.findViewById(R.id.iv_fav_option_menu);
            ll_main = itemView.findViewById(R.id.ll_main);
        }
    }


}