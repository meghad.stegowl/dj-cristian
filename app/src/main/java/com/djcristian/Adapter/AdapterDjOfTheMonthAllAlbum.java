package com.djcristian.Adapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.djcristian.Manager.MediaController;
import com.djcristian.Model.GsonFeaturingDjsSongs2;
import com.djcristian.Model.GsonGenresAllAbums2;
import com.djcristian.Model.PojoSongForPlayer;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class AdapterDjOfTheMonthAllAlbum extends RecyclerView.Adapter<AdapterDjOfTheMonthAllAlbum.ViewHolder> {

    String genresID;
    private ArrayList<GsonGenresAllAbums2> arrayList;
    private ArrayList<GsonFeaturingDjsSongs2> arrayList_album = new ArrayList<>();
    private FragmentManager fragmentManager;
    private Context context;
    private DrawerLayout drawerLayout;
    private GsonGenresAllAbums2 bean;
    private LayoutInflater inflater;
    private DisplayImageOptions options;
    private RecyclerView rv_playlist;
    private ProgressDialog mProgressDialog;


    public AdapterDjOfTheMonthAllAlbum(Context context, ArrayList<GsonGenresAllAbums2> arrayList, FragmentManager fragmentManager, RecyclerView rv_playlist, String genresID) {
        this.context = context;
        this.genresID = genresID;
        this.arrayList = arrayList;
        this.fragmentManager = fragmentManager;
        this.rv_playlist = rv_playlist;

    }

    @Override
    public AdapterDjOfTheMonthAllAlbum.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_featuring_djs, parent, false);
        inflater = LayoutInflater.from(context);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.andu_placeholder)
                .showImageForEmptyUri(R.drawable.andu_placeholder)
                .showImageOnFail(R.drawable.andu_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterDjOfTheMonthAllAlbum.ViewHolder holder, final int position) {
        final GsonGenresAllAbums2 bean = arrayList.get(position);
        String title = bean.getAlbum_name();
        Log.i("mytag", "Album name : " + title);
        holder.tv_title.setText(title);
        Utils.getInstance().loadGlide(context, holder.iv_icon, bean.getAlbum_image());
        holder.rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(context)) {
                    setAlbumRv(rv_playlist, bean.getAlbum_id());
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @SuppressLint("StaticFieldLeak")
    private void setAlbumRv(final RecyclerView rv_playlist, final String album_id) {
        final PojoSongForPlayer songDetail = MediaController.getInstance().getPlayingSongDetail();
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("album_id", album_id);
                    jsonObject.put("genres_id", genresID);
                    jsonObject.put("u_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.SELECT_ALBUM, jsonObject.toString());


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("mytag" + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject data1 = data.getJSONObject(i);
                            String song_id = data1.getString("song_id");
                            String like_status = data1.getString("like_status");
                            String song_time = data1.getString("song_time");
                            String totle_like = data1.getString("totle_like");
                            String song_name = data1.getString("song_name");
                            String song_url = data1.getString("song_url");
                            String image = data1.getString("image");
                            String genres_id = data1.getString("genres_id");
                            String album_id = data1.getString("album_id");
                            String artists_name = data1.getString("artists_name");
                            String nowPlaying;
                            if (songDetail != null) {
                                if (songDetail.getS_url().equals(song_url)) {
                                    nowPlaying = "1";
                                } else {
                                    nowPlaying = "0";
                                }
                            } else {
                                nowPlaying = "0";
                            }
                            arrayList_album.add(new GsonFeaturingDjsSongs2(song_id, song_name, song_url, image, like_status, totle_like, song_time, artists_name, nowPlaying));
                        }
                        AdapterFeaturingDjsSongs adapter = new AdapterFeaturingDjsSongs(context, arrayList_album, fragmentManager);
                        rv_playlist.setAdapter(adapter);
                    }

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        }.execute();
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rl_main;
        LinearLayout ll_arrow_count;
        TextView tv_title;
        ImageView iv_icon;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_arrow_count = itemView.findViewById(R.id.ll_arrow_count);
            ll_arrow_count.setVisibility(View.GONE);
            rl_main = itemView.findViewById(R.id.rl_main);
            tv_title = itemView.findViewById(R.id.tv_title);
            iv_icon = itemView.findViewById(R.id.iv_icon);
        }
    }


}
