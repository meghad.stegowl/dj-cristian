package com.djcristian.CustomView;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.djcristian.R;

public class CustomHeader {
    public static void setOuterFragment(final Activity activity, RelativeLayout relativeLayout) {
        Button btn_menu = (Button) relativeLayout.findViewById(R.id.btn_menu);
//        btn_menu.setBackgroundResource(R.drawable.back_arrow);
        relativeLayout.setTag("Outer");
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
//                HomeActivity.visiblePlayer();
            }
        });

    }

    public static void setInnerFragment(final Activity activity, final RelativeLayout relativeLayout) {
        final Button btn_menu = (Button) relativeLayout.findViewById(R.id.btn_menu);
//        btn_menu.setBackgroundResource(R.drawable.back_arrow);
        relativeLayout.setTag("Inner");
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (relativeLayout.getTag().equals("Inner")) {
                    activity.onBackPressed();
                } else {
//                    btn_menu.setBackgroundResource(R.drawable.menu);
                }

            }
        });

    }

    public static void setOuterFragment_more(final Activity activity, RelativeLayout relativeLayout) {
        Button btn_menu = (Button) relativeLayout.findViewById(R.id.btn_menu);
//        btn_menu.setBackgroundResource(R.drawable.back_arrow);
        relativeLayout.setTag("Outer");
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
//                HomeActivity.visiblePlayer();
            }
        });

    }
}
