package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterMusicGenere;
import com.djcristian.Model.PojoAlbum;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.RestClient;
import com.djcristian.utility.WebInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentMusicGeners extends Fragment {

    private View rootView;
    private Context context;
    private RecyclerView rv_playlist;
    private ArrayList<PojoAlbum> generelist = new ArrayList<>();
    private AdapterMusicGenere adapter;
    FragmentManager fm;
    private ProgressDialog mProgressDialog;
    private boolean isOuter = false;
    private RelativeLayout rl_fragments_header, header_screen;
    private TextView tv_title;
    private Button btn_menu, btn_player;
    private boolean isFragmentLoaded = false;
    private Boolean Count;


    @SuppressLint("ValidFragment")
    public FragmentMusicGeners(Context context, RelativeLayout rl_fragments_header, boolean isOuter, boolean Count) {
        this.context = context;
        this.isOuter = isOuter;
        this.rl_fragments_header = rl_fragments_header;
        this.Count = Count;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_album, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        if (isOuter) {
            rl_fragments_header.setTag("Outer");
        } else {
            rl_fragments_header.setTag("Inner");
        }
        init();
        listner();
        return rootView;
    }

    private void init() {
        rv_playlist = (RecyclerView) rootView.findViewById(R.id.rv_album);
        rv_playlist.setLayoutManager(new LinearLayoutManager(context));
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_title.setText("MUSIC CATEGORIES");
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        header_screen.setVisibility(View.VISIBLE);
        if (CheckNetwork.isInternetAvailable(context)) {
            get_geners();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

    @SuppressLint("StaticFieldLeak")
    private void get_geners() {
        generelist = new ArrayList<>();
        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                JSONObject jsonObject = new JSONObject();
                try {
                    response = WebInterface.getInstance().doGet(Const.COUNT_GENERS);
                    Log.i("mytag", "Response : " + response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.i("mytag", " Final Response : " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject innerdata = data.optJSONObject(i);
                            String genres_id = innerdata.getString("category_id").toString();
                            String genres_name = innerdata.getString("category_name").toString();
                            String total_genres = innerdata.getString("total_songs").toString();
                            String image = innerdata.getString("image").toString();
                            generelist.add(new PojoAlbum(genres_id, genres_name, total_genres, image, ""));
                        }
                    }
                    adapter = new AdapterMusicGenere(context, generelist, getFragmentManager(), rl_fragments_header);
                    rv_playlist.setAdapter(adapter);

                } catch (Exception e) {
                }
            }
        }.execute();
    }
}
