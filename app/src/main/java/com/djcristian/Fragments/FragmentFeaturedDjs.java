package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterFeaturingDjs;
import com.djcristian.Adapter.AdapterFeaturingDjsSongs;
import com.djcristian.Adapter.ImageAdapter;
import com.djcristian.Manager.MediaController;
import com.djcristian.Model.GsonFeaturingDjsSongs2;
import com.djcristian.Model.PojoAlbum;
import com.djcristian.Model.PojoSongForPlayer;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentFeaturedDjs extends Fragment {
    ArrayList<PojoAlbum> gsonGetPlaylist2ArrayList;
    private Context context;
    private RelativeLayout rl_fragments_header, header_screen;
    private View rootView;
    private RecyclerView rv_featured_djs;
    private ProgressDialog mProgressDialog;
    private AdapterFeaturingDjs adapter;
    private boolean isOuter = false;
    private FragmentManager fm;
    private boolean isFragmentLoaded = false;
    private Button btn_menu, btn_player;
    private TextView tv_title;
    private Gallery gallery;
    private Handler handler = new Handler();
    private Boolean Count;
    private ArrayList<GsonFeaturingDjsSongs2> gsonPlaylistSongs2s;
    private AdapterFeaturingDjsSongs adapterFeaturingDjsSongs;


    @SuppressLint("ValidFragment")
    public FragmentFeaturedDjs(Context context, RelativeLayout rl_fragments_header, boolean isOuter) {
        this.context = context;
        this.isOuter = isOuter;
        this.rl_fragments_header = rl_fragments_header;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_featured_djs, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        if (isOuter) {
            rl_fragments_header.setTag("Outer");
        } else {
            rl_fragments_header.setTag("Inner");

        }
        init();
        listner();
        return rootView;
    }


    private void init() {
        gallery = rootView.findViewById(R.id.gallery);
        gallery.setSpacing(2);
        rv_featured_djs = rootView.findViewById(R.id.rv_album);
        rv_featured_djs.setLayoutManager(new LinearLayoutManager(context));
        header_screen = rootView.findViewById(R.id.header_screen);
        tv_title = rootView.findViewById(R.id.tv_title);
        tv_title.setText("FEATURED DJS");
        btn_menu = rootView.findViewById(R.id.btn_menu);
        btn_player = rootView.findViewById(R.id.btn_player);
        header_screen.setVisibility(View.VISIBLE);
        if (CheckNetwork.isInternetAvailable(context)) {
            getFeaturedDjs();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

    @SuppressLint("StaticFieldLeak")
    private void getFeaturedDjs() {
        gsonGetPlaylist2ArrayList = new ArrayList<>();
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }


            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    response = WebInterface.getInstance().doGet(Const.GET_FEATURING_DJS);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("Dj's Response : " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject data = jsonArray.getJSONObject(i);
                            String feature_dj_id = data.getString("feature_dj_id");
                            String name = data.getString("name");
                            String image = data.getString("image");
                            String insta_url = data.getString("insta_url");
                            gsonGetPlaylist2ArrayList.add(new PojoAlbum(feature_dj_id, name, image, insta_url));
                        }
                        if (gsonGetPlaylist2ArrayList.size() > 0) {
                            Utils.getInstance().d("size " + gsonGetPlaylist2ArrayList.size());
                            setAlbum();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();


    }

    private void setAlbum() {
        final ImageAdapter adapter = new ImageAdapter(context, gsonGetPlaylist2ArrayList);
        gallery.setAdapter(adapter);
        gallery.setSelection(0);
        gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                PojoAlbum item = gsonGetPlaylist2ArrayList.get(position);
                adapter.setSelectedItem(position);
                getSongs(item.getGenres_id());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void getSongs(final String id) {
        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (CheckNetwork.isInternetAvailable(context)) {
                    getFeaturedDjsSongs(id);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        }, 1000);
    }

    public void onLoadSong(String url) {
        Utils.getInstance().d("url" + url);
        for (GsonFeaturingDjsSongs2 item : gsonPlaylistSongs2s) {
            if (item.getUrl().equals(url)) {
                item.setNowPlaying("1");
            } else {
                item.setNowPlaying("0");
            }
            adapterFeaturingDjsSongs = new AdapterFeaturingDjsSongs(context, gsonPlaylistSongs2s, getFragmentManager());
            rv_featured_djs.setAdapter(adapterFeaturingDjsSongs);

        }

    }

    @SuppressLint("StaticFieldLeak")
    private void getFeaturedDjsSongs(final String id) {
        final PojoSongForPlayer songDetail = MediaController.getInstance().getPlayingSongDetail();
        new AsyncTask<Void, Void, String>() {

            @SuppressLint("StaticFieldLeak")
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("u_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    jsonObject.put("feature_dj_id", id);
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.GET_SONG_BY_FEATURING_DJS, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("mytag" + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        gsonPlaylistSongs2s = new ArrayList<GsonFeaturingDjsSongs2>();
                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject data1 = data.getJSONObject(i);
                            String song_id = data1.getString("song_id");
                            String like_status = data1.getString("like_status");
                            String song_time = data1.getString("song_time");
                            String totle_like = data1.getString("total_likes");
                            String song_name = data1.getString("song_name");
                            String song_url = data1.getString("song_url");
                            String image = data1.getString("image");
                            String artists_name = data1.getString("artist_name");
                            String nowPlaying;
                            if (songDetail != null) {
                                if (songDetail.getS_url().equals(song_url)) {
                                    nowPlaying = "1";
                                } else {
                                    nowPlaying = "0";
                                }
                            } else {
                                nowPlaying = "0";
                            }
                            gsonPlaylistSongs2s.add(new GsonFeaturingDjsSongs2(song_id, song_name, song_url, image, like_status, totle_like, song_time, artists_name, nowPlaying));
                        }
                        adapterFeaturingDjsSongs = new AdapterFeaturingDjsSongs(context, gsonPlaylistSongs2s, getFragmentManager());
                        rv_featured_djs.setAdapter(adapterFeaturingDjsSongs);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }
}
