package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterVideoMovies;
import com.djcristian.Model.PojoVideoMovies;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.WebInterface;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentVideosList extends Fragment {
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;
    private ProgressDialog mProgressDialog;
    private RecyclerView rv_video_list;
    private ArrayList<PojoVideoMovies> mdata = new ArrayList<PojoVideoMovies>();
    private AdapterVideoMovies adapterVideoMovies;
    private Button btn_menu, btn_player;
    private TextView tv_title;
    private RelativeLayout header_screen;
    private boolean isFragmentLoaded = false;
    private AdView mAdView;


    @SuppressLint("ValidFragment")
    public FragmentVideosList(Context context, RelativeLayout rl_fragments_header, AdView mAdView) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.mAdView = mAdView;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_video_new, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Inner");
        HomeActivity.rl_fragments_bottom_player.setVisibility(View.GONE);
        header_screen = rootView.findViewById(R.id.header_screen);
        tv_title = rootView.findViewById(R.id.tv_title);
        tv_title.setText("VIDEOS");
        btn_menu = rootView.findViewById(R.id.btn_menu);
        btn_player = rootView.findViewById(R.id.btn_player);
        header_screen.setVisibility(View.VISIBLE);
        init();
        listner();
        return rootView;
    }

    private void init() {
        rv_video_list = rootView.findViewById(R.id.rv_video_list);
        rv_video_list.setLayoutManager(new LinearLayoutManager(context));
        if (CheckNetwork.isInternetAvailable(context)) {
            getdata();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.rl_fragments_bottom_player.setVisibility(View.VISIBLE);
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.rl_fragments_bottom_player.setVisibility(View.VISIBLE);
                HomeActivity.visiblePlayer();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

    @SuppressLint("StaticFieldLeak")
    private void getdata() {
        mdata = new ArrayList<PojoVideoMovies>();
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                JSONObject jsonObject = new JSONObject();
                try {
                    response = WebInterface.getInstance().doGet(Const.VIDEO);
                    Log.i("mytag", "Response : " + response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.i("mytag", " Final Response : " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject innerdata = data.optJSONObject(i);
                            String name = innerdata.getString("name");
                            String image = innerdata.getString("image");
                            String data_link = innerdata.getString("video_link");
                            String date = innerdata.getString("date");
                            PojoVideoMovies arraylist = new PojoVideoMovies(name, data_link, image, date);
                            mdata.add(arraylist);
                        }
                        adapterVideoMovies = new AdapterVideoMovies(context, mdata, getFragmentManager(), rl_fragments_header, mAdView);
                        rv_video_list.setAdapter(adapterVideoMovies);
                    }
                } catch (Exception e) {
                }
            }
        }.execute();
    }
}
