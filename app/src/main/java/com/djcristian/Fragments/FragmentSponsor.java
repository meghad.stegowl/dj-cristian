package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterSponsor;
import com.djcristian.Model.MoreSlider;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.WebInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentSponsor extends Fragment {
    private View rootView;
    private Context context;
    private RecyclerView rv_sponsor;
    private ArrayList<MoreSlider> arrayList = new ArrayList<>();
    private AdapterSponsor adapterSponsor;
    private RelativeLayout rl_fragments_header, rl_player, rl_fragments;
    private boolean isOuter = false;
    private ProgressDialog mProgressDialog;
    private Button btn_menu, btn_player;
    private RelativeLayout header_screen;
    private TextView tv_title;
    private FragmentManager fm;
    private boolean isFragmentLoaded = false;

    public FragmentSponsor(Context context, RelativeLayout rl_fragments_header) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sponsor, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Inner");
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_title.setText("SPONSORED BY");
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        header_screen.setVisibility(View.VISIBLE);
        init();
        listner();
        return rootView;
    }

    private void init() {
        rv_sponsor = (RecyclerView) rootView.findViewById(R.id.rv_sponsor);
        rv_sponsor.setLayoutManager(new LinearLayoutManager(context));
        if (CheckNetwork.isInternetAvailable(context)) {
            getsponsor();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void getsponsor() {
        arrayList = new ArrayList<MoreSlider>();
        new AsyncTask<Void, Void, String>() {
            String response = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();            }

            @Override
            protected String doInBackground(Void... voids) {
                try {
                    response = WebInterface.getInstance().doGet(Const.GET_SINGLE_SLIDER_IMAGES);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.d("mytag", "In Sponsor Tab Response : " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject innerdata = jsonArray.getJSONObject(i);
                        String name = innerdata.getString("sponsor_name").toString();
                        String image = innerdata.getString("sponsor_logo").toString();
                        String id = innerdata.getString("sponsor_id").toString();
                        arrayList.add(new MoreSlider(image, name, id));
                    }
                    adapterSponsor = new AdapterSponsor(arrayList, context, getFragmentManager(), rl_fragments_header);
                    rv_sponsor.setAdapter(adapterSponsor);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }
}