package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterImagesSponsorSlider;
import com.djcristian.CustomView.CustomHeader;
import com.djcristian.Model.MoreSlider;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentSingleSponsor extends Fragment {
    private View rootView;
    private Context context;
    private RecyclerView rv_single_sponsor;
    private RelativeLayout rl_fragments_header;
    private ProgressDialog mProgressDialog;
    private String ID, title;
    private ArrayList<MoreSlider> arrayList = new ArrayList<>();
    private TextView tv_address_one, tv_address_two, tv_title_sponsor;
    private ImageView iv_main_logo;
    private AdapterImagesSponsorSlider adapter;
    private boolean isOuter = false;
    private Button btn_menu, btn_player;
    private RelativeLayout header_screen;
    private TextView tv_title, tv_title1;


    public FragmentSingleSponsor(Context context, RelativeLayout rl_fragments_header, boolean isOuter, String ID, String title) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.ID = ID;
        this.title = title;
        this.isOuter = isOuter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_single_sponsor, container, false);
        CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        init();
        listner();
        return rootView;
    }


    private void init() {
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Inner");
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        tv_title1 = (TextView) rootView.findViewById(R.id.tv_title1);
        tv_title1.setText(title);
        tv_title1.setSelected(true);
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        header_screen.setVisibility(View.VISIBLE);
        rv_single_sponsor = (RecyclerView) rootView.findViewById(R.id.rv_inside_image);
        rv_single_sponsor.setLayoutManager(new LinearLayoutManager(context));
        tv_address_one = (TextView) rootView.findViewById(R.id.tv_address);
        tv_address_two = (TextView) rootView.findViewById(R.id.tv_add_two);
        tv_title_sponsor = (TextView) rootView.findViewById(R.id.tv_title_sponsor);
        tv_title_sponsor.setText(title);
        iv_main_logo = (ImageView) rootView.findViewById(R.id.iv_image_main);
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        if (CheckNetwork.isInternetAvailable(context)) {
            getData();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void getData() {
        arrayList = new ArrayList<MoreSlider>();
        new AsyncTask<Void, Void, String>() {
            String response = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                JSONObject jsonObject = new JSONObject();
                try {
                    try {
                        jsonObject.put("id", ID);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    response = WebInterface.getInstance().doPostRequest(Const.GET_SINGLE_SPOSOR_DETAIL, jsonObject.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.d("mytag", "In Sponsor single Tab Response : " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        JSONObject data = jsonObject.getJSONObject("data");
                        String address = data.getString("sponsor_address").toString();
                        String description = data.getString("sponsor_description").toString();
                        String logo = data.getString("sponsor_logo").toString();
                        Utils.getInstance().loadGlide(context, iv_main_logo, logo);
                        tv_address_one.setText(address);
                        tv_address_two.setText(description);
                        JSONArray jsonArray = jsonObject.getJSONArray("inner_images");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            String link = jsonArray.getString(i);
                            arrayList.add(new MoreSlider(link));
                        }
                        adapter = new AdapterImagesSponsorSlider(context, arrayList);
                        rv_single_sponsor.setAdapter(adapter);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();


    }
}