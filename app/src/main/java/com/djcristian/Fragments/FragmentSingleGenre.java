package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterDjOfTheMonthAllAlbum;
import com.djcristian.Adapter.AdapterSingleGenreSong;
import com.djcristian.Manager.MediaController;
import com.djcristian.Model.GsonFeaturingDjsSongs2;
import com.djcristian.Model.GsonGenreWiseSong2;
import com.djcristian.Model.GsonGenresAllAbums1;
import com.djcristian.Model.GsonGenresAllAbums2;
import com.djcristian.Model.PojoSongForPlayer;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.RestClient;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentSingleGenre extends Fragment {

    private View rootView;
    private Context context;
    private LinearLayout ll_header;
    private RecyclerView rv_playlist;
    private ProgressDialog mProgressDialog;
    ArrayList<GsonFeaturingDjsSongs2> arrayList = new ArrayList<>();
    Button all_tacks, all_albums, btn_menu, btn_player;
    private String id, title;
    RelativeLayout rl_fragments_header, header_screen;
    private TextView tv_single_album, tv_title, tv_single_album_name;
    private boolean isOuter = false;
    private FragmentManager fm;
    private String banner_image;
    private ImageView iv_instagram;
    private DisplayImageOptions options;

    public FragmentSingleGenre(Context context, RelativeLayout rl_fragments_header, String banner_image) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.banner_image = banner_image;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.cristian_placeholder)
                .showImageForEmptyUri(R.drawable.cristian_placeholder)
                .showImageOnFail(R.drawable.cristian_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_single_genre, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Inner");
        Bundle args = getArguments();
        id = args.getString("id");
        title = args.getString("genere_name");
        init();
        listner();
        return rootView;
    }

    private void init() {

        iv_instagram=rootView.findViewById(R.id.iv_instagram);
        all_tacks = (Button) rootView.findViewById(R.id.btn_all_tracks);
        all_albums = (Button) rootView.findViewById(R.id.btn_all_albums);
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        rv_playlist = (RecyclerView) rootView.findViewById(R.id.rv_in_genre_fragment);
        rv_playlist.setLayoutManager(new LinearLayoutManager(context));
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        header_screen.setVisibility(View.VISIBLE);
        tv_single_album = (TextView) rootView.findViewById(R.id.tv_single_album);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_single_album_name = (TextView) rootView.findViewById(R.id.tv_single_album_name);
        tv_title.setText(title);
        tv_single_album_name.setText(title);
        Log.d("mytag", "Banner_image : " + banner_image);
        if (CheckNetwork.isInternetAvailable(context)) {
            getdata1();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void onLoadSong(String url) {
        Utils.getInstance().d("url" + url);
        for (GsonFeaturingDjsSongs2 item : arrayList) {
            if (item.getUrl().equals(url)) {
                item.setNowPlaying("1");
            } else {
                item.setNowPlaying("0");
            }
            AdapterSingleGenreSong adapter = new AdapterSingleGenreSong(context, arrayList, fm, ll_header);
            rv_playlist.setAdapter(adapter);
        }

    }

    @SuppressLint("StaticFieldLeak")
    private void getdata1() {
        final PojoSongForPlayer songDetail = MediaController.getInstance().getPlayingSongDetail();
        new AsyncTask<Void, Void, String>() {

            @SuppressLint("StaticFieldLeak")
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("category_id", id);
                    jsonObject.put("u_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.SELECT_GENERS, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("mytag" + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject data1 = data.getJSONObject(i);
                            String song_id = data1.getString("song_id");
                            String like_status = data1.getString("like_status");
                            String song_time = data1.getString("song_time");
                            String totle_like = data1.getString("total_likes");
                            String song_name = data1.getString("song_name");
                            String song_url = data1.getString("song_url");
                            String image = data1.getString("image");
                            String genres_id = data1.getString("category_id");
                            String album_id = data1.getString("album_id");
                            String artists_name = data1.getString("artist_name");
                            String nowPlaying;
                            if (songDetail != null) {
                                if (songDetail.getS_url().equals(song_url)) {
                                    nowPlaying = "1";
                                } else {
                                    nowPlaying = "0";
                                }
                            } else {
                                nowPlaying = "0";
                            }
                            arrayList.add(new GsonFeaturingDjsSongs2(song_id, song_name, song_url, image, like_status
                                    , totle_like, song_time, artists_name, nowPlaying));
                        }
                        AdapterSingleGenreSong adapter = new AdapterSingleGenreSong(context, arrayList, fm, ll_header);
                        rv_playlist.setAdapter(adapter);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    private void listner() {
        all_tacks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        all_albums.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });

        iv_instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

}
