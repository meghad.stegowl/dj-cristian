package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterGallery;
import com.djcristian.Model.GalleryFolder;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


@SuppressLint("ValidFragment")
public class FragmentGallery extends Fragment {
    FragmentManager fm;
    private Context context;
    private View rootView;
    private ProgressDialog mProgressDialog;
    private RecyclerView rv_gallery;
    private ArrayList<GalleryFolder> galleryImagesArrayList;
    private RelativeLayout rl_fragments_header;
    private Button btn_menu, btn_player;
    private RelativeLayout header_screen;
    private TextView tv_title;
    private boolean isFragmentLoaded = false;

    @SuppressLint("ValidFragment")
    public FragmentGallery(Context context, RelativeLayout rl_fragments_header) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_gallery_new, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Inner");
        header_screen = rootView.findViewById(R.id.header_screen);
        tv_title = rootView.findViewById(R.id.tv_title);
        tv_title.setText("GALLERY");
        btn_menu = rootView.findViewById(R.id.btn_menu);
        btn_player = rootView.findViewById(R.id.btn_player);
        header_screen.setVisibility(View.VISIBLE);
        init();
        listner();
        return rootView;
    }

    private void init() {
        rv_gallery = rootView.findViewById(R.id.rv_gallery);
        rv_gallery.setHasFixedSize(true);
        ((SimpleItemAnimator) rv_gallery.getItemAnimator()).setSupportsChangeAnimations(false);
        rv_gallery.setLayoutManager(new GridLayoutManager(context, 2));
        if (CheckNetwork.isInternetAvailable(context)) {
            getGalleryImages();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void getGalleryImages() {
        galleryImagesArrayList = new ArrayList<>();
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(getActivity());
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();

            }

            @Override
            protected String doInBackground(Void... params) {
                String response = null;
                try {
                    response = WebInterface.getInstance().doGet(Const.GALLERY_FOLDER);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("Gallery Response:: " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                JSONObject mJsonObject;
                try {
                    mJsonObject = new JSONObject(response);
                    if (mJsonObject.getString("status").equals("1")) {
                        JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject object = mJsonArray.optJSONObject(i);
                            String f_id = object.getString("folder_id");
                            String name = object.getString("folder_name");
                            String image = object.getString("folder_image");
                            galleryImagesArrayList.add(new GalleryFolder(image, f_id, name));

                        }
                        AdapterGallery adapterGalleryNew = new AdapterGallery(context, rl_fragments_header, galleryImagesArrayList, getFragmentManager());
                        rv_gallery.setAdapter(adapterGalleryNew);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }
}
