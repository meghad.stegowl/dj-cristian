package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterMore;
import com.djcristian.CustomView.RecyclerItemClickListener;
import com.djcristian.Model.More;
import com.djcristian.R;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentMore extends Fragment {
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;
    private RecyclerView rv_more;
    private ArrayList<More> moreArrayList;
    private boolean isOuter = false;
    private Boolean Count = true;
    private Button btn_player, btn_menu;
    private AdView mAdView;

    @SuppressLint("ValidFragment")
    public FragmentMore(Context context, RelativeLayout rl_fragments_header, AdView mAdView) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.mAdView = mAdView;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_more, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Outer");
        init();
        listner();
        return rootView;
    }

    private void init() {
        rv_more = rootView.findViewById(R.id.rv_more);
        rv_more.setLayoutManager(new LinearLayoutManager(context));
        btn_player = rootView.findViewById(R.id.btn_player);
        btn_menu = rootView.findViewById(R.id.btn_menu);
        String[] menuName = {"Music Player", "Music Categories", "Hit Singles", "My Favorites",
                "My Playlist", "Videos", "Events", "Social Media", "Gallery", "Vip List (Sign Up)", "Share My App" ,
                "Notifications"};
        int[] menuImage = {R.drawable.menu_player, R.drawable.menu_music, R.drawable.menu_singles, R.drawable.menu_fav,
                R.drawable.menu_my_playlist,
                R.drawable.menu_videos, R.drawable.menu_event, R.drawable.menu_social,
                R.drawable.menu_gallery, R.drawable.menu_vip_list, R.drawable.menu_share,R.drawable.icon_notificaion};
        moreArrayList = new ArrayList<>();
        for (int i = 0; i < menuImage.length; i++) {
            moreArrayList.add(new More(menuImage[i], menuName[i]));
        }
        AdapterMore adapterMore = new AdapterMore(moreArrayList, context);
        rv_more.setAdapter(adapterMore);

    }

    private void listner() {
        rv_more.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                More item = moreArrayList.get(position);
                String name = item.getTitle();
                final RelativeLayout rl_main = view.findViewById(R.id.rl_main);
                rl_main.setPressed(true);
                switch (name) {
                    case "Music Player":
                        HomeActivity.visiblePlayer();
                        break;
                    case "Music Categories":
                        pushInnerFragment(new FragmentMusicGeners(context, rl_fragments_header, false, Count), "Music Geners", true);
//                        pushInnerFragment(new FragmentFeaturedArtist1(context, rl_fragments_header, false, getActivity().getSupportFragmentManager()), "Feature Artist", true);
                        break;
//                    case "Artist":
//                        pushInnerFragment(new FragmentFeaturedArtist(context, rl_fragments_header, false, getActivity().getSupportFragmentManager()), "Feature Artist", true);
//                        break;
//                    case "Bookings":
//                        pushInnerFragment(new FragmentBooking(context, rl_fragments_header, false), "bookings", true);
//                        break;
                    case "Hit Singles":
                        pushInnerFragment(new FragmentArtistOfTheMonth(context, rl_fragments_header, false), "Hit Single", true);
                        break;
//                    case "Featured Djs":
//                        pushInnerFragment(new FragmentFeaturedDjs(context, rl_fragments_header, false), "New Releases", true);
//                        break;
                    case "My Favorites":
                        pushInnerFragment(new FragmentMyFav(context, rl_fragments_header, false), "myFav", true);
                        break;
                    case "My Playlist":
                        pushInnerFragment(new FragmentMyPlaylist(context, rl_fragments_header, false), "myPlaylist", true);
                        break;
//                    case "Biographies":
//                        pushInnerFragment(new FragmentBiographies(context, rl_fragments_header, false), "myPlaylist", true);
//                        break;
                    case "Videos":
                        pushInnerFragment(new FragmentVideosList(context, rl_fragments_header, mAdView), "videoList", true);
                        break;
                    case "Events":
                        pushInnerFragment(new FragmentEvents(context, rl_fragments_header, false), "myFav", true);
                        break;
                    case "Social Media":
                        pushInnerFragment(new FragmentSocialMedia(context, rl_fragments_header), "socialMedia", true);
                        break;
                    case "Gallery":
                        pushInnerFragment(new FragmentGallery(context, rl_fragments_header), "gallery", true);
                        break;
//                    case "Sponsored By":
//                        pushInnerFragment(new FragmentSponsor(context, rl_fragments_header), "gallery", true);
//                        break;
                    case "Vip List (Sign Up)":
                        pushInnerFragment(new FragmentSignUp(context, rl_fragments_header, false), "signup", true);
                        break;

                    case "Share My App":
                        pushInnerFragment(new FragmentShare(context, rl_fragments_header), "signup", true);
                        break;
                    case "Notifications":
                        pushInnerFragment(new FragmentInbox(context, rl_fragments_header), "Inbox", true);
                        break;
                }

            }
        }));
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }
}
