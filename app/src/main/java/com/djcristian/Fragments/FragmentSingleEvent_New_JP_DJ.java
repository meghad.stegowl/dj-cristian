package com.djcristian.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.CustomView.CustomHeader;
import com.djcristian.Model.GsonGetEvents2;
import com.djcristian.R;
import com.djcristian.utility.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.TrackerSettings;

@SuppressLint("ValidFragment")
public class FragmentSingleEvent_New_JP_DJ extends Fragment implements OnMapReadyCallback {

    private View rootView;
    private Context context;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    double latitude = 0;
    private RelativeLayout r1_header;
    double longitude = 0;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    FragmentManager fm;
    private String date, time, address, title, desc, image, lat, long_t;
    private GsonGetEvents2 bean;
    private TextView tv_title_single_event, tv_description_single_event, txt_event_title, txt_event_location, txt_event_date, txt_event_time;
    private GoogleMap mMap;
    private Button btn_menu, btn_player;
    private Button btn_get_direction;
    private DisplayImageOptions options;
    private ImageView iv_event_img;
    private boolean isOuter = false;
    private TrackerSettings settings =
            new TrackerSettings()
                    .setUseGPS(true)
                    .setUseNetwork(true)
                    .setUsePassive(true)
                    .setTimeBetweenUpdates(1 * 60 * 1000)
                    .setMetersBetweenUpdates(100);


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public FragmentSingleEvent_New_JP_DJ(Context context, RelativeLayout rl_fragments_header, boolean is_outer, String date, String time, String title, String desc, String image, String lati, String long_t, String address) {
        this.context = context;
        this.r1_header = rl_fragments_header;
        this.isOuter = is_outer;
        this.date = date;
        this.address = address;
        this.time = time;
        this.title = title;
        this.desc = desc;
        this.image = image;
        this.lat = lati;
        this.long_t = long_t;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_single_event, container, false);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.cristian_placeholder)
                .showImageForEmptyUri(R.drawable.cristian_placeholder)
                .showImageOnFail(R.drawable.cristian_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        CustomHeader.setInnerFragment(getActivity(), r1_header);
        init();
        listner();
        return rootView;
    }


    private void init() {
        if (Build.VERSION.SDK_INT >= 23) {
            CheckPermission();
        } else {
            loadLocation();
        }
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        btn_get_direction = (Button) rootView.findViewById(R.id.btn_get_direction);
        txt_event_title = (TextView) rootView.findViewById(R.id.txt_event_title);
        txt_event_title.setText(title);
        txt_event_location = (TextView) rootView.findViewById(R.id.txt_event_location);
        txt_event_location.setText(address);
        txt_event_date = (TextView) rootView.findViewById(R.id.txt_event_date);
        txt_event_date.setText(date);
        txt_event_time = (TextView) rootView.findViewById(R.id.txt_event_time);
        txt_event_time.setText(time);
        tv_title_single_event = (TextView) rootView.findViewById(R.id.tv_title);
        tv_title_single_event.setSelected(true);
        tv_description_single_event = (TextView) rootView.findViewById(R.id.tv_description_single_event);
        iv_event_img = (ImageView) rootView.findViewById(R.id.iv_event_img);
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        ImageLoader.getInstance().displayImage(image, iv_event_img, options);
        tv_title_single_event.setText(title);
        tv_description_single_event.setText(desc);
        tv_title_single_event.setText(title);
        tv_title_single_event.setSelected(true);

    }

    @TargetApi(Build.VERSION_CODES.M)
    private void CheckPermission() {
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Fine Location");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("Coarse Location");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @TargetApi(Build.VERSION_CODES.M)
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }
        loadLocation();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Utils.getInstance().d("permision in Home");
        Utils.getInstance().d("permision in Home code " + requestCode);
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    loadLocation();
                } else {
                    Toast.makeText(context, "Some Permission is Denied", Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }

    private void loadLocation() {
        LocationTracker tracker = null;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            tracker = new LocationTracker(context, settings) {
                @Override
                public void onLocationFound(@NonNull Location location) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Utils.getInstance().d("lat" + location.getLatitude() + " long" + location.getLongitude());
                }

                @Override
                public void onTimeout() {
                }
            };
            tracker.startListening();
        } else {
            Utils.getInstance().d("Not");

        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng locatoin = new LatLng(Double.parseDouble(lat), Double.parseDouble(long_t));
        mMap.addMarker(new MarkerOptions().position(locatoin).title(title));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(locatoin));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
    }

    private void listner() {
        btn_get_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String provider = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                if ((latitude == 0 && longitude == 0) || !provider.contains("gps")) {
                    Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(gpsOptionsIntent);
                } else {
                    float destiLong = Float.parseFloat(long_t);
                    float destiLat = Float.parseFloat(lat);
                    Utils.getInstance().d("lat:" + destiLat + " long:" + destiLong);
                    String uri = String.format("http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f", latitude, longitude, destiLat, destiLong);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                }


            }
        });
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

}
