package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Manager.MediaController;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ValidFragment")
public class FragmentVideos2 extends Fragment {
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;
    private WebView webView;
    private RelativeLayout header_screen;
    private boolean isFragmentLoaded = false;
    private String title_web, link, banner_image, desc;
    private ImageView bannerimage;
    private TextView video_title, tv_desc;
    private boolean isOuter;
    private FragmentManager fragmentManager;
    private Button btn_menu, btn_player;
    private TextView tv_title;

    @SuppressLint("ValidFragment")
    public FragmentVideos2(Context context, RelativeLayout rl_fragments_header, String name, String data_link,
                           String banner_image, String desc, boolean isOuter, FragmentManager fragmentManager) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.title_web = name;
        this.link = data_link;
        this.banner_image = banner_image;
        this.desc = desc;
        this.isOuter = isOuter;
        this.fragmentManager = fragmentManager;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_video2, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Outer");
        init();
        listner();
        return rootView;
    }

    private void init() {
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        header_screen.setVisibility(View.VISIBLE);
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        bannerimage = (ImageView) rootView.findViewById(R.id.banner_image);
        ImageLoader.getInstance().displayImage(banner_image, bannerimage);
        Utils.getInstance().loadGlideBannerImage(context, bannerimage, banner_image);
        video_title = (TextView) rootView.findViewById(R.id.video_title);
        tv_desc = (TextView) rootView.findViewById(R.id.tv_desc);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        video_title.setText(title_web);
        tv_title.setText(title_web);
        tv_desc.setText(desc);
        webView = (WebView) rootView.findViewById(R.id.webView);
        if (CheckNetwork.isInternetAvailable(context)) {
            startWebView(link);
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        webView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        webView.onResume();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void startWebView(final String url) {
        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    if (MediaController.getInstance().isAudioPaused() == false) {
                        MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                    }
                }
                return false;
            }
        });
        Log.i("mytag", "Failed: ");
        webView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }
}
