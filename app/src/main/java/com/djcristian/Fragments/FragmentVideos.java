package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.google.android.gms.ads.AdView;

@SuppressLint("ValidFragment")
public class FragmentVideos extends Fragment {
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;
    private WebView webView;
    private ProgressDialog mProgressDialog;
    private TextView tv_title;
    private Button btn_menu, btn_player;
    private RelativeLayout header_screen;
    private boolean isFragmentLoaded = false;
    FragmentManager fm;
    private String title_web, link;
    private AdView mAdView;


    @SuppressLint("ValidFragment")
    public FragmentVideos(Context context, RelativeLayout rl_fragments_header, String name, String data_link, AdView mAdView) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.title_web = name;
        this.link = data_link;
        this.mAdView=mAdView;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_video, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Inner");
        init();
        listner();
        return rootView;
    }

    private void init() {
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_title.setSelected(true);
        tv_title.setText(title_web);
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        header_screen.setVisibility(View.VISIBLE);
        webView = (WebView) rootView.findViewById(R.id.webView);
        if (CheckNetwork.isInternetAvailable(context)) {
            startWebView(link);
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    private void startWebView(String url) {
        mAdView.setVisibility(View.GONE);
        webView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }

    @Override
    public void onPause() {
        super.onPause();
        webView.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();
        webView.onResume();
    }
}
