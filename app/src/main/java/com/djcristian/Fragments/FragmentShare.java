package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Model.GsonSocialPojo;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.WebInterface;

import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentShare extends Fragment implements View.OnClickListener {
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;
    private LinearLayout ll_fb, ll_insta, ll_twitter, ll_youtube;
    private ProgressDialog mProgressDialog;
    private String title_fb, title_insta, title_twit, title_you;
    private String android_link,text,ios_link,created_at,updated_at;
    private TextView tv_title;
    private Button btn_menu, btn_player,btn_share;
    private RelativeLayout header_screen;
    private int share_id;


    @SuppressLint("ValidFragment")
    public FragmentShare(Context context, RelativeLayout rl_fragments_header) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_share, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Inner");
        init();
        if (CheckNetwork.isInternetAvailable(context)) {
            getdata();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        listner();
        return rootView;
    }

    private void init() {
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_title.setText("SHARE MY APP");
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        btn_share = (Button) rootView.findViewById(R.id.btn_share);
        header_screen.setVisibility(View.VISIBLE);
        ll_fb = (LinearLayout) rootView.findViewById(R.id.ll_fb);
        ll_insta = (LinearLayout) rootView.findViewById(R.id.ll_insta);
        ll_twitter = (LinearLayout) rootView.findViewById(R.id.ll_twitter);
        ll_youtube = (LinearLayout) rootView.findViewById(R.id.ll_youtube);

    }

    private void listner() {
        ll_fb.setOnClickListener(this);
        ll_insta.setOnClickListener(this);
        ll_twitter.setOnClickListener(this);
        ll_youtube.setOnClickListener(this);
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });

        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = (text + "\n\n"+"Android Link : " + android_link + "\n\n" +"iOs Link :" + ios_link);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_fb:
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushInnerFragment(new FragmentFacebook(context, rl_fragments_header, false, title_fb), "Facebook", true);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.ll_insta:
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushInnerFragment(new FragmentInstagram(context, rl_fragments_header, false, title_insta), "Instagram", true);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.ll_twitter:
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushInnerFragment(new FragmentTwitter(context, rl_fragments_header, false, title_twit), "Twitter", true);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.ll_youtube:
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushInnerFragment(new FragmentYoutube(context, rl_fragments_header, false, title_you), "Soundcloud", true);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;

        }

    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }

    @SuppressLint("StaticFieldLeak")
    private void getdata() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                JSONObject jsonObject = new JSONObject();
                try {
                    response = WebInterface.getInstance().doGet(Const.SHARE);
                    Log.i("mytag", "Response : " + response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.i("mytag", " Final Response : " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        JSONObject data=jsonObject.getJSONObject("data");
                        share_id=data.getInt("share_id");
                        android_link=data.getString("android_link");
                        ios_link=data.getString("ios_link");
                        text=data.getString("text");
                        updated_at=data.getString("updated_at");
                        created_at=data.getString("created_at");                    }
                } catch (Exception e) {
                }
            }
        }.execute();
    }
}
