package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Model.GsonSocialPojo;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.WebInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentSocialMedia extends Fragment implements View.OnClickListener {
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;
    private LinearLayout ll_fb, ll_insta, ll_twitter, ll_youtube;
    private ArrayList<GsonSocialPojo> generelist = new ArrayList<>();
    private String fb, insta, twit, you;
    private ProgressDialog mProgressDialog;
    private String title_fb, title_insta, title_twit, title_you;
    private TextView tv_title;
    private Button btn_menu, btn_player,btn_share;
    private RelativeLayout header_screen;
    private boolean isFragmentLoaded = false;
    FragmentManager fm;

    @SuppressLint("ValidFragment")
    public FragmentSocialMedia(Context context, RelativeLayout rl_fragments_header) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_social_media, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Inner");
        init();
        if (CheckNetwork.isInternetAvailable(context)) {
            getdata();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        listner();
        return rootView;
    }

    private void init() {
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_title.setText("SOCIAL MEDIA");
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);

        header_screen.setVisibility(View.VISIBLE);
        ll_fb = (LinearLayout) rootView.findViewById(R.id.ll_fb);
        ll_insta = (LinearLayout) rootView.findViewById(R.id.ll_insta);
        ll_twitter = (LinearLayout) rootView.findViewById(R.id.ll_twitter);
        ll_youtube = (LinearLayout) rootView.findViewById(R.id.ll_youtube);

    }

    private void listner() {
        ll_fb.setOnClickListener(this);
        ll_insta.setOnClickListener(this);
        ll_twitter.setOnClickListener(this);
        ll_youtube.setOnClickListener(this);
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });


    }

    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        Intent browserIntent;
        switch (view.getId()) {
            case R.id.ll_fb:
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushInnerFragment(new FragmentFacebook(context, rl_fragments_header, false, title_fb), "Facebook", true);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.ll_insta:
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushInnerFragment(new FragmentInstagram(context, rl_fragments_header, false, title_insta), "Instagram", true);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.ll_twitter:
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushInnerFragment(new FragmentTwitter(context, rl_fragments_header, false, title_twit), "Twitter", true);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.ll_youtube:
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushInnerFragment(new FragmentYoutube(context, rl_fragments_header, false, title_you), "Soundcloud", true);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;

        }

    }

    private void pushInnerFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }

    @SuppressLint("StaticFieldLeak")
    private void getdata() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                JSONObject jsonObject = new JSONObject();
                try {
                    response = WebInterface.getInstance().doGet(Const.SOCIAL);
                    Log.i("mytag", "Response : " + response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.i("mytag", " Final Response : " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject innerdata = data.optJSONObject(i);
                            String ID = innerdata.getString("social_id").toString();
                            String name = innerdata.getString("social_name").toString();
                            String link = innerdata.getString("social_link").toString();
                            if (ID.equals("4")) {
                                insta = link;
                                title_insta = name;
                                Prefs.getPrefInstance().setValue(context, Const.INSTA, insta);
                            }
                            if (ID.equals("1")) {
                                fb = link;
                                title_fb = name;
                                Prefs.getPrefInstance().setValue(context, Const.FB, fb);
                            }
                            if (ID.equals("2")) {
                                twit = link;
                                title_twit = name;
                                Prefs.getPrefInstance().setValue(context, Const.TWIT, twit);
                            }
                            if (ID.equals("3")) {
                                you = link;
                                title_you = name;
                                Prefs.getPrefInstance().setValue(context, Const.SOUND, you);
                            }
                        }
                    }
                } catch (Exception e) {
                }
            }
        }.execute();
    }
}
