package com.djcristian.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;

import org.json.JSONObject;

import java.io.IOException;

@SuppressLint("ValidFragment")
public class FragmentSignUp extends Fragment {

    private View rootView;
    private Context context;
    private EditText edt_fname, edt_lname, edt_bname, edt_baddress, edt_btype, edt_mobile_number, edt_office_number;
    private Button btn_submit;
    String name, lname, bname, baddress, btype, mobile, office;
    private ProgressDialog mProgressDialog;
    private RelativeLayout rl_fragments_header;
    private boolean isOuter = false;
    private TextView tv_link1, tv_link2, tv_mobile, tv_office;
    private String mobile_number = "2013122553";
    private String office_number = "2017961730";
    private Button btn_menu, btn_player;
    private RelativeLayout header_screen;
    private TextView tv_title;
    private boolean isFragmentLoaded = false;
    FragmentManager fm;
    private NestedScrollView signup_scroll;


    @SuppressLint("ValidFragment")
    public FragmentSignUp(Context context, RelativeLayout rl_fragments_header, boolean isOuter) {
        this.rl_fragments_header = rl_fragments_header;
        this.context = context;
        this.isOuter = isOuter;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sign_up, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Inner");
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_title.setText("SIGN UP");
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        header_screen.setVisibility(View.VISIBLE);
        init();
        listner();
        return rootView;
    }

    private void init() {
        edt_fname = (EditText) rootView.findViewById(R.id.edt_fname);
        edt_lname = (EditText) rootView.findViewById(R.id.edt_lname);
        edt_bname = (EditText) rootView.findViewById(R.id.edt_bname);
        edt_baddress = (EditText) rootView.findViewById(R.id.edt_baddress);
        edt_btype = (EditText) rootView.findViewById(R.id.edt_btype);
        edt_mobile_number = (EditText) rootView.findViewById(R.id.edt_mobile_number);
        edt_office_number = (EditText) rootView.findViewById(R.id.edt_office_number);
        tv_link1 = (TextView) rootView.findViewById(R.id.tv_link1);
        tv_link2 = (TextView) rootView.findViewById(R.id.tv_link2);
        tv_mobile = (TextView) rootView.findViewById(R.id.tv_mobile);
        tv_office = (TextView) rootView.findViewById(R.id.tv_radio);
        btn_submit = (Button) rootView.findViewById(R.id.btn_signup);
        signup_scroll=rootView.findViewById(R.id.signup_scroll);
    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyData();
            }
        });
        tv_link1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "Promotion@lpmradio.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        });
        tv_link2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "Djnananina@lpmradio.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        });
        tv_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                call();
            }
        });
        tv_office.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                call1();
            }
        });


    }

    private void call() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mobile_number));
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        context.startActivity(intent);
    }

    private void call1() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + office_number));
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        context.startActivity(intent);
    }

    private void verifyData() {
        String name = edt_fname.getText().toString();
        String lname = edt_lname.getText().toString();
        String bname = edt_bname.getText().toString();
        String baddress = edt_baddress.getText().toString();
        String btype = edt_btype.getText().toString();
        String mobile = edt_mobile_number.getText().toString();
        String office = edt_office_number.getText().toString();
        Log.d("mytag", "values : " + name + lname + bname + baddress + btype + mobile + office);
        if (!name.equals("") && !name.equals(null)) {
            if (!lname.equals("") && !lname.equals(null)) {
                if (!bname.equals("") && !bname.equals(null)) {
                    if (!baddress.equals("") && !baddress.equals(null)) {
                        if (!btype.equals("") && !btype.equals(null)) {
                            if (CheckNetwork.isInternetAvailable(context)) {
                                callApiSubmitData();
                            } else {
                                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            edt_btype.setError("Please Enter Businees Type.");
                            edt_btype.requestFocus();
                        }
                    } else {
                        edt_baddress.setError("Please Enter Business Address.");
                        edt_baddress.requestFocus();
                    }
                } else {
                    edt_bname.setError("Please Enter Business Name.");
                    edt_bname.requestFocus();
                }
            } else {
                edt_lname.setError("Please Enter Last Name.");
                edt_lname.requestFocus();
            }
        } else {
            edt_fname.setError("Please Enter First Name.");
            edt_fname.requestFocus();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void callApiSubmitData() {
        final String fname = edt_fname.getText().toString();
        final String lname = edt_lname.getText().toString();
        final String bname = edt_bname.getText().toString();
        final String baddress = edt_baddress.getText().toString();
        final String btype = edt_btype.getText().toString();
        final String mobile = edt_mobile_number.getText().toString();
        final String office = edt_office_number.getText().toString();
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("fname", fname);
                    jsonObject.put("lname", lname);
                    jsonObject.put("b_name", bname);
                    jsonObject.put("b_address", baddress);
                    jsonObject.put("b_type", btype);
                    jsonObject.put("mob_no", mobile);
                    jsonObject.put("office", office);
                    jsonObject.put("email", "");
                    jsonObject.put("bdate", "");
                    jsonObject.put("description", "");
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.CONTACT_US, jsonObject.toString());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("Geners Response:: " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                Log.i("mytag", "inside class:FragmentGenresDJ \t " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        Utils.getInstance().toast(getActivity(), jsonObject.getString("msg"));
                        clearEditText();
                    }
                    if (jsonObject.getString("status").equals("0")) {
                        Utils.getInstance().toast(getActivity(), jsonObject.getString("msg"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    private void clearEditText() {
        edt_fname.setText("");
        edt_lname.setText("");
        edt_bname.setText("");
        edt_baddress.setText("");
        edt_btype.setText("");
        edt_mobile_number.setText("");
        edt_office_number.setText("");
        signup_scroll.fullScroll(View.FOCUS_UP);
        edt_fname.requestFocus();
    }
}
