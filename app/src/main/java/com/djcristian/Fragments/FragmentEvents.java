package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterEventsDJ;
import com.djcristian.Model.GsonGetEvents1;
import com.djcristian.Model.GsonGetEvents2;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.RestClient;
import com.djcristian.utility.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentEvents extends Fragment {
    ArrayList<GsonGetEvents2> GsonGetEvents2s;
    FragmentManager fm;
    RestClient restClient;
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;
    private RecyclerView rv_featured_djs;
    private ProgressDialog mProgressDialog;
    private AdapterEventsDJ adapter;
    private TextView tv_title;
    private Button btn_menu, btn_player;
    private RelativeLayout header_screen;
    private boolean isFragmentLoaded = false;
    private boolean isOuter = false;

    @SuppressLint("ValidFragment")
    public FragmentEvents(Context context, RelativeLayout rl_fragments_header, boolean isOuter) {
        this.context = context;
        this.isOuter = isOuter;
        this.rl_fragments_header = rl_fragments_header;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_events, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        if (isOuter) {
            rl_fragments_header.setTag("Outer");
        } else {
            rl_fragments_header.setTag("Inner");
        }
        init();
        listner();
        return rootView;
    }

    private void init() {
        rv_featured_djs = rootView.findViewById(R.id.rv_featured_djs);
        rv_featured_djs.setLayoutManager(new GridLayoutManager(context, 2));
        header_screen = rootView.findViewById(R.id.header_screen);
        tv_title = rootView.findViewById(R.id.tv_title);
        tv_title.setText("EVENTS");
        btn_menu = rootView.findViewById(R.id.btn_menu);
        btn_player = rootView.findViewById(R.id.btn_player);
        header_screen.setVisibility(View.VISIBLE);
        if (CheckNetwork.isInternetAvailable(context)) {
            getEvents();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

    private void getEvents() {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Loading");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();
        restClient = RestClient.getInstance();
        RestClient.setRestClientInterface(new RestClient.getRestClientInterface() {
            @Override
            public void onSuccess(String jsonResponse) {
                Gson gson = new Gson();
                GsonGetEvents1 gsonGetEvents1 = gson.fromJson(jsonResponse, GsonGetEvents1.class);
                GsonGetEvents2s = gsonGetEvents1.getGsonGetEvents2s();
                Utils.getInstance().d(GsonGetEvents2s.size() + "");
                rv_featured_djs = rootView.findViewById(R.id.rv_featured_djs);
                rv_featured_djs.setLayoutManager(new LinearLayoutManager(context));
                adapter = new AdapterEventsDJ(context, GsonGetEvents2s, getFragmentManager(), rl_fragments_header);
                rv_featured_djs.setAdapter(adapter);
                mProgressDialog.dismiss();
            }

            @Override
            public void onFail(String msg) {
                mProgressDialog.dismiss();
            }

            @Override
            public void onNetworkError(String msg) {
                mProgressDialog.dismiss();
            }
        });
        restClient.doGetRequest(context, Const.EVENTS);

    }
}
