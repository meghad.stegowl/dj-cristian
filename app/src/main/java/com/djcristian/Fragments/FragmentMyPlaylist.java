package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterMyPlaylist;
import com.djcristian.Model.GsonPlaylistData;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentMyPlaylist extends Fragment {
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;
    private RecyclerView rv_featured_djs;
    private ProgressDialog mProgressDialog;
    private AdapterMyPlaylist adapter;
    private boolean isOuter = false;
    private TextView tv_title;
    private Button btn_menu, btn_player;
    private RelativeLayout header_screen;
    private boolean isFragmentLoaded = false;
    FragmentManager fm;
    private String status;
    ArrayList<GsonPlaylistData> gsonGetPlaylist2ArrayList = new ArrayList<>();


    @SuppressLint("ValidFragment")
    public FragmentMyPlaylist(Context context, RelativeLayout rl_fragments_header, boolean isOuter) {
        this.context = context;
        this.isOuter = isOuter;
        this.rl_fragments_header = rl_fragments_header;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my_playlist, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        if (isOuter) {
            rl_fragments_header.setTag("Outer");
        } else {
            rl_fragments_header.setTag("Inner");

        }
        init();
        listner();
        return rootView;
    }

    private void init() {
        rv_featured_djs = (RecyclerView) rootView.findViewById(R.id.rv_featured_djs);
        rv_featured_djs.setLayoutManager(new LinearLayoutManager(context));
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_title.setText("MY PLAYLIST");
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        header_screen.setVisibility(View.VISIBLE);
        if (CheckNetwork.isInternetAvailable(context)) {
            getMyPlaylist();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

    @SuppressLint("StaticFieldLeak")
    private void getMyPlaylist() {
        gsonGetPlaylist2ArrayList = new ArrayList<>();
        new AsyncTask<Void, Void, String>() {


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("u_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    response = WebInterface.getInstance().doPostRequest(Const.GET_PLAYLIST, jsonObject.toString());


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("Response :" + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    try {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject data = jsonArray.getJSONObject(i);
                            String playlist_id = data.getString("playlist_id");
                            String name = data.getString("name");
                            String total_count = data.getString("total_count");
                            gsonGetPlaylist2ArrayList.add(new GsonPlaylistData(playlist_id, name, total_count));
                        }
                        rv_featured_djs.setLayoutManager(new LinearLayoutManager(context));
                        adapter = new AdapterMyPlaylist(context, gsonGetPlaylist2ArrayList, getFragmentManager(), rl_fragments_header);
                        rv_featured_djs.setAdapter(adapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }
}
