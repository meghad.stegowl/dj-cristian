package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterFeaturingDjsSongs;
import com.djcristian.Manager.MediaController;
import com.djcristian.Model.GsonFeaturingDjsSongs2;
import com.djcristian.Model.PojoSongForPlayer;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentFeaturedDjsSong extends Fragment {
    private Context context;
    private RelativeLayout rl_fragments_header, header_screen;
    private View rootView;
    private RecyclerView rv_featured_djs_song;
    private ProgressDialog mProgressDialog;
    private AdapterFeaturingDjsSongs adapter;
    private String id, title;
    private TextView tv_title, tv_single_album_name;
    private boolean isFragmentLoaded = false;
    private Button btn_menu, btn_player;
    private FragmentManager fm;
    private ArrayList<GsonFeaturingDjsSongs2> gsonPlaylistSongs2s = new ArrayList<>();
    private String insta_url;


    @SuppressLint("ValidFragment")
    public FragmentFeaturedDjsSong(Context context, RelativeLayout rl_fragments_header, String insta_url) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.insta_url = insta_url;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_featured_djs_song, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Inner");
        Bundle args = getArguments();
        id = args.getString("id");
        title = args.getString("name");
        init();
        listner();
        return rootView;
    }

    private void init() {
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_single_album_name = (TextView) rootView.findViewById(R.id.tv_single_album_name);
        tv_title.setText(title);
        tv_single_album_name.setText(title);
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        header_screen.setVisibility(View.VISIBLE);
        rv_featured_djs_song = (RecyclerView) rootView.findViewById(R.id.rv_featured_djs_song);
        rv_featured_djs_song.setLayoutManager(new LinearLayoutManager(context));
        if (CheckNetwork.isInternetAvailable(context)) {
            getFeaturedDjsSongs();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void onLoadSong(String url) {
        Utils.getInstance().d("url" + url);
        for (GsonFeaturingDjsSongs2 item : gsonPlaylistSongs2s) {
            if (item.getUrl().equals(url)) {
                item.setNowPlaying("1");
            } else {
                item.setNowPlaying("0");
            }
            adapter = new AdapterFeaturingDjsSongs(context, gsonPlaylistSongs2s, getFragmentManager());
            rv_featured_djs_song.setAdapter(adapter);

        }

    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
        ImageView iv_instagram = (ImageView) rootView.findViewById(R.id.iv_instagram);
        iv_instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse(insta_url));
                startActivity(browserIntent);
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

    @SuppressLint("StaticFieldLeak")
    private void getFeaturedDjsSongs() {
        final PojoSongForPlayer songDetail = MediaController.getInstance().getPlayingSongDetail();
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("u_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    jsonObject.put("feature_dj_id", id);
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.GET_SONG_BY_FEATURING_DJS, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("mytag" + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject data1 = data.getJSONObject(i);
                            String song_id = data1.getString("song_id");
                            String like_status = data1.getString("like_status");
                            String song_time = data1.getString("song_time");
                            String totle_like = data1.getString("total_likes");
                            String song_name = data1.getString("song_name");
                            String song_url = data1.getString("song_url");
                            String image = data1.getString("image");
                            String artists_name = data1.getString("artist_name");
                            String nowPlaying;
                            if (songDetail != null) {
                                if (songDetail.getS_url().equals(song_url)) {
                                    nowPlaying = "1";
                                } else {
                                    nowPlaying = "0";
                                }
                            } else {
                                nowPlaying = "0";
                            }
                            gsonPlaylistSongs2s.add(new GsonFeaturingDjsSongs2(song_id, song_name, song_url, image, like_status, totle_like, song_time, artists_name, nowPlaying));
                        }
                        adapter = new AdapterFeaturingDjsSongs(context, gsonPlaylistSongs2s, getFragmentManager());
                        rv_featured_djs_song.setAdapter(adapter);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
   }
}
