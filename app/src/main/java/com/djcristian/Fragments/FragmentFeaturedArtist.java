package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterFeaturedArtist;
import com.djcristian.Adapter.AdapterFeaturingDjsSongs;
import com.djcristian.Adapter.ImageAdapter;
import com.djcristian.Manager.MediaController;
import com.djcristian.Model.GsonFeaturingDjsSongs2;
import com.djcristian.Model.PojoAlbum;
import com.djcristian.Model.PojoSongForPlayer;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentFeaturedArtist extends Fragment {
    private Context context;
    private RelativeLayout rl_fragments_header, header_screen;
    private TextView tv_title;
    private View rootView;
    private RecyclerView rv_featured_artist;
    private ProgressDialog mProgressDialog;
    private boolean isOuter = false;
    private Button btn_menu, btn_player;
    private boolean isFragmentLoaded = false;
    ArrayList<GsonFeaturingDjsSongs2> gsonPlaylistSongs2s;
    private AdapterFeaturingDjsSongs adapterFeaturingDjsSongs;
    FragmentManager fm;
    ArrayList<PojoAlbum> gsonGetPlaylist2ArrayList = new ArrayList<>();
    private Gallery gallery;
    private Handler handler = new Handler();
    private String Check_Name;


    @SuppressLint("ValidFragment")
    public FragmentFeaturedArtist(Context context, RelativeLayout rl_fragments_header, boolean isOuter, FragmentManager fm) {
        this.context = context;
        this.isOuter = isOuter;
        this.rl_fragments_header = rl_fragments_header;
        this.fm = fm;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_featured_artist, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        if (isOuter) {
            rl_fragments_header.setTag("Outer");
        } else {
            rl_fragments_header.setTag("Inner");
        }
        init();
        listener();
        return rootView;
    }

    private void init() {
        gallery = (Gallery) rootView.findViewById(R.id.gallery);
        rv_featured_artist = (RecyclerView) rootView.findViewById(R.id.rv_album);
        rv_featured_artist.setLayoutManager(new LinearLayoutManager(context));
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_title.setText("FEATURED ARTIST");
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        header_screen.setVisibility(View.VISIBLE);
        if (CheckNetwork.isInternetAvailable(context)) {
            getFeaturedArtist();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        gallery.setSpacing(2);


    }

    public void onLoadSong(String url) {
        Utils.getInstance().d("url" + url);
        for (GsonFeaturingDjsSongs2 item : gsonPlaylistSongs2s) {
            if (item.getUrl().equals(url)) {
                item.setNowPlaying("1");
            } else {
                item.setNowPlaying("0");
            }
            AdapterFeaturingDjsSongs adapterFeaturingDjsSongs = new AdapterFeaturingDjsSongs(context, gsonPlaylistSongs2s, getFragmentManager());
            rv_featured_artist.setAdapter(adapterFeaturingDjsSongs);
        }

    }

    private void getSongs(final String id) {
        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (CheckNetwork.isInternetAvailable(context)) {
                    getFeaturedDjsSongs(id);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        }, 1000);


    }


    private void listener() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void getFeaturedArtist() {
        gsonGetPlaylist2ArrayList = new ArrayList<>();
        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                JSONObject jsonObject = new JSONObject();
                try {
                    response = WebInterface.getInstance().doGet(Const.GET_FEATURING_ARTIST);
                    Log.i("mytag", "Response : " + response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.i("mytag", " Final Response : " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject innerdata = data.optJSONObject(i);
                            String genres_id = innerdata.getString("artist_id").toString();
                            String genres_name = innerdata.getString("artist_name").toString();
                            String image = innerdata.getString("image").toString();
                            String insta_url = innerdata.getString("insta_url").toString();
                            gsonGetPlaylist2ArrayList.add(new PojoAlbum(genres_id, genres_name, image, insta_url));
                        }
                    }
                    if (gsonGetPlaylist2ArrayList.size() > 0) {
                        setAlbum();
                    }
                } catch (Exception e) {
                }
            }
        }.execute();

    }

    private void setAlbum() {
        final ImageAdapter adapter = new ImageAdapter(context, gsonGetPlaylist2ArrayList);
        gallery.setAdapter(adapter);
        gallery.setSelection(0);
        gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                PojoAlbum item = gsonGetPlaylist2ArrayList.get(position);
                adapter.setSelectedItem(position);
                getSongs(item.getGenres_id());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void getFeaturedDjsSongs(final String id) {
        final PojoSongForPlayer songDetail = MediaController.getInstance().getPlayingSongDetail();
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("artist_id", id);
                    jsonObject.put("u_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.GET_SONG_BY_FEATURING_ARTIST, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("mytag" + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        gsonPlaylistSongs2s = new ArrayList<GsonFeaturingDjsSongs2>();
                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject data1 = data.getJSONObject(i);
                            String song_id = data1.getString("song_id");
                            String like_status = data1.getString("like_status");
                            String song_time = data1.getString("song_time");
                            String totle_like = data1.getString("total_likes");
                            String song_name = data1.getString("song_name");
                            String song_url = data1.getString("song_url");
                            String image = data1.getString("image");
                            String artists_name = data1.getString("artist_name");
                            String nowPlaying;
                            if (songDetail != null) {
                                if (songDetail.getS_url().equals(song_url)) {
                                    nowPlaying = "1";
                                } else {
                                    nowPlaying = "0";
                                }
                            } else {
                                nowPlaying = "0";
                            }
                            gsonPlaylistSongs2s.add(new GsonFeaturingDjsSongs2(song_id, song_name, song_url, image, like_status
                                    , totle_like, song_time, artists_name, nowPlaying));
                        }
                        Utils.getInstance().d("size " + gsonPlaylistSongs2s.size());
                        adapterFeaturingDjsSongs = new AdapterFeaturingDjsSongs(context, gsonPlaylistSongs2s, getFragmentManager());
                        rv_featured_artist.setAdapter(adapterFeaturingDjsSongs);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();

    }
}
