package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterMyFav;
import com.djcristian.Manager.MediaController;
import com.djcristian.Model.GsonFavDjsSongs2;
import com.djcristian.Model.PojoSongForPlayer;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentMyFav extends Fragment {
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;
    private RecyclerView rv_featured_djs_song;
    private ProgressDialog mProgressDialog;
    private AdapterMyFav adapter;
    private String id, title;
    private TextView tv_title;
    public  static TextView tv_name;
    private Button btn_menu, btn_player;
    private RelativeLayout header_screen;
    private boolean isFragmentLoaded = false;
    FragmentManager fm;
    private Boolean isOuter = false;
    private Boolean Count;
    private ArrayList<GsonFavDjsSongs2> gsonPlaylistSongs2s = new ArrayList<>();


    @SuppressLint("ValidFragment")
    public FragmentMyFav(Context context, RelativeLayout rl_fragments_header, boolean isOuter) {
        this.context = context;
        this.isOuter = isOuter;
        this.rl_fragments_header = rl_fragments_header;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my_fav, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        if (isOuter) {
            rl_fragments_header.setTag("Outer");
        } else {
            rl_fragments_header.setTag("Inner");

        }
        init();
        listner();
        return rootView;
    }

    private void init() {
        rv_featured_djs_song = (RecyclerView) rootView.findViewById(R.id.rv_featured_djs_song);
        rv_featured_djs_song.setLayoutManager(new LinearLayoutManager(context));
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        tv_name = (TextView) rootView.findViewById(R.id.tv_name);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_title.setText("MY FAVORITES");
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        header_screen.setVisibility(View.VISIBLE);
        if (CheckNetwork.isInternetAvailable(context)) {
            getFavSongs();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void onLoadSong(String url) {
        Utils.getInstance().d("url" + url);
        for (GsonFavDjsSongs2 item : gsonPlaylistSongs2s) {
            if (item.getUrl().equals(url)) {
                item.setNowPlaying("1");
            } else {
                item.setNowPlaying("0");
            }
            adapter = new AdapterMyFav(context, gsonPlaylistSongs2s, getFragmentManager());
            rv_featured_djs_song.setAdapter(adapter);

        }

    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

    @SuppressLint("StaticFieldLeak")
    private void getFavSongs() {
        final PojoSongForPlayer songDetail = MediaController.getInstance().getPlayingSongDetail();
        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("u_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.GET_FAV, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("mytag", "Favorites Response : " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject inner_data = data.getJSONObject(i);
                            String song_id = inner_data.getString("song_id");
                            String like_status = inner_data.getString("like_status");
                            String song_time = inner_data.getString("song_time");
                            String totle_like = inner_data.getString("total_likes");
                            String song_name = inner_data.getString("song_name");
                            String song_url = inner_data.getString("song_url");
                            String image = inner_data.getString("image");
                            String artists_name = inner_data.getString("artist_name");
                            String fav_id = inner_data.getString("fav_id");
                            Log.i("mytag", "Arryyyyyyy" + song_url);
                            String nowPlaying;
                            if (songDetail != null) {
                                if (songDetail.getS_url().equals(song_url)) {
                                    nowPlaying = "1";
                                } else {
                                    nowPlaying = "0";
                                }
                            } else {
                                nowPlaying = "0";
                            }
                            gsonPlaylistSongs2s.add(new GsonFavDjsSongs2(song_id, song_name, song_url, image, like_status, totle_like, song_time, artists_name, nowPlaying, fav_id));
                        }
                        Log.i("mytag", "Song_Url _vhmcgnhzxcvbn mdfghj" + gsonPlaylistSongs2s);
                        adapter = new AdapterMyFav(context, gsonPlaylistSongs2s, getFragmentManager());
                        rv_featured_djs_song.setAdapter(adapter);
                        tv_name.setText(gsonPlaylistSongs2s.size() + " Tracks");
                    }

                } catch (Exception e) {
                }
            }
        }.execute();

    }
}
