package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterPlaylistSingleSong;
import com.djcristian.CustomView.CustomHeader;
import com.djcristian.Manager.MediaController;
import com.djcristian.Model.GsonPlaylistSongs2;
import com.djcristian.Model.PojoSongForPlayer;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class FragmentMyPlaylistSong extends Fragment {
    private Context context;
    private RelativeLayout rl_fragments_header;
    private View rootView;
    private RecyclerView rv_featured_djs_song;
    private ProgressDialog mProgressDialog;
    private AdapterPlaylistSingleSong adapter;
    private String id, title;
    private ArrayList<GsonPlaylistSongs2> gsonPlaylistSongs2s = new ArrayList<>();
    private TextView tv_title, tv_single_title, tv_single_album_name;
    private Button btn_menu, btn_player;
    private RelativeLayout header_screen;


    @SuppressLint("ValidFragment")
    public FragmentMyPlaylistSong(Context context, RelativeLayout rl_fragments_header) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_featured_playlist_single_song, container, false);
        CustomHeader.setInnerFragment(getActivity(), rl_fragments_header);
        Bundle args = getArguments();
        id = args.getString("id");
        title = args.getString("name");
        init();
        listener();
        return rootView;
    }

    private void init() {
        rv_featured_djs_song = (RecyclerView) rootView.findViewById(R.id.rv_featured_djs_song);
        rv_featured_djs_song.setLayoutManager(new LinearLayoutManager(context));
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_single_album_name = (TextView) rootView.findViewById(R.id.tv_single_album_name);
        tv_title.setText(title);
        tv_single_album_name.setText(title);
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        header_screen.setVisibility(View.VISIBLE);
        if (CheckNetwork.isInternetAvailable(context)) {
            getPlaylistSong();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void listener() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    public void onLoadSong(String url) {
        Utils.getInstance().d("url" + url);
        for (GsonPlaylistSongs2 item : gsonPlaylistSongs2s) {
            if (item.getSong_url().equals(url)) {
                item.setNowPlaying("1");
            } else {
                item.setNowPlaying("0");
            }
            adapter = new AdapterPlaylistSingleSong(context, gsonPlaylistSongs2s, getFragmentManager());
            rv_featured_djs_song.setAdapter(adapter);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void getPlaylistSong() {
        final PojoSongForPlayer songDetail = MediaController.getInstance().getPlayingSongDetail();
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("playlist_id", id);
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.GET_SONGS_BY_PLAYLIST, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("mytag" + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject data1 = data.getJSONObject(i);
                            String song_id = data1.getString("song_id");
                            String like_status = data1.getString("like_status");
                            String song_time = data1.getString("song_time");
                            String totle_like = data1.getString("total_likes");
                            String song_name = data1.getString("song_name");
                            String song_url = data1.getString("song_url");
                            String artists_name = data1.getString("artist_name");
                            String sip_id = data1.getString("sip_id");
                            String image = data1.getString("image");
                            String nowPlaying;
                            if (songDetail != null) {
                                if (songDetail.getS_url().equals(song_url)) {
                                    nowPlaying = "1";
                                } else {
                                    nowPlaying = "0";
                                }
                            } else {
                                nowPlaying = "0";
                            }
                            gsonPlaylistSongs2s.add(new GsonPlaylistSongs2(song_id, song_name, song_url, image, like_status, totle_like, song_time, artists_name, nowPlaying, sip_id));
                        }
                        adapter = new AdapterPlaylistSingleSong(context, gsonPlaylistSongs2s, getFragmentManager());
                        rv_featured_djs_song.setAdapter(adapter);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();

    }

}