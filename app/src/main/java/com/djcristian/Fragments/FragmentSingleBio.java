package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.CustomView.CustomHeader;
import com.djcristian.Model.Biographies;
import com.djcristian.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ValidFragment")
public class FragmentSingleBio extends Fragment {

    FragmentManager fm;
    private View rootView;
    private Context context;
    private RelativeLayout r1_header;
    private TextView tv_title, tv_artist_name, tv_desc;
    private Button btn_menu, btn_player;
    private DisplayImageOptions options, optionsThumb;
    private boolean isOuter = false;
    private ImageView iv_banner_bio, iv_artist_thumb, iv_insta;
    private Biographies item;

    public FragmentSingleBio(Context context, RelativeLayout rl_fragments_header, boolean is_outer, Biographies item) {
        this.context = context;
        this.item = item;
        this.r1_header = rl_fragments_header;
        this.isOuter = is_outer;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_single_bio, container, false);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.andu_header_events)
                .showImageForEmptyUri(R.drawable.andu_header_events)
                .showImageOnFail(R.drawable.andu_header_events)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        optionsThumb = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.andu_placeholder)
                .showImageForEmptyUri(R.drawable.andu_placeholder)
                .showImageOnFail(R.drawable.andu_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        CustomHeader.setInnerFragment(getActivity(), r1_header);
        init();
        listner();
        return rootView;
    }


    private void init() {
        tv_artist_name = rootView.findViewById(R.id.tv_artist_name);
        tv_artist_name.setText(item.getArtist_name());
        tv_desc = rootView.findViewById(R.id.tv_desc);
        tv_desc.setText(item.getBiography());
        tv_title = rootView.findViewById(R.id.tv_title);
        tv_title.setText(item.getArtist_name());
        tv_title.setSelected(true);
        iv_banner_bio = rootView.findViewById(R.id.iv_banner_bio);
        iv_artist_thumb = rootView.findViewById(R.id.iv_artist_thumb);
        iv_insta = rootView.findViewById(R.id.iv_insta);
        btn_menu = rootView.findViewById(R.id.btn_menu);
        btn_player = rootView.findViewById(R.id.btn_player);
        ImageLoader.getInstance().displayImage(item.getBanner(), iv_banner_bio, options);
        ImageLoader.getInstance().displayImage(item.getThumb(), iv_artist_thumb, optionsThumb);


    }


    private void listner() {
        iv_insta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(item.getInsta_url())));
            }
        });
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }
}
