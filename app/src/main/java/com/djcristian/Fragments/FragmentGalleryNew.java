package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterGalleryNew;
import com.djcristian.Model.GalleryImages;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


@SuppressLint("ValidFragment")
public class FragmentGalleryNew extends Fragment {
    FragmentManager fm;
    private Context context;
    private View rootView;
    private ProgressDialog mProgressDialog;
    private RecyclerView rv_gallery;
    private ArrayList<GalleryImages> galleryImagesArrayList;
    private RelativeLayout rl_fragments_header;
    private TextView tv_title;
    private Button btn_menu, btn_player;
    private RelativeLayout header_screen;
    private boolean isFragmentLoaded = false;
    private boolean isOuter;
    private String fid, name;

    @SuppressLint("ValidFragment")
    public FragmentGalleryNew(Context context, RelativeLayout rl_fragments_header, boolean isOuter, String fid, String name) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
        this.fid = fid;
        this.name = name;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_gallery_new, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Inner");
        header_screen = rootView.findViewById(R.id.header_screen);
        tv_title = rootView.findViewById(R.id.tv_title);
        tv_title.setText(name);
        tv_title.setSelected(true);
        btn_menu = rootView.findViewById(R.id.btn_menu);
        btn_player = rootView.findViewById(R.id.btn_player);
        header_screen.setVisibility(View.VISIBLE);
        init();
        listner();
        return rootView;
    }

    private void init() {
        rv_gallery = rootView.findViewById(R.id.rv_gallery);
        rv_gallery.setLayoutManager(new GridLayoutManager(context, 3));
        if (CheckNetwork.isInternetAvailable(context)) {
            getGalleryImages();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }

    @SuppressLint("StaticFieldLeak")
    private void getGalleryImages() {
        galleryImagesArrayList = new ArrayList<>();
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(getActivity());
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {

                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("folder_id", fid);
                    response = WebInterface.getInstance().doPostRequest(Const.GET_ALL_IMGS, jsonObject.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("Geners Response:: " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();

                JSONObject mJsonObject;
                try {
                    mJsonObject = new JSONObject(response);
                    if (mJsonObject.getString("status").equals("1")) {
                        JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject object = mJsonArray.optJSONObject(i);
                            String image = object.getString("image");
                            galleryImagesArrayList.add(new GalleryImages(image));
                        }
                        AdapterGalleryNew adapterGalleryNew = new AdapterGalleryNew(context, galleryImagesArrayList, getFragmentManager());
                        rv_gallery.setAdapter(adapterGalleryNew);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }
}
