package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


@SuppressLint("ValidFragment")
public class FragmentBooking extends Fragment implements View.OnClickListener, View.OnFocusChangeListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    String partyType = null;
    private View rootView;
    private Context context;
    private EditText et_name, et_number, et_email, edt_date, edt_time, et_address, et_city, et_state, et_zipCode, et_country;
    private Button btnSubmit;
    private CheckBox cb_night_club, cb_lounge, cb_festival, cb_private_function;
    private ProgressDialog mProgressDialog;
    private RelativeLayout rl_fragments_header, header_screen;
    private TextView tv_title;
    private Button btn_menu, btn_player;
    private boolean isOuter = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_booking_dj, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        if (isOuter) {
            rl_fragments_header.setTag("Outer");
        } else {
            rl_fragments_header.setTag("Inner");
        }
        init();
        listner();
        return rootView;
    }

    private void init() {
        tv_title = rootView.findViewById(R.id.tv_title);
        tv_title.setText("BOOKINGS");
        header_screen = rootView.findViewById(R.id.header_screen);
        btn_player = rootView.findViewById(R.id.btn_player);
        btn_menu = rootView.findViewById(R.id.btn_menu);
        header_screen.setVisibility(View.VISIBLE);
        cb_night_club = rootView.findViewById(R.id.cb_night_club);
        cb_lounge = rootView.findViewById(R.id.cb_lounge);
        cb_festival = rootView.findViewById(R.id.cb_festival);
        cb_private_function = rootView.findViewById(R.id.cb_private_function);
        et_name = rootView.findViewById(R.id.et_booking_details1);
        et_number = rootView.findViewById(R.id.et_booking_details2);
        et_email = rootView.findViewById(R.id.et_booking_details3);
        edt_date = rootView.findViewById(R.id.edt_date);
        edt_time = rootView.findViewById(R.id.edt_time);
        et_address = rootView.findViewById(R.id.et_booking_details6);
        et_city = rootView.findViewById(R.id.et_booking_details7);
        et_state = rootView.findViewById(R.id.et_booking_details8);
        et_zipCode = rootView.findViewById(R.id.et_booking_details9);
        et_country = rootView.findViewById(R.id.et_booking_details10);
        edt_time.setOnClickListener(this);
        edt_time.setOnFocusChangeListener(this);
        edt_date.setOnClickListener(this);
        edt_date.setOnFocusChangeListener(this);
        btnSubmit = rootView.findViewById(R.id.btn_submit);
    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VarifyData();
            }
        });
        cb_private_function.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    partyType = "Private Function";
                    cb_festival.setChecked(false);
                    cb_lounge.setChecked(false);
                    cb_night_club.setChecked(false);
                }
            }
        });
        cb_festival.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    partyType = "Festival";
                    cb_private_function.setChecked(false);
                    cb_lounge.setChecked(false);
                    cb_night_club.setChecked(false);
                }

            }
        });
        cb_lounge.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    partyType = "Lounge";
                    cb_festival.setChecked(false);
                    cb_private_function.setChecked(false);
                    cb_night_club.setChecked(false);
                }

            }
        });
        cb_night_club.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    partyType = "Night Club";
                    cb_festival.setChecked(false);
                    cb_lounge.setChecked(false);
                    cb_private_function.setChecked(false);
                }

            }
        });

    }

    private void VarifyData() {
        String u_id = Prefs.getPrefInstance().getValue(context, Const.USER_ID, "");
        String full_name = et_name.getText().toString();
        String phone_no = et_number.getText().toString();
        String email = et_email.getText().toString();
        String date = edt_date.getText().toString();
        String time = edt_time.getText().toString();
        String address = et_address.getText().toString();
        String city = et_city.getText().toString();
        String state = et_state.getText().toString();
        String zip = et_zipCode.getText().toString();
        String country = et_country.getText().toString();
        String party_type = partyType;
        if (!u_id.isEmpty() && !full_name.isEmpty() && Utils.getInstance().isValidEmail(email) && !date.isEmpty() && !time.isEmpty() && !address.isEmpty() && !city.isEmpty() && !state.isEmpty() && !zip.isEmpty() && !country.isEmpty() && party_type != null) {
            Utils.getInstance().d(u_id + "\n" + full_name + "\n" + phone_no + "\n" + email + "\n" + date + "\n" + time + "\n" + address + "\n" + city + "\n" + state + "\n" + zip + "\n" + country + "\n" + party_type);
            if (CheckNetwork.isInternetAvailable(context)) {
                submitData(u_id, full_name, phone_no, email, date, time, address, city, state, zip, country, party_type);
            } else {
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
            clearAllEdditText();
        } else {
            if (full_name.isEmpty()) {
                et_name.setError("Please Enter Full Name");
                et_name.requestFocus();
            } else if (!Utils.getInstance().isValidEmail(email)) {
                et_email.setError("Please Enter valid Email");
                et_email.requestFocus();
            } else if (date.isEmpty()) {
                edt_date.setError("Please Enter valid Date");
                edt_date.requestFocus();
            } else if (time.isEmpty()) {
                edt_time.setError("Please Enter Time");
                edt_time.requestFocus();
            } else if (address.isEmpty()) {
                et_address.setError("Please Enter Address");
                et_address.requestFocus();
            } else if (city.isEmpty()) {
                et_city.setError("Please Enter City");
                et_city.requestFocus();
            } else if (state.isEmpty()) {
                et_state.setError("Please Enter State");
                et_state.requestFocus();
            } else if (zip.isEmpty()) {
                et_zipCode.setError("Please Enter Zip Code");
                et_zipCode.requestFocus();
            } else if (country.isEmpty()) {
                et_country.setError("Please Enter Country");
                et_country.requestFocus();
            } else if (party_type == null) {
                Utils.getInstance().toast(getActivity(), "please select party type");
            }
        }
    }

    private void clearAllEdditText() {
        et_name.setText("");
        et_number.setText("");
        et_email.setText("");
        edt_date.setText("");
        edt_time.setText("");
        et_address.setText("");
        et_city.setText("");
        et_state.setText("");
        et_zipCode.setText("");
        et_country.setText("");
        cb_festival.setSelected(false);
        cb_lounge.setSelected(false);
        cb_night_club.setSelected(false);
        cb_private_function.setSelected(false);
    }

    @SuppressLint("StaticFieldLeak")
    private void submitData(final String u_id, final String full_name, final String phone_no, final String email, final String date, final String time, final String address, final String city, final String state, final String zip, final String country, final String party_type) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                String response = null;
                try {
                    jsonObject.put("user_id", u_id);
                    jsonObject.put("name", full_name);
                    jsonObject.put("mob_no", phone_no);
                    jsonObject.put("email", email);
                    jsonObject.put("date", date);
                    jsonObject.put("time", time);
                    jsonObject.put("address", address);
                    jsonObject.put("city", city);
                    jsonObject.put("state", state);
                    jsonObject.put("zip", zip);
                    jsonObject.put("country", country);
                    jsonObject.put("party_type", party_type);
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.BOOKING, jsonObject.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        Utils.getInstance().toast(getActivity(), jsonObject.getString("msg"));
                    }
                    if (jsonObject.getString("status").equals("0")) {
                        Utils.getInstance().toast(getActivity(), jsonObject.getString("msg"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edt_date:
                edt_date.setError(null);
                datePick(view.getId());
                break;
            case R.id.edt_time:
                edt_time.setError(null);
                String date = edt_date.getText().toString().trim();
                boolean flag = false;
                flag = !date.equals(null) && !date.equals("");
                timePick(view.getId(), flag);
                break;
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (b) {
            switch (view.getId()) {
                case R.id.edt_date:
                    edt_date.setError(null);
                    datePick(view.getId());
                    break;
                case R.id.edt_time:
                    edt_time.setError(null);
                    String date = edt_date.getText().toString().trim();
                    boolean flag = false;
                    flag = !date.equals(null) && !date.equals("");
                    timePick(view.getId(), flag);
                    break;
            }
        }
    }

    private void timePick(int id, boolean flag) {
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance((TimePickerDialog.OnTimeSetListener) context,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true
        );
        if (flag == true) {
            String selected_date = edt_date.getText().toString().trim();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
            Date date2 = new Date(System.currentTimeMillis());
            String current_date = formatter.format(date2);
            if (selected_date.equals(current_date)) {
                tpd.setMinTime(now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE) + 1, now.get(Calendar.SECOND));
            }
        }
        tpd.show(getFragmentManager(), "Datepickerdialog");
        tpd.dismissOnPause(true);
        tpd.setOnTimeSetListener(this);
    }

    private void datePick(int id) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance((DatePickerDialog.OnDateSetListener) context,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(Calendar.getInstance());
        dpd.show(getFragmentManager(), "Datepickerdialog");
        dpd.dismissOnPause(true);
        dpd.setOnDateSetListener(this);

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        edt_time.setText(null);
        int day = dayOfMonth;
        int yy = year;
        int month = ++monthOfYear;
        String str_day = day < 10 ? "0" + day : "" + day;
        String str_month = month < 10 ? "0" + month : "" + month;
        edt_date.setText(yy + "/" + str_month + "/" + str_day);

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        edt_time.setText(hourString + ":" + minuteString);
    }
}
