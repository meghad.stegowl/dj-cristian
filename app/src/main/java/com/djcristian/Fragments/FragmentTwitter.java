package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.R;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.RestClient;
import com.djcristian.utility.WebInterface;

import org.json.JSONArray;
import org.json.JSONObject;

@SuppressLint("ValidFragment")
public class FragmentTwitter extends Fragment {

    private View rootView;
    private Context context;
    private ProgressDialog mProgressDialog;
    private WebView webView;
    private RelativeLayout rl_fragments_header;
    private String link;
    private boolean isOuter = false;
    private String fb, title_twit;
    private Button btn_menu, btn_player;
    private RelativeLayout header_screen;
    private boolean isFragmentLoaded = false;
    FragmentManager fm;


    @SuppressLint("ValidFragment")
    public FragmentTwitter(Context context, RelativeLayout rl_fragments_header, boolean isOuter, String title_twit) {
        this.context = context;
        this.rl_fragments_header = rl_fragments_header;
        this.isOuter = isOuter;
        this.title_twit = title_twit;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_webview, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Inner");
        init();
        listner();
        return rootView;
    }

    private void init() {
        TextView tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_title.setText(title_twit);
        btn_menu = (Button) rootView.findViewById(R.id.btn_menu);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        btn_player = (Button) rootView.findViewById(R.id.btn_player);
        header_screen = (RelativeLayout) rootView.findViewById(R.id.header_screen);
        header_screen.setVisibility(View.VISIBLE);
        webView = (WebView) rootView.findViewById(R.id.webView);
        Log.i("mytag", "Link : " + Prefs.getPrefInstance().getValue(context, Const.TWIT, ""));
        startWebView(Prefs.getPrefInstance().getValue(context, Const.TWIT, ""));
    }


    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });
    }

    private void startWebView(String url) {
        webView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

        });
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }
        @Override
    public void onDetach() {
        super.onDetach();
        rl_fragments_header.setVisibility(View.VISIBLE);
    }
}
