package com.djcristian.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.djcristian.Activities.HomeActivity;
import com.djcristian.Adapter.AdapterFeaturingDjsSongs;
import com.djcristian.Manager.MediaController;
import com.djcristian.Model.GsonFeaturingDjsSongs2;
import com.djcristian.Model.PojoSongForPlayer;
import com.djcristian.R;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


@SuppressLint("ValidFragment")
public class FragmentArtistOfTheMonth extends Fragment {
    FragmentManager fm;
    ArrayList<GsonFeaturingDjsSongs2> arrayList = new ArrayList<>();
    private Context context;
    private RelativeLayout rl_fragments_header, header_screen;
    private View rootView;
    private RecyclerView rv_dj_mixes;
    private ProgressDialog mProgressDialog;
    private AdapterFeaturingDjsSongs adapter;
    private TextView tv_all_tracks, tv_all_albums, tv_main_title;
    private CircleImageView iv_dj_mixes;
    private DisplayImageOptions options;
    private boolean isOuter = false;
    private boolean isFragmentLoaded = false;
    private TextView tv_title, tv_single_album_name;
    private Button btn_menu, btn_player;


    @SuppressLint("ValidFragment")
    public FragmentArtistOfTheMonth(Context context, RelativeLayout rl_fragments_header, boolean isOuter) {
        this.context = context;
        this.isOuter = isOuter;
        this.rl_fragments_header = rl_fragments_header;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_artist_of_the_month, container, false);
        rl_fragments_header.setVisibility(View.GONE);
        rl_fragments_header.setTag("Inner");
        init();
        listner();
        return rootView;
    }

    private void init() {
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.cristian_placeholder)
                .showImageForEmptyUri(R.drawable.cristian_placeholder)
                .showImageOnFail(R.drawable.cristian_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        rv_dj_mixes = rootView.findViewById(R.id.rv_featured_djs_song);
        rv_dj_mixes.setLayoutManager(new LinearLayoutManager(context));
        header_screen = rootView.findViewById(R.id.header_screen);
        tv_title = rootView.findViewById(R.id.tv_title);
        tv_single_album_name = rootView.findViewById(R.id.tv_single_album_name);
        tv_title.setText("HIT SINGLES");
        tv_single_album_name.setText("HIT SINGLES");
        btn_player = rootView.findViewById(R.id.btn_player);
        btn_menu = rootView.findViewById(R.id.btn_menu);
        header_screen.setVisibility(View.VISIBLE);
        if (CheckNetwork.isInternetAvailable(context)) {
            getAllSongs();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void listner() {
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        btn_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.visiblePlayer();
            }
        });

    }

    @SuppressLint("StaticFieldLeak")
    private void getAllSongs() {
        arrayList = new ArrayList<>();
        final PojoSongForPlayer songDetail = MediaController.getInstance().getPlayingSongDetail();
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("u_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                    response = WebInterface.getInstance().doPostRequest(Const.GET_ARTIST_OF_THE_MONTH_ALL_SONGS, jsonObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("mytag" + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject data1 = data.getJSONObject(i);
                            String song_id = data1.getString("song_id");
                            String like_status = data1.getString("like_status");
                            String song_time = data1.getString("song_time");
                            String totle_like = data1.getString("total_likes");
                            String song_name = data1.getString("song_name");
                            String song_url = data1.getString("song_url");
                            String image = data1.getString("image");
                            String artists_name = data1.getString("artist_name");
                            String nowPlaying;
                            if (songDetail != null) {
                                if (songDetail.getS_url().equals(song_url)) {
                                    nowPlaying = "1";
                                } else {
                                    nowPlaying = "0";
                                }
                            } else {
                                nowPlaying = "0";
                            }
                            arrayList.add(new GsonFeaturingDjsSongs2(song_id, song_name, song_url, image, like_status
                                    , totle_like, song_time, artists_name, nowPlaying));
                        }
                        adapter = new AdapterFeaturingDjsSongs(context, arrayList, getFragmentManager());
                        rv_dj_mixes.setAdapter(adapter);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();


    }

    public void onLoadSong(String url) {
        Utils.getInstance().d("url" + url);
        for (GsonFeaturingDjsSongs2 item : arrayList) {
            if (item.getUrl().equals(url)) {
                item.setNowPlaying("1");
            } else {
                item.setNowPlaying("0");
            }
            AdapterFeaturingDjsSongs adapter = new AdapterFeaturingDjsSongs(context, arrayList, getFragmentManager());
            rv_dj_mixes.setAdapter(adapter);

        }

    }
}
