package com.djcristian.Activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.djcristian.Adapter.AdapterImageSlider2;
import com.djcristian.CustomView.ClickableViewPager;
import com.djcristian.CustomView.OnSwipeTouchListener;
import com.djcristian.DJCristian;
import com.djcristian.Fragments.FragmentArtistOfTheMonth;
import com.djcristian.Fragments.FragmentMore;
import com.djcristian.Fragments.FragmentMyFav;
import com.djcristian.Fragments.FragmentMyPlaylist;
import com.djcristian.Fragments.FragmentMyPlaylistSong;
import com.djcristian.Fragments.FragmentSingleGenre;
import com.djcristian.Fragments.FragmentVideos2;
import com.djcristian.Lib.GifImageView;
import com.djcristian.Manager.MediaController;
import com.djcristian.Manager.MusicPreferance;
import com.djcristian.Manager.NotificationManager;
import com.djcristian.Model.More2;
import com.djcristian.Model.PojoSongForPlayer;
import com.djcristian.Model.PojoVideoMovies2;
import com.djcristian.R;
import com.djcristian.phonemidea.PhoneMediaControl;
import com.djcristian.utility.BufferData;
import com.djcristian.utility.CheckNetwork;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.RestClient;
import com.djcristian.utility.Utils;
import com.djcristian.utility.WebInterface;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener,
        com.djcristian.Manager.NotificationManager.NotificationCenterDelegate, TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {

    private static final String TAG = "ActivityHome";
    private static Button btn_more;
    private static RelativeLayout rl_fragments, rl_subPlayer, rel_controls;
    private static RelativeLayout rl_player, rl_mainPlayer;
    private static ImageView iv_back_home, iv_logo_home;
    private static ClickableViewPager clickableViewpager;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 121;
    private final Object progressTimerSync = new Object();
    private final Object sync = new Object();
    String android_key,android_interstitial;
    AdView adView;
    private TextView tv_current_duration, tv_current_duration1, tv_artist_name, tv_song_name, tv_total_duration;
    private Button btn_repeat, btn_shuffle;
    private ImageView btn_next, btn_previous;
    private ImageView iv_play_pause;
    private Button btn_menu;
    private Button btn_back;
    private Button btn_player, btn_player_sub;
    private Button btn_bottom_player_previous;
    private Button btn_bottom_player_next;
    private Button btn_bottom_player_playlist, btn_repeat1, btn_shuffle1;
    private TextView tv_bottom_player_song_name, tv_bottom_player_song_name1, tv_dj_name, tv_dj_name1, tv_title_1, tv_bottom_player_artist_name;
    private ImageView iv_bottom_play_pause, iv_bottom_play_pause1;
    private RelativeLayout rl_fragments_header, rl_header_main;
    public static RelativeLayout rl_fragments_bottom_player;
    private FragmentManager fm;
    private boolean isFragmentLoaded = false;
    private SeekBar sb_player, sb_player_single, sb_player_single1, sb_volume;
    private AudioManager audioManager = null;
    private int TAG_Observer;
    private int progressValue = 0;
    private Context context = this;
    private PojoSongForPlayer songDetail = null;
    private ProgressDialog mProgressDialog, universalProgressLoader;
    private ArrayList<More2> imageArraylist;
    private Timer progressTimer = null;
    private AdapterImageSlider2 adapter;
    //    private AdView mAdView;
    private GifImageView drawer_gif_image, main_player_gif, main_player_gif1;
    private String token, android_id;
    private TextView tv_bootom_song_total_time;
    private ImageView botm_fav, iv_open_playlist, iv_open_fav, image_bottom_player, image_bottom_player1;
    private ArrayList<PojoVideoMovies2> mdata = new ArrayList<PojoVideoMovies2>();
    private ArrayList<PojoSongForPlayer> sngList = new ArrayList<>();
    private String duration;
    private ImageView iv_single_image;
    private LinearLayout adViewContainer;
    private InterstitialAd mInterstitialAd;
    boolean homevisible;
    String addloadd;

    public static void runOnUIThread(Runnable runnable) {
        runOnUIThread(runnable, 0);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        if (delay == 0) {
            DJCristian.applicationHandler.post(runnable);
        } else {
            DJCristian.applicationHandler.postDelayed(runnable, delay);
        }
    }

    public static void visiblePlayer() {
        rl_player.setVisibility(View.VISIBLE);
        btn_more.setVisibility(View.VISIBLE);
        iv_back_home.setVisibility(View.VISIBLE);
        iv_logo_home.setVisibility(View.VISIBLE);
        rl_subPlayer.setVisibility(View.GONE);
        rl_mainPlayer.setVisibility(View.VISIBLE);
        rl_fragments.setVisibility(View.GONE);

    }

    public static void visiblePlayerTwo() {
        rl_fragments.setVisibility(View.GONE);
        rl_player.setVisibility(View.VISIBLE);
        btn_more.setVisibility(View.GONE);
        iv_back_home.setVisibility(View.GONE);
        iv_logo_home.setVisibility(View.GONE);
        rl_subPlayer.setVisibility(View.VISIBLE);
        rl_mainPlayer.setVisibility(View.GONE);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MobileAds.initialize(getApplicationContext(), getResources().getString(R.string.banner_home_footer));
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setContentView(R.layout.activity_main);


        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    String token = Objects.requireNonNull(task.getResult()).getToken();
                    Log.d("mytag", "Refreshed token: " + token);
                    Prefs.getPrefInstance().setValue(this, Const.GCM_ID, token);
                });


        token = FirebaseInstanceId.getInstance().getToken();
        FirebaseMessaging.getInstance().subscribeToTopic("djcristian");
        Log.d("mytag", "Refreshed token: " + token);
        android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Prefs.getPrefInstance().setValue(this, Const.USER_ID, android_id);
        Prefs.getPrefInstance().setValue(this, Const.GCM_ID, token);
        googleAdview();
        init();
        getIntentData();
        listner();
        registerUser();
        sliderClick();
        InputStream is = null;
        byte[] bytes = new byte[0];
        try {
            is = getAssets().open("gif.gif");
            bytes = new byte[is.available()];
            is.read(bytes);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        drawer_gif_image = findViewById(R.id.drawer_gif_image);
        main_player_gif = findViewById(R.id.main_player_gif);
        main_player_gif1 = findViewById(R.id.main_player_gif1);
        drawer_gif_image.setBytes(bytes);
        main_player_gif.setBytes(bytes);
        main_player_gif1.setBytes(bytes);
        main_player_gif.stopAnimation();
        main_player_gif1.stopAnimation();
        drawer_gif_image.stopAnimation();
    }

    private void init() {
        if (Build.VERSION.SDK_INT >= 23) {
            CheckPermission();
        }
        universalProgressLoader = new ProgressDialog(context);
        universalProgressLoader.setMessage("Loading");
        universalProgressLoader.setCanceledOnTouchOutside(false);
        universalProgressLoader.setIndeterminate(true);
        universalProgressLoader.setCancelable(false);
        adViewContainer = findViewById(R.id.adViewContainer);


        clickableViewpager = findViewById(R.id.clickableViewpager);
        btn_more = findViewById(R.id.btn_more);
        iv_logo_home = findViewById(R.id.iv_logo_home);
        iv_back_home = findViewById(R.id.iv_back_home);
        rl_player = findViewById(R.id.rl_player);
        rl_fragments = findViewById(R.id.rl_fragments);
        rl_subPlayer = findViewById(R.id.rl_subPlayer);
        rl_fragments_header = findViewById(R.id.rl_fragments_header);
        rl_header_main = findViewById(R.id.rl_header_main);
        rl_fragments_bottom_player = findViewById(R.id.rl_fragments_bottom_player);
        rel_controls = findViewById(R.id.rel_controls);
        rl_mainPlayer = findViewById(R.id.rl_mainPlayer);
        rl_player.setVisibility(View.VISIBLE);
        btn_more.setVisibility(View.VISIBLE);
        iv_back_home.setVisibility(View.VISIBLE);
        iv_logo_home.setVisibility(View.VISIBLE);
        rl_subPlayer.setVisibility(View.GONE);
        rl_mainPlayer.setVisibility(View.VISIBLE);
        rl_fragments.setVisibility(View.GONE);
        sb_player = findViewById(R.id.sb_player);
        sb_player_single = findViewById(R.id.sb_player_single);
        sb_player_single1 = findViewById(R.id.sb_player_single1);
        BufferData.getInstance().setSb_progress(sb_player);
        BufferData.getInstance().setSb_progress_single(sb_player_single);
        BufferData.getInstance().setSb_progress_single(sb_player_single1);
        BufferData.getInstance().setUniversalProgressLoader(universalProgressLoader);
        tv_current_duration = findViewById(R.id.tv_current_duration);
        tv_current_duration1 = findViewById(R.id.tv_current_duration1);
        tv_song_name = findViewById(R.id.tv_song_name);
        tv_title_1 = findViewById(R.id.tv_title_1);
        tv_artist_name = findViewById(R.id.tv_artist_name);
        tv_song_name.setSelected(true);
        tv_title_1.setSelected(true);
        tv_total_duration = findViewById(R.id.tv_total_duration);
        tv_bootom_song_total_time = findViewById(R.id.tv_bootom_song_total_time);
        iv_play_pause = findViewById(R.id.iv_play_pause);
        btn_repeat = findViewById(R.id.btn_repeat);
        btn_repeat1 = findViewById(R.id.btn_repeat1);
        btn_previous = findViewById(R.id.btn_previous);
        btn_bottom_player_previous = findViewById(R.id.btn_bottom_player_previous);
        btn_next = findViewById(R.id.btn_next);
        btn_bottom_player_next = findViewById(R.id.btn_bottom_player_next);
        btn_bottom_player_playlist = findViewById(R.id.btn_bottom_player_playlist);
        btn_shuffle = findViewById(R.id.btn_shuffle);
        btn_shuffle1 = findViewById(R.id.btn_shuffle1);
        botm_fav = findViewById(R.id.botm_fav);
        image_bottom_player = findViewById(R.id.image_bottom_player);
        image_bottom_player1 = findViewById(R.id.image_bottom_player1);
        iv_open_playlist = findViewById(R.id.iv_open_playlist);
        iv_open_fav = findViewById(R.id.iv_open_fav);
        iv_single_image = findViewById(R.id.iv_single_image);
        tv_bottom_player_song_name = findViewById(R.id.tv_bottom_player_song_name);
        tv_bottom_player_song_name1 = findViewById(R.id.tv_bottom_player_song_name1);
        tv_dj_name = findViewById(R.id.tv_dj_name);
        tv_dj_name1 = findViewById(R.id.tv_dj_name1);
        iv_bottom_play_pause = findViewById(R.id.iv_bottom_play_pause);
        iv_bottom_play_pause1 = findViewById(R.id.iv_bottom_play_pause1);
        btn_menu = findViewById(R.id.btn_menu);
        btn_back = findViewById(R.id.btn_back);
        btn_player = findViewById(R.id.btn_player);
        btn_player_sub = findViewById(R.id.btn_player_sub);
        btn_bottom_player_next = findViewById(R.id.btn_bottom_player_next);

        tv_bottom_player_song_name1.setSelected(true);
        tv_bottom_player_song_name.setSelected(true);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        String isRepeat = Prefs.getPrefInstance().getValue(this, Const.REPEAT, "");
        String isShuffel = Prefs.getPrefInstance().getValue(this, Const.SHUFFEL, "");
        if (isRepeat.equals("1")) {
            btn_repeat.setSelected(true);
            btn_repeat1.setSelected(true);
        } else {
            btn_repeat.setSelected(false);
            btn_repeat1.setSelected(false);
        }
        if (isShuffel.equals("1")) {
            btn_shuffle.setSelected(true);
            btn_shuffle1.setSelected(true);
        } else {
            btn_shuffle.setSelected(false);
            btn_shuffle1.setSelected(false);
        }
        getdata();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void listner() {
        iv_play_pause.setOnClickListener(this);
        btn_back.setOnClickListener(this);
        iv_play_pause.setSelected(false);
        btn_repeat.setOnClickListener(this);
        btn_repeat1.setOnClickListener(this);
        btn_previous.setOnClickListener(this);
        btn_bottom_player_previous.setOnClickListener(this);
        btn_next.setOnClickListener(this);
        btn_bottom_player_next.setOnClickListener(this);
        btn_bottom_player_playlist.setOnClickListener(this);
        btn_shuffle.setOnClickListener(this);
        btn_shuffle1.setOnClickListener(this);
        btn_more.setOnClickListener(this);
        iv_bottom_play_pause.setOnClickListener(this);
        iv_bottom_play_pause1.setOnClickListener(this);
        iv_bottom_play_pause.setSelected(false);
        iv_bottom_play_pause1.setSelected(false);
        btn_menu.setOnClickListener(this);
        btn_player.setOnClickListener(this);
        btn_player_sub.setOnClickListener(this);
        rl_fragments_bottom_player.setOnClickListener(this);
        botm_fav.setOnClickListener(this);
        iv_open_playlist.setOnClickListener(this);
        iv_open_fav.setOnClickListener(this);

        rl_header_main.setOnTouchListener(new OnSwipeTouchListener(this) {
            public void onSwipeBottom() {
                if (isFragmentLoaded == true) {
                    rl_fragments.setVisibility(View.VISIBLE);
                    ViewAnimator
                            .animate(rl_fragments)
                            .duration(200)
                            .translationY(-2000, 0)
                            .andAnimate(rl_player)
                            .duration(200)
                            .translationY(0, 2000)
                            .onStart(new AnimationListener.Start() {
                                @Override
                                public void onStart() {
                                    stopProgressTimer();
                                    drawer_gif_image.stopAnimation();
                                    main_player_gif.stopAnimation();
//                                    gif_image_single.stopAnimation();
                                    rl_fragments.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                                    rl_player.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                                }
                            })
                            .onStop(new AnimationListener.Stop() {
                                @Override
                                public void onStop() {

                                    if (!MediaController.getInstance().isAudioPaused()) {
                                        drawer_gif_image.startAnimation();
                                        main_player_gif.startAnimation();
                                    }
                                    rl_fragments.setLayerType(View.LAYER_TYPE_NONE, null);
                                    rl_player.setLayerType(View.LAYER_TYPE_NONE, null);
                                    rl_player.setVisibility(View.GONE);
                                    ViewAnimator.animate(rl_player).duration(500).translationY(-2000, 0).start();
                                }
                            })
                            .start();
                }
            }
        });

        rl_fragments_bottom_player.setOnTouchListener(new OnSwipeTouchListener(this) {
            public void onSwipeTop() {
                rl_player.setVisibility(View.VISIBLE);
                ViewAnimator
                        .animate(rl_player)
                        .duration(200)
                        .translationY(2000, 0)
                        .andAnimate(rl_fragments)
                        .duration(200)
                        .translationY(0, -2000)
                        .onStart(new AnimationListener.Start() {
                            @Override
                            public void onStart() {
//                                startImageSliderTimer();
                                main_player_gif.stopAnimation();
                                main_player_gif1.stopAnimation();
                                drawer_gif_image.stopAnimation();
//                                gif_image_single.stopAnimation();
                                rl_fragments.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                                rl_player.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                            }
                        })
                        .onStop(new AnimationListener.Stop() {
                            @Override
                            public void onStop() {
                                if (!MediaController.getInstance().isAudioPaused()) {
                                    main_player_gif.startAnimation();
                                    main_player_gif1.startAnimation();
                                    drawer_gif_image.startAnimation();
//                                    gif_image_single.startAnimation();
                                }
                                rl_fragments.setLayerType(View.LAYER_TYPE_NONE, null);
                                rl_player.setLayerType(View.LAYER_TYPE_NONE, null);
                                rl_fragments.setVisibility(View.GONE);
                                rl_mainPlayer.setVisibility(View.VISIBLE);
                                ViewAnimator.animate(rl_fragments).duration(500).translationY(2000, 0).start();
                            }
                        })
                        .start();

            }


        });

        sb_player.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int prog = seekBar.getProgress();
                MediaController.getInstance().seekToProgress(MediaController.getInstance().getPlayingSongDetail(), (float) prog / 100);
            }
        });

        sb_player_single.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int prog = seekBar.getProgress();
                MediaController.getInstance().seekToProgress(MediaController.getInstance().getPlayingSongDetail(), (float) prog / 100);
            }
        });

        sb_player_single1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int prog = seekBar.getProgress();
                MediaController.getInstance().seekToProgress(MediaController.getInstance().getPlayingSongDetail(), (float) prog / 100);
            }
        });


        mInterstitialAd = new InterstitialAd(this);
        String android_interstitial = Prefs.getPrefInstance().getValue(context, Const.INTERESTITIAL_KEY, "");
        Log.d("mytag","interstial add key isss-----------"+android_interstitial);
//        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.setAdUnitId(android_interstitial);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                displayInterstitial();
                addloadd = "true";
                Log.d("mytag", "add load");
            }

            public void onAdFailedToLoad() {
                Log.d("mytag", "add load fail");
            }
        });


        // Create a banner ad
        adView = new AdView(HomeActivity.this);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(Prefs.getPrefInstance().getValue(context, Const.Google_add_key, ""));
//        adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adViewContainer.addView(adView);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.d("mytag", "Ad is loaded");
            }

            @Override
            public void onAdClosed() {
                Log.d("mytag", "Ad is closed");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d("mytag", "Ad is Failed to load");
            }

            @Override
            public void onAdLeftApplication() {
            }

            @Override
            public void onAdOpened() {
                Log.d("mytag", "Ad is opened");
            }
        });
    }

    private void displayInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
        if (homevisible) {
            new android.app.AlertDialog.Builder(HomeActivity.this)
                    .setCancelable(true)
                    .setTitle(R.string.app_name)
                    .setMessage("Are you sure you want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }
    }

    @Override
    public void onBackPressed() {
        if (rl_subPlayer.getVisibility() == View.VISIBLE) {

            homevisible = true;
            displayInterstitial();

            rl_player.setVisibility(View.GONE);
            btn_more.setVisibility(View.VISIBLE);
            iv_back_home.setVisibility(View.VISIBLE);
            iv_logo_home.setVisibility(View.VISIBLE);
            rl_subPlayer.setVisibility(View.GONE);
            rl_mainPlayer.setVisibility(View.GONE);
            rl_fragments.setVisibility(View.VISIBLE);
            return;
        }
        if (rl_player.getVisibility() == View.VISIBLE) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("DJ Cristian");
            alertDialog.setMessage("Are you sure want to exit?");
            alertDialog.setPositiveButton("YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    });
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();


        } else {
            if (rl_fragments_header.getTag().equals("Outer")) {
                rl_player.setVisibility(View.VISIBLE);
                rl_mainPlayer.setVisibility(View.VISIBLE);
                rl_fragments.setVisibility(View.GONE);
            } else if (rl_fragments_header.getTag().equals("Inner")) {
                super.onBackPressed();
            }
        }

    }

    @Override
    protected void onDestroy() {
        removeObserver();
        if (MediaController.getInstance().isAudioPaused()) {
            MediaController.getInstance().cleanupPlayer(context, true, true);
            MusicPreferance.playingSongDetail = null;
            MusicPreferance.saveLastSong(context, null);
            MusicPreferance.playlist.clear();
            MusicPreferance.shuffledPlaylist.clear();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        removeObserver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        addloadd = "false";
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (addloadd == "false") {
            mInterstitialAd = new InterstitialAd(this);
            String android_interstitial = Prefs.getPrefInstance().getValue(context, Const.INTERESTITIAL_KEY, "");
//        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
            mInterstitialAd.setAdUnitId(android_interstitial);
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener() {
                public void onAdLoaded() {
                    displayInterstitial();
                    addloadd = "true";
                }

                public void onAdFailedToLoad() {
                    Log.d("mytag", "add load fail");
                }
            });
        }

        loadAlreadyPlayng();
        addObserver();

    }



    private void sliderClick() {
        Log.d("urtag", "my slider click is called");
        sngList = new ArrayList<>();
        imageArraylist = new ArrayList<>();
        mdata = new ArrayList<>();
        clickableViewpager.setOnItemClickListener(new ClickableViewPager.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Utils.getInstance().d("click");
                int pos = clickableViewpager.getCurrentItem();
                Log.d("urtag", "my slider position iss--" + pos);
                Utils.getInstance().d("Image Clicked Position : " + pos);
                switch (pos) {
                    case 0:
                        PojoVideoMovies2 item = mdata.get(0);
                        Utils.getInstance().d("position video 1 : " + item.getName());
                        pushFragment(new FragmentVideos2(context, rl_fragments_header, item.getName(), item.getData_link(),
                                item.getBanner_image(), item.getDescription(), true, getSupportFragmentManager()), "Single Video", false);
                        rl_fragments.setVisibility(View.VISIBLE);
                        rl_player.setVisibility(View.GONE);
                        break;
                    case 1:
                        final PojoSongForPlayer Detail = sngList.get(0);
                        Utils.getInstance().d("position audio 1 : " + Detail.getS_name());
                        if (Detail != null) {
                            final ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                            if (progressDialog != null) {
                                Utils.getInstance().d("not null");
                                if (!progressDialog.isShowing()) {
                                    Utils.getInstance().d("not showing");
                                    if (CheckNetwork.isInternetAvailable(context)) {
                                        progressDialog.show();
                                        progressDialog.setCanceledOnTouchOutside(true);
                                    } else {
                                        progressDialog.dismiss();
                                        Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                                    }
                                    if (MediaController.getInstance().isPlayingAudio(Detail) && !MediaController.getInstance().isAudioPaused()) {
                                        MediaController.getInstance().pauseAudio(Detail);

                                    } else {
                                        MediaController.getInstance().setPlaylist(sngList, Detail, PhoneMediaControl.SonLoadFor.All.ordinal(), -1);
                                    }
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (progressDialog != null) {
                                                if (progressDialog.isShowing()) {
                                                    new Handler().postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            String duration1 = duration;
                                                            String strTime = duration1.substring(duration1.length() - 2);
                                                            int sec1 = Detail.audioProgressSec % 60;
                                                            int sec = Integer.parseInt(strTime);
                                                            if (sec > 1 && sec1 > 1) {
                                                                progressDialog.dismiss();
                                                            }
                                                        }
                                                    }, 200);
                                                }
                                            }
                                        }
                                    }, 200);
                                }
                            }
                        }
                        break;
                    case 2:
                        PojoVideoMovies2 item2 = mdata.get(1);
                        Utils.getInstance().d("position video 2 : " + item2.getName());
                        pushFragment(new FragmentVideos2(context, rl_fragments_header, item2.getName(), item2.getData_link(),
                                item2.getBanner_image(), item2.getDescription(), true, getSupportFragmentManager()), "Single Video", false);
                        rl_fragments.setVisibility(View.VISIBLE);
                        rl_player.setVisibility(View.GONE);
                        break;
                    case 3:
                        final PojoSongForPlayer mDetail1 = sngList.get(1);
                        Utils.getInstance().d("mdetails audio 2 : " + mDetail1.getS_name());
                        if (mDetail1 != null) {
                            final ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                            if (progressDialog != null) {
                                Utils.getInstance().d("not null");
                                if (!progressDialog.isShowing()) {
                                    Utils.getInstance().d("not showing");
                                    if (CheckNetwork.isInternetAvailable(context)) {
                                        progressDialog.show();
                                        progressDialog.setCanceledOnTouchOutside(true);
                                    } else {
                                        progressDialog.dismiss();
                                        Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                                    }
                                    if (MediaController.getInstance().isPlayingAudio(mDetail1) && !MediaController.getInstance().isAudioPaused()) {
                                        MediaController.getInstance().pauseAudio(mDetail1);

                                    } else {
                                        MediaController.getInstance().setPlaylist(sngList, mDetail1, PhoneMediaControl.SonLoadFor.All.ordinal(), -1);
                                    }
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (progressDialog != null) {
                                                if (progressDialog.isShowing()) {
                                                    new Handler().postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            String duration1 = duration;
                                                            String strTime = duration1.substring(duration1.length() - 2);
                                                            int sec1 = mDetail1.audioProgressSec % 60;
                                                            int sec = Integer.parseInt(strTime);
                                                            if (sec > 1 && sec1 > 1) {
                                                                progressDialog.dismiss();
                                                            }
                                                        }
                                                    }, 200);
                                                }
                                            }
                                        }
                                    }, 200);
                                }
                            }
                        }
                        break;
                    case 4:
                        PojoVideoMovies2 item4 = mdata.get(2);
                        Utils.getInstance().d("position video 3 : " + item4.getName());
                        pushFragment(new FragmentVideos2(context, rl_fragments_header, item4.getName(), item4.getData_link(),
                                item4.getBanner_image(), item4.getDescription(), true, getSupportFragmentManager()), "Single Video", false);
                        rl_fragments.setVisibility(View.VISIBLE);
                        rl_player.setVisibility(View.GONE);
                        break;
                    default:
                        break;
                }
            }
        });

    }

    @SuppressLint("StaticFieldLeak")
    private void getdata() {
        imageArraylist = new ArrayList<>();
        mdata = new ArrayList<>();
        sngList = new ArrayList<>();
        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    response = WebInterface.getInstance().doGet(Const.PAGES_VIDEO);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject slider_image_data = data.optJSONObject(i);
                            String image = slider_image_data.getString("image");
                            imageArraylist.add(new More2(image, ""));

                        }
                        adapter = new AdapterImageSlider2(imageArraylist, context);
                        clickableViewpager.setAdapter(adapter);
                        clickableViewpager.setPagingEnabled(false);
                        startImageSliderTimer();


                        JSONArray jsonArray2 = jsonObject.getJSONArray("video");
                        for (int j = 0; j < jsonArray2.length(); j++) {
                            JSONObject video_data = jsonArray2.getJSONObject(j);
                            String name = video_data.getString("name");
                            String description = video_data.getString("description");
                            String image = video_data.getString("image");
                            String banner_image = video_data.getString("banner_image");
                            String video_link = video_data.getString("video_link");
                            mdata.add(new PojoVideoMovies2(name, video_link, image, banner_image, description));
                        }

                        JSONArray jsonArray3 = jsonObject.getJSONArray("audio");
                        for (int k = 0; k < jsonArray3.length(); k++) {
                            JSONObject audio_data = jsonArray3.getJSONObject(k);
                            String song_id = audio_data.getString("song_id");
                            String song_name = audio_data.getString("song_name");
                            String song_url = audio_data.getString("song_url");
                            String image = audio_data.getString("image");
                            String artists_name = audio_data.getString("artist_name");
                            duration = audio_data.getString("song_time");
                            String durInMili = String.valueOf(Utils.getInstance().strToMilli(duration));
                            sngList.add(new PojoSongForPlayer(song_name, durInMili, song_url, image, artists_name, song_id));
                        }
                    }
                } catch (Exception e) {
                }
            }
        }.execute();

    }

    private void startImageSliderTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {
                }
            }
            progressTimer = new Timer();
            progressTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (sync) {
                        runOnUIThread(new Runnable() {
                            @Override
                            public void run() {
                                int currentItem = clickableViewpager.getCurrentItem() + 1;
                                int totalItem = adapter.getCount();
                                if (currentItem == totalItem) {
                                    clickableViewpager.setCurrentItem(0);
                                } else {
                                    clickableViewpager.setCurrentItem(currentItem);
                                }
                            }
                        });
                    }
                }
            }, 0, 3000);
        }

    }
    //permission on marshmallow //

    @TargetApi(Build.VERSION_CODES.M)
    private void CheckPermission() {
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, android.Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("Phone");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @TargetApi(Build.VERSION_CODES.M)
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                return shouldShowRequestPermissionRationale(permission);
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                perms.put(android.Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                if (perms.get(android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
//                    loadLocation();
                } else {
                    Toast.makeText(context, "Some Permission is Denied", Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }
    ///


    private void stopProgressTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {
                    Log.e("tmessages", e.toString());
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_fragments_bottom_player:
                break;
            case R.id.iv_play_pause:
                if (CheckNetwork.isInternetAvailable(context)) {
                    if (MediaController.getInstance().getPlayingSongDetail() != null) {
                        PlayPauseEvent(view);
                    } else {
                        getAllSongs();
                    }
                }else{
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_back:
                if (CheckNetwork.isInternetAvailable(context)) {
                    onBackPressed();
                }else{
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_repeat:
                if (btn_repeat.isSelected()) {
                    btn_repeat.setSelected(!btn_repeat.isSelected());
                    btn_repeat1.setSelected(!btn_repeat1.isSelected());
                    Prefs.getPrefInstance().setValue(this, Const.REPEAT, "0");

                } else {
                    btn_repeat.setSelected(!btn_repeat.isSelected());
                    btn_repeat1.setSelected(!btn_repeat1.isSelected());
                    Prefs.getPrefInstance().setValue(this, Const.REPEAT, "1");
                }
                break;
            case R.id.btn_repeat1:
                if (btn_repeat1.isSelected()) {
                    btn_repeat1.setSelected(!btn_repeat1.isSelected());
                    btn_repeat.setSelected(!btn_repeat.isSelected());
                    Prefs.getPrefInstance().setValue(this, Const.REPEAT, "0");

                } else {
                    btn_repeat1.setSelected(!btn_repeat1.isSelected());
                    btn_repeat.setSelected(!btn_repeat.isSelected());
                    Prefs.getPrefInstance().setValue(this, Const.REPEAT, "1");
                }
                break;
            case R.id.btn_previous:
                if (CheckNetwork.isInternetAvailable(context)) {
                    if (MediaController.getInstance().getPlayingSongDetail() != null) {
                        ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                        if (progressDialog != null) {
                            Utils.getInstance().d("not null");
                            if (!progressDialog.isShowing()) {
                                Utils.getInstance().d("not showing");
//                            mProgressDialog.show();
                                progressDialog.show();
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        MediaController.getInstance().playPreviousSong();
                                        loadSongsDetails(MediaController.getInstance().getPlayingSongDetail());
                                    }
                                }, 1000);

                            }

                        }
                    }
                }else{
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_next:
                if (CheckNetwork.isInternetAvailable(context)) {
                    if (MediaController.getInstance().getPlayingSongDetail() != null) {
                        ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                        if (progressDialog != null) {
                            Utils.getInstance().d("not null");
                            if (!progressDialog.isShowing()) {
                                Utils.getInstance().d("not showing");
//                            mProgressDialog.show();
                                progressDialog.show();
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        MediaController.getInstance().playNextSong();
                                        loadSongsDetails(MediaController.getInstance().getPlayingSongDetail());
                                    }
                                }, 1000);
                            }
                        }
                    }
                }else{
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_shuffle:
                if (btn_shuffle.isSelected()) {
                    btn_shuffle.setSelected(!btn_shuffle.isSelected());
                    btn_shuffle1.setSelected(!btn_shuffle1.isSelected());
                    Prefs.getPrefInstance().setValue(this, Const.SHUFFEL, "0");
                } else {
                    btn_shuffle.setSelected(!btn_shuffle.isSelected());
                    btn_shuffle1.setSelected(!btn_shuffle1.isSelected());
                    Prefs.getPrefInstance().setValue(this, Const.SHUFFEL, "1");
                }
                break;
            case R.id.btn_shuffle1:
                if (btn_shuffle1.isSelected()) {
                    btn_shuffle1.setSelected(!btn_shuffle1.isSelected());
                    btn_shuffle.setSelected(!btn_shuffle.isSelected());
                    Prefs.getPrefInstance().setValue(this, Const.SHUFFEL, "0");
                } else {
                    btn_shuffle1.setSelected(!btn_shuffle1.isSelected());
                    btn_shuffle.setSelected(!btn_shuffle.isSelected());
                    Prefs.getPrefInstance().setValue(this, Const.SHUFFEL, "1");
                }
                break;
//            case R.id.btn_artist:
//                pushFragment(new FragmentBooking(this, rl_fragments_header, true), "STORE", false);
//                rl_player.setVisibility(View.GONE);
//                rl_fragments.setVisibility(View.VISIBLE);
//                break;
//            case R.id.btn_playlist:
//                pushFragment(new FragmentFeaturedDjs(this, rl_fragments_header, true), "playlist", false);
//                rl_player.setVisibility(View.GONE);
//                rl_fragments.setVisibility(View.VISIBLE);
//                break;
            case R.id.iv_open_playlist:
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushFragment(new FragmentMyPlaylist(this, rl_fragments_header, true), "playlist", false);
                    rl_player.setVisibility(View.GONE);
                    rl_fragments.setVisibility(View.VISIBLE);
                }else{
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
//            case R.id.btn_events:
//                pushFragment(new FragmentEvents(this, rl_fragments_header, true), "playlist", false);
//                rl_player.setVisibility(View.GONE);
//                rl_fragments.setVisibility(View.VISIBLE);
//                break;
            case R.id.iv_open_fav:
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushFragment(new FragmentMyFav(this, rl_fragments_header, true), "playlist", false);
                    rl_player.setVisibility(View.GONE);
                    rl_fragments.setVisibility(View.VISIBLE);
                }else{
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
//            case R.id.btn_djs:
//                pushFragment(new FragmentFeaturedArtist(this, rl_fragments_header, true, getSupportFragmentManager()), "artist", false);
//                rl_player.setVisibility(View.GONE);
//                rl_fragments.setVisibility(View.VISIBLE);
//                break;
//            case R.id.btn_mixes:
//                pushFragment(new FragmentMusicGeners(this, rl_fragments_header, true, true), "artist", false);
//                rl_player.setVisibility(View.GONE);
//                rl_fragments.setVisibility(View.VISIBLE);
//                break;
            case R.id.btn_more:
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushFragment(new FragmentMore(this, rl_fragments_header, adView), "more", false);
                    rl_player.setVisibility(View.GONE);
                    rl_fragments.setVisibility(View.VISIBLE);
                }else{
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.botm_fav:
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushFragment(new FragmentMyFav(context, rl_fragments_header, true), "myFav", true);
                    rl_player.setVisibility(View.GONE);
                    rl_fragments.setVisibility(View.VISIBLE);
                }else{
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
//            case R.id.botm_plylist:
//                pushFragment(new FragmentMyPlaylist(context, rl_fragments_header, true), "myPlaylist", true);
//                rl_player.setVisibility(View.GONE);
//                rl_fragments.setVisibility(View.VISIBLE);
//                break;
            case R.id.iv_bottom_play_pause:
                if (CheckNetwork.isInternetAvailable(context)) {
                    if (MediaController.getInstance().getPlayingSongDetail() != null) {
                        PlayPauseEvent(view);
                    } else {
                        getAllSongs();
                    }
                }else{
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.iv_bottom_play_pause1:
                if (CheckNetwork.isInternetAvailable(context)) {
                    if (MediaController.getInstance().getPlayingSongDetail() != null) {
                        PlayPauseEvent(view);
                    } else {
                        getAllSongs();
                    }
                }else{
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_menu:
                break;
            case R.id.btn_player:
                rl_player.setVisibility(View.VISIBLE);
                rl_subPlayer.setVisibility(View.GONE);
                rl_mainPlayer.setVisibility(View.VISIBLE);
                btn_more.setVisibility(View.VISIBLE);
                iv_back_home.setVisibility(View.VISIBLE);
                iv_logo_home.setVisibility(View.VISIBLE);
                rl_fragments.setVisibility(View.GONE);
                break;

            case R.id.btn_player_sub:
                rl_player.setVisibility(View.VISIBLE);
                rl_subPlayer.setVisibility(View.GONE);
                rl_mainPlayer.setVisibility(View.VISIBLE);
                btn_more.setVisibility(View.VISIBLE);
                iv_back_home.setVisibility(View.VISIBLE);
                iv_logo_home.setVisibility(View.VISIBLE);
                rl_fragments.setVisibility(View.GONE);
                break;

            case R.id.btn_bottom_player_previous:
                if (CheckNetwork.isInternetAvailable(context)) {
                    if (MediaController.getInstance().getPlayingSongDetail() != null) {
                        ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                        if (progressDialog != null) {
                            Utils.getInstance().d("not null");
                            if (!progressDialog.isShowing()) {
                                Utils.getInstance().d("not showing");
                                progressDialog.show();
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        MediaController.getInstance().playPreviousSong();
                                        loadSongsDetails(MediaController.getInstance().getPlayingSongDetail());
                                    }
                                }, 1000);

                            }
                        }
                    }
                }else{
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_bottom_player_next:
                if (CheckNetwork.isInternetAvailable(context)) {
                    if (MediaController.getInstance().getPlayingSongDetail() != null) {
                        ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                        if (progressDialog != null) {
                            Utils.getInstance().d("not null");
                            if (!progressDialog.isShowing()) {
                                Utils.getInstance().d("not showing");
                                progressDialog.show();
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        MediaController.getInstance().playNextSong();
                                        loadSongsDetails(MediaController.getInstance().getPlayingSongDetail());
                                    }
                                }, 1000);
                            }
                        }
                    }
                }else{
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_bottom_player_playlist:
                if (CheckNetwork.isInternetAvailable(context)) {
                    pushFragment(new FragmentMyPlaylist(context, rl_fragments_header, true), "Playlist", false);
                }else{
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void PlayPauseEvent(View v) {
        if (MediaController.getInstance().isAudioPaused()) {
            MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
            iv_play_pause.setSelected(true);
            iv_bottom_play_pause.setSelected(true);
            iv_bottom_play_pause1.setSelected(true);
            main_player_gif.startAnimation();
            main_player_gif1.startAnimation();
            drawer_gif_image.startAnimation();
        } else {
            MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
            iv_play_pause.setSelected(false);
            iv_bottom_play_pause.setSelected(false);
            iv_bottom_play_pause1.setSelected(false);
            if (main_player_gif.isAnimating() && drawer_gif_image.isAnimating()) {
                main_player_gif.stopAnimation();
                main_player_gif1.stopAnimation();
                drawer_gif_image.stopAnimation();
            } else {
                main_player_gif.startAnimation();
                main_player_gif1.startAnimation();
                drawer_gif_image.startAnimation();
            }

        }
    }

    public void addObserver() {
        TAG_Observer = MediaController.getInstance().generateObserverTag();
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioDidReset);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioPlayStateChanged);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioDidStarted);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioProgressDidChanged);
        NotificationManager.getInstance().addObserver(this, NotificationManager.newaudioloaded);
    }

    public void removeObserver() {
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioDidReset);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioPlayStateChanged);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioDidStarted);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioProgressDidChanged);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.newaudioloaded);
    }

    public void loadSongsDetails(PojoSongForPlayer mDetail) {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.cristian_placeholder)
                .showImageForEmptyUri(R.drawable.cristian_placeholder)
                .showImageOnFail(R.drawable.cristian_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        tv_song_name.setText(mDetail.getS_name());
        tv_title_1.setText(mDetail.getS_name());
//        tv_artist_name.setText(mDetail.getArtist());
        tv_bottom_player_song_name.setText(mDetail.getS_name());
        Log.d("mytag", "player 2 song name" + mDetail.getS_name());
        tv_bottom_player_song_name1.setText(mDetail.getS_name());
        tv_dj_name.setText(mDetail.getArtist());
        Log.d("mytag", "player 2 artist name" + mDetail.getArtist());
        tv_dj_name1.setText(mDetail.getArtist());
        tv_dj_name.setSelected(true);
        tv_dj_name1.setSelected(true);

//        tv_bottom_player_artist_name.setText(mDetail.getArtist());
        ImageLoader.getInstance().displayImage(mDetail.getS_img(), iv_single_image, options);
        ImageLoader.getInstance().displayImage(mDetail.getS_img(), image_bottom_player, options);
        ImageLoader.getInstance().displayImage(mDetail.getS_img(), image_bottom_player1, options);
        if (tv_total_duration != null) {
            long duration = Long.valueOf(mDetail.getS_duration());
            if (duration != 0) {
                tv_total_duration.setText(Utils.getInstance().milliToString(duration));
                tv_bootom_song_total_time.setText(Utils.getInstance().milliToString(duration));
            } else {
                tv_total_duration.setText("00:00:00");
                tv_bootom_song_total_time.setText("00:00:00");
            }
        }
        updateProgress(mDetail);
    }

    private void updateProgress(final PojoSongForPlayer mSongDetail) {
        if (sb_player != null || sb_player_single != null || sb_player_single1 != null) {
            // When SeekBar Draging Don't Show Progress
//            if (!isDragingStart) {
            // Progress Value comming in point it range 0 to 1
            progressValue = (int) (mSongDetail.audioProgress * 100);
            sb_player.setProgress(progressValue);
            sb_player_single.setProgress(progressValue);
            sb_player_single1.setProgress(progressValue);
//            }
            String timeString = String.format("%02d:%02d:%02d", mSongDetail.audioProgressSec / 3600, (mSongDetail.audioProgressSec % 3600) / 60, mSongDetail.audioProgressSec % 60);
            tv_current_duration.setText(timeString);
            tv_current_duration1.setText(timeString);
            final ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            String duration = tv_current_duration.getText().toString();
                            String strTime = duration.substring(duration.length() - 2);
                            int sec1 = mSongDetail.audioProgressSec % 60;
                            int sec = Integer.parseInt(strTime);
                            if (sec > 1 && sec1 > 1) {
                                progressDialog.dismiss();
                            }
                        }
                    }, 200);
                }
            }
        }
    }

    private void loadAlreadyPlayng() {
        addObserver();
        boolean isPaused = MediaController.getInstance().isAudioPaused();
        PojoSongForPlayer mSongDetail = MusicPreferance.getLastSong(context);
        if (mSongDetail != null && !isPaused) {
            songDetail = mSongDetail;
            updateTitle(false);
            loadSongsDetails(mSongDetail);
        }
    }

    private void updateTitle(boolean shutdown) {
        PojoSongForPlayer mSongDetail = MediaController.getInstance().getPlayingSongDetail();
        if (mSongDetail == null && shutdown) {
            return;
        } else {
            updateProgress(mSongDetail);
            if (MediaController.getInstance().isAudioPaused()) {
                iv_play_pause.setSelected(false);
                iv_bottom_play_pause.setSelected(false);
                iv_bottom_play_pause1.setSelected(false);
//                iv_bottom_play_pause_single.setSelected(false);
            } else {
                iv_play_pause.setSelected(true);
                iv_bottom_play_pause.setSelected(true);
                iv_bottom_play_pause1.setSelected(true);
//                iv_bottom_play_pause_single.setSelected(true);
            }
            PojoSongForPlayer audioInfo = MediaController.getInstance().getPlayingSongDetail();
            loadSongsDetails(audioInfo);
            if (tv_total_duration != null) {
                long duration = Long.valueOf(audioInfo.getS_duration());
                if (duration != 0) {
                    tv_total_duration.setText(Utils.getInstance().milliToString(duration));
                    tv_bootom_song_total_time.setText(Utils.getInstance().milliToString(duration));
//                    tv_bootom_song_total_time_new.setText(Utils.getInstance().milliToString(duration));
                } else {
                    tv_total_duration.setText("00:00:00");
                    tv_bootom_song_total_time.setText("00:00:00");
//                    tv_bootom_song_total_time_new.setText("00:00:00");
                }
//                txt_timetotal.setText(duration != 0 ? String.format("%d:%02d", duration / 60, duration % 60) : "-:--");
            }
        }
    }

    private void getIntentData() {
        try {
            Uri data = getIntent().getData();
            if (data != null) {
                if (data.getScheme().equalsIgnoreCase("file")) {
                    String path = data.getPath();
                    if (!TextUtils.isEmpty(path)) {
                        MediaController.getInstance().cleanupPlayer(context, true, true);
                        updateTitle(false);
                        MediaController.getInstance().playAudio(MusicPreferance.playingSongDetail);

                    }
                }
                if (data.getScheme().equalsIgnoreCase("http"))
                    Log.i(TAG, data.getPath());
                if (data.getScheme().equalsIgnoreCase("content"))
                    Log.i(TAG, data.getPath());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void pushFragment(Fragment fragment, String tag, boolean addToBackStack) {
        isFragmentLoaded = true;
        fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment, tag);
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commitAllowingStateLoss();
    }

    @Override
    public void didReceivedNotification(int id, Object... args) {
        if (id == NotificationManager.audioDidStarted || id == NotificationManager.audioPlayStateChanged || id == NotificationManager.audioDidReset) {
            updateTitle(id == NotificationManager.audioDidReset && (Boolean) args[1]);
        } else if (id == NotificationManager.audioProgressDidChanged) {
            PojoSongForPlayer mSongDetail = MediaController.getInstance().getPlayingSongDetail();
            updateProgress(mSongDetail);
        }

    }

    @Override
    public void newSongLoaded(Object... args) {
        songDetail = (PojoSongForPlayer) args[0];
        main_player_gif.startAnimation();
        main_player_gif1.startAnimation();
        drawer_gif_image.startAnimation();
//        gif_image_single.startAnimation();
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment);

//        if (f instanceof FragmentFeaturedArtist) {
//            Utils.getInstance().d("url" + songDetail.getS_url());
//            ((FragmentFeaturedArtist) f).onLoadSong(songDetail.getS_url());
//        }
//        if (f instanceof FragmentFeaturedDjsSong) {
//            Utils.getInstance().d("url" + songDetail.getS_url());
//            ((FragmentFeaturedDjsSong) f).onLoadSong(songDetail.getS_url());
//        }
//        if (f instanceof FragmentFeaturedDjs) {
//            Utils.getInstance().d("url" + songDetail.getS_url());
//            ((FragmentFeaturedDjs) f).onLoadSong(songDetail.getS_url());
//        }
        if (f instanceof FragmentMyFav) {
            Utils.getInstance().d("url" + songDetail.getS_url());
            ((FragmentMyFav) f).onLoadSong(songDetail.getS_url());
        }
        if (f instanceof FragmentMyPlaylistSong) {
            Utils.getInstance().d("url" + songDetail.getS_url());
            ((FragmentMyPlaylistSong) f).onLoadSong(songDetail.getS_url());
        }
        if (f instanceof FragmentArtistOfTheMonth) {
            Utils.getInstance().d("url" + songDetail.getS_url());
            ((FragmentArtistOfTheMonth) f).onLoadSong(songDetail.getS_url());
        }
        if (f instanceof FragmentSingleGenre) {
            Utils.getInstance().d("url" + songDetail.getS_url());
            ((FragmentSingleGenre) f).onLoadSong(songDetail.getS_url());
        }

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
    }

    @SuppressLint("StaticFieldLeak")
    private void getAllSongs() {
        final ArrayList<PojoSongForPlayer> songList = new ArrayList<>();
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Loading");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String response = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("u_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    response = WebInterface.getInstance().doPostRequest(Const.GET_ALL_SONGS, jsonObject.toString());
                    Utils.getInstance().d("mytag" + jsonObject.toString());
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                Utils.getInstance().d("Response : " + response);
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                mProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray data = jsonObject.getJSONArray("data");
                    Utils.getInstance().d("data length : " + data.length());
                    int endPoint = 0;
                    if (data.length() > 150) {
                        endPoint = 150;
                    } else {
                        endPoint = data.length();
                    }
                    for (int i = 0; i < endPoint; i++) {
                        JSONObject object = data.optJSONObject(i);
                        String song_id = object.getString("song_id");
                        Utils.getInstance().d("data song_id : " + song_id);
                        String song_name = object.getString("song_name");
                        Utils.getInstance().d("data song_name : " + song_name);
                        String song_url = object.getString("song_url");
                        Utils.getInstance().d("data song_url : " + song_url);
                        String song_time = object.getString("song_time");
                        Utils.getInstance().d("data song_time : " + song_time);
                        String image = object.getString("image");
                        Utils.getInstance().d("data image : " + image);
                        String artist_name = object.getString("artist_name");
                        Utils.getInstance().d("data artist_name : " + artist_name);
                        String durInMili = String.valueOf(Utils.getInstance().strToMilli(song_time));
                        songList.add(new PojoSongForPlayer(song_name, durInMili, song_url, image, artist_name, song_id));

                    }
                    if (songList.size() > 0) {
                        final PojoSongForPlayer mDetail = songList.get(0);
                        ProgressDialog progressDialog = BufferData.getInstance().getUniversalProgressLoader();
                        if (mDetail != null) {
                            if (progressDialog != null) {
                                Utils.getInstance().d("not null");
                                if (!progressDialog.isShowing()) {
                                    Utils.getInstance().d("not showing");
                                    progressDialog.show();
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (MediaController.getInstance().isPlayingAudio(mDetail) && !MediaController.getInstance().isAudioPaused()) {
                                                MediaController.getInstance().pauseAudio(mDetail);
                                            } else {
                                                MediaController.getInstance().setPlaylist(songList, mDetail, PhoneMediaControl.SonLoadFor.All.ordinal(), -1);
                                            }
                                            songDetail = mDetail;
//                                            isNextPrevious = true;
                                            updateTitle(false);
                                            loadSongsDetails(mDetail);
                                        }
                                    }, 200);

                                }


                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute();

    }

    private void registerUser() {
        RestClient restClient;
        restClient = RestClient.getInstance();
        RestClient.setRestClientInterface(new RestClient.getRestClientInterface() {
            @Override
            public void onSuccess(String jsonResponse) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);
                    Log.d("mytag","response of the registeruser is---"+jsonResponse);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(String msg) {
            }

            @Override
            public void onNetworkError(String msg) {
            }
        });
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("u_id", Prefs.getPrefInstance().getValue(this, Const.USER_ID, ""));
            jsonObject.put("gcm_id", FirebaseInstanceId.getInstance().getToken());
            jsonObject.put("device", "android");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        restClient.doPostRequest(context, Const.REGISTER_USER, jsonObject.toString());

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

    }

    public void googleAdview() {
        if (CheckNetwork.isInternetAvailable(context)) {
            new googleAdds().execute();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private class googleAdds extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            try {
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String url = Const.GOOGLE_ADD;
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return result;
            } catch (Exception e) {
                return null;
            }
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("mytag", "add Response : " + s);
            if (s != null) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    int status = jsonObject.getInt("status");
                    Log.d("mytag", "status is---" + status);
                    String msg = jsonObject.getString("msg");
                    Log.d("mytag", "message is---" + msg);
                    if (status == 1) {
                        android_key = jsonObject.getString("android_key");
                        android_interstitial = jsonObject.getString("android_interstitial");
                        Log.d("mytag", "my ad key is-----" + android_key);
                        Prefs.getPrefInstance().setValue(context, Const.Google_add_key, android_key);
                        Prefs.getPrefInstance().setValue(context, Const.INTERESTITIAL_KEY, android_interstitial);
                    } else {
                        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
                    }
                    System.out.println(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
            }
        }
    }
}
