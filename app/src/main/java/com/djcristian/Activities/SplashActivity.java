package com.djcristian.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.djcristian.R;
import com.djcristian.utility.Const;
import com.djcristian.utility.Prefs;
import com.djcristian.utility.RestClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;


public class SplashActivity extends AppCompatActivity {

    private String token, android_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    String token = Objects.requireNonNull(task.getResult()).getToken();
                    Prefs.getPrefInstance().setValue(this, Const.GCM_ID, token);
                });

        token = FirebaseInstanceId.getInstance().getToken();
        FirebaseMessaging.getInstance().subscribeToTopic("djcristian");
        Log.d("mytag", "Refreshed token: " + token);
        android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        Prefs.getPrefInstance().setValue(SplashActivity.this, Const.USER_ID, android_id);
        Prefs.getPrefInstance().setValue(SplashActivity.this, Const.GCM_ID, token);
//        registerUser();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
            }
        }, 3000);
    }

    private void registerUser() {
        RestClient restClient;
        restClient = RestClient.getInstance();
        RestClient.setRestClientInterface(new RestClient.getRestClientInterface() {
            @Override
            public void onSuccess(String jsonResponse) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(String msg) {
            }

            @Override
            public void onNetworkError(String msg) {
            }
        });
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("u_id", android_id);
            jsonObject.put("gcm_id", FirebaseInstanceId.getInstance().getToken());
            jsonObject.put("device_type", "android");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        restClient.doPostRequest(this, Const.REGISTER_USER, jsonObject.toString());

    }


}
