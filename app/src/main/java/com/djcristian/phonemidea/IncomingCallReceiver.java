package com.djcristian.phonemidea;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import com.djcristian.Manager.MediaController;
import com.djcristian.utility.Utils;


public class IncomingCallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (TelephonyManager.EXTRA_STATE_RINGING.equals(intent.getStringExtra(TelephonyManager.EXTRA_STATE))) {
            Utils.getInstance().d("call ringing");
            if (MediaController.getInstance().getPlayingSongDetail() != null) {
                if (MediaController.getInstance().isAudioPaused() == false) {
//                    MsgData.getInstance().setIsPlaying("1");
                    MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                }
            }
            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
//            Toast.makeText(context, incomingNumber, Toast.LENGTH_LONG).show();
        }
        if (TelephonyManager.EXTRA_STATE_IDLE.equals(intent.getStringExtra(TelephonyManager.EXTRA_STATE))) {
            if (MediaController.getInstance().getPlayingSongDetail() != null) {
                if (MediaController.getInstance().isAudioPaused() != true) {
//                    MsgData.getInstance().setIsPlaying("1");
                    MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                }
            }

        }

    }
}
