package com.djcristian.utility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class RestClient {
    private static RestClient Instance = null;

    public static RestClient getInstance() {
        if (Instance == null)
            Instance = new RestClient();
        return Instance;
    }

    @SuppressLint("StaticFieldLeak")
    public void doPostRequest(Context context, final String url, final String json) {
        Utils.getInstance().d("Json" + json);
        if (Utils.getInstance().isConnectivity(context)) {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }


                @Override
                protected String doInBackground(Void... params) {
                    String response = null;
                    try {
                        response = WebInterface.getInstance().doPostRequest(url, json);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return response;
                }

                @Override
                protected void onPostExecute(String response) {
                    super.onPostExecute(response);
                    if (getRestClientInterface != null) {
                        if (response != null) {
                            Utils.getInstance().d("Response" + response);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response);
                                if (jsonObject.getString("status").equals("1")) {
                                    getRestClientInterface.onSuccess(response);
                                } else {
                                    getRestClientInterface.onFail(jsonObject.getString("msg"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            getRestClientInterface.onFail("Response Null");
                        }
                    }
                }
            }.execute();
        } else {
            getRestClientInterface.onNetworkError("No Internet Connection");
        }
    }

    @SuppressLint("StaticFieldLeak")
    public void doGetRequest(Context context, final String url) {
//        Utils.getInstance().d("Json" +json);
        if (Utils.getInstance().isConnectivity(context)) {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected String doInBackground(Void... params) {
                    String response = null;
                    try {
                        response = WebInterface.getInstance().doGet(url);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return response;
                }

                @Override
                protected void onPostExecute(String response) {
                    super.onPostExecute(response);
                    if (getRestClientInterface != null) {
                        if (response != null) {
                            Utils.getInstance().d("Response" + response);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response);
                                if (jsonObject.getString("status").equals("1")) {
                                    getRestClientInterface.onSuccess(response);
                                } else {
                                    getRestClientInterface.onFail(jsonObject.getString("msg"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            getRestClientInterface.onFail("Response Null");
                        }
                    }
                }
            }.execute();
        } else {
            getRestClientInterface.onNetworkError("No Internet Connection");
        }
    }

    public static getRestClientInterface getRestClientInterface;

    public static void setRestClientInterface(getRestClientInterface getRestClientInterface) {
        RestClient.getRestClientInterface = getRestClientInterface;
    }


    public interface getRestClientInterface {
        public void onSuccess(String jsonResponse);

        public void onFail(String msg);

        public void onNetworkError(String msg);
    }
}
