package com.djcristian.utility;


public class Const {

    /**
     * Preference file name where you can store data in device
     */
    public static final String PREFS_FILENAME = "djmusic";
    public static final String SHUFFEL = "isdhuffel";
    public static final String REPEAT = "isRepear";
    public static final String USER_ID = "user_id";
    public static final String GCM_ID = "gcm";
    public static final String FB = "fb";
    public static final String INSTA = "insta";
    public static final String TWIT = "twit";
    public static final String YOUTUBE = "youtube";
    public static final String SOUND = "sound";
    public static final String WEB = "web";
    public static final String CHECK = "check";
    public static final String NOTIFICATION_STATUS= "notification_status";
    public static final String Google_add_key = "googleadskey";
    public static final String INTERESTITIAL_KEY = "android_interstitial";

    //    Total API
//    los sobrevivientes
    public static final String HOST_CRISTIAN = "http://durisimomobileapps.net/djcristian/api/";
//    public static final String HOST_CRISTIAN = "http://durisimomobileapps.net/djcristian/api/googleads";

    public static final String BOOKING = HOST_CRISTIAN + "booking";
    public static final String SELECT_GENERS = HOST_CRISTIAN + "songsbycategory";
    public static final String SELECT_ALBUM = HOST_CRISTIAN + "Genres/geners_album_song";
    public static final String ADD_TO_FAV = HOST_CRISTIAN + "favs/add";
    public static final String GET_FAV = HOST_CRISTIAN + "favs/getsongs";
    public static final String ADD_COMMENT = HOST_CRISTIAN + "comments/add";
    public static final String GET_COMMENT = HOST_CRISTIAN + "comments";
    public static final String REMOVE_FROM_FAV = HOST_CRISTIAN + "favs/remove";
    public static final String CREATE_PLAYLIST = HOST_CRISTIAN + "playlist/new";
    public static final String GET_PLAYLIST = HOST_CRISTIAN + "playlist/user";
    public static final String ADD_NEW_SONG = HOST_CRISTIAN + "playlist/add";
    public static final String GET_ALL_IMGS = HOST_CRISTIAN + "folders/images";
    public static final String GET_ALL_SONGS = HOST_CRISTIAN + "all_songs";
    public static final String GET_SONGS_BY_PLAYLIST = HOST_CRISTIAN + "playlist/getsongs";
    public static final String EVENTS = HOST_CRISTIAN + "events";
    public static final String COUNT_GENERS = HOST_CRISTIAN + "categories";
    public static final String SONG_LIKES = HOST_CRISTIAN + "addlike";
    public static final String PAGES_PRIVACY = HOST_CRISTIAN + "Pages/privacy";
    public static final String PAGES_TERMS = HOST_CRISTIAN + "Pages/terms";
    public static final String ALL_ALBUM = HOST_CRISTIAN + "Genres/demo";
    public static final String CONTACT_US = HOST_CRISTIAN + "signup";
    public static final String DELETE_PLAYLIST = HOST_CRISTIAN + "playlist/removePlaylist";
    public static final String GET_FEATURING_DJS = HOST_CRISTIAN + "djs";
    public static final String GET_SONG_BY_FEATURING_DJS = HOST_CRISTIAN + "djs/songs";
    public static final String GET_FEATURING_ARTIST = HOST_CRISTIAN + "artists";
    public static final String GET_SONG_BY_FEATURING_ARTIST = HOST_CRISTIAN + "artists/songs";
    public static final String REGISTER_USER = HOST_CRISTIAN + "add_user";
    public static final String GET_DJ_MIXES_ALL_ALBUMS_SONGS = HOST_CRISTIAN + "Feature/select_dj";
    public static final String GET_ARTIST_OF_THE_MONTH_ALL_SONGS = HOST_CRISTIAN + "hitsingles";
    public static final String GET_ARTIST_OF_THE_MONTH_ALL_ALBUMS_SONGS = HOST_CRISTIAN + "Dj/select_artists_album";
    public static final String GET_MIX_OF_THE_MONTH_ALL_SONGS = HOST_CRISTIAN + "Dj/mix_month";
    public static final String GET_MIX_OF_THE_MONTH_ALL_ALBUMS = HOST_CRISTIAN + "Dj/mix_month_album";
    public static final String GET_MIX_OF_THE_MONTH_ALL_ALBUMS_SONGS = HOST_CRISTIAN + "Dj/select_mix_month_album";
    public static final String GET_DJ_OF_THE_MONTH_ALL_SONGS = HOST_CRISTIAN + "Dj/dj_month";
    public static final String GET_DJ_OF_THE_MONTH_ALL_ALBUMS = HOST_CRISTIAN + "Dj/dj_month_album";
    public static final String SOCIAL = HOST_CRISTIAN + "social_links";
    public static final String SHARE = HOST_CRISTIAN + "share";
    public static final String PAGES_SUPPORT = HOST_CRISTIAN + "Pages/support";
    public static final String VIDEO = HOST_CRISTIAN + "videos";
    public static final String PAGES_HOW_USE_APP = HOST_CRISTIAN + "Pages/how";
    public static final String GET_NOTIFICATION = HOST_CRISTIAN + "notifications";
    public static final String REMOVE_FROM_PLAYLIST = HOST_CRISTIAN + "playlist/remove";
    public static final String GALLERY_FOLDER = HOST_CRISTIAN + "folders";
    public static final String GET_BIOGRAPHIES = HOST_CRISTIAN + "biographies";
    public static final String GET_SINGLE_SLIDER_IMAGES = HOST_CRISTIAN + "sponsors";
    public static final String GET_SINGLE_SPOSOR_DETAIL = HOST_CRISTIAN + "getSponsor";
    public static final String GOOGLE_ADD= HOST_CRISTIAN+"googleads";

    public static final String HOST_DJ_HERE_THE_MUSIC = "http://durisimomobileapps.net/hearthemusic/api/";

    public static final String PAGES_VIDEO = HOST_CRISTIAN + "slider";


}
